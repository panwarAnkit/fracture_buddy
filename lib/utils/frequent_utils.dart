import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fracture_buddy/models/registerUser/User.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:intl/intl.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'constants.dart';
import 'custom_extensions.dart';
import 'SharedPref.dart';

class FrequentUtils {
  static FrequentUtils _instance;
  Future _dialog;

  static FrequentUtils getInstance() {
    if (_instance == null) _instance = new FrequentUtils();
    return _instance;
  }

  String countryCodeToEmoji(String countryCode) {
    // 0x41 is Letter A
    // 0x1F1E6 is Regional Indicator Symbol Letter A
    // Example :
    // firstLetter U => 20 + 0x1F1E6
    // secondLetter S => 18 + 0x1F1E6
    // See: https://en.wikipedia.org/wiki/Regional_Indicator_Symbol
    final int firstLetter = countryCode.codeUnitAt(0) - 0x41 + 0x1F1E6;
    final int secondLetter = countryCode.codeUnitAt(1) - 0x41 + 0x1F1E6;
    return String.fromCharCode(firstLetter) + String.fromCharCode(secondLetter);
  }

  String getDateWithSavedFormat(int date) {
    var formatter = new DateFormat("dd/MMM/yyyy");
    return formatter.format(DateTime.fromMillisecondsSinceEpoch(date));
  }

  showSnackBarMessage(GlobalKey<ScaffoldState> _globalKey, String message) {
    if (message.isNullOrEmpty()) return;
    final snackBar = SnackBar(
      content: Text(message, maxLines: 4, overflow: TextOverflow.ellipsis),
      duration: Duration(seconds: 1),
    );
    _globalKey.currentState.showSnackBar(snackBar);
  }

  showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        backgroundColor: Colors.black,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        fontSize: 15.0);
  }

  showProgressDialog(BuildContext context) {
    if (_dialog == null) {
      _dialog = showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Material(
            color: Colors.transparent,
            child: Center(
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 120,
                      height: 120,
                      child: LoadingIndicator(
                        indicatorType: Indicator.ballClipRotateMultiple,
                        color: APP_PRIMARY_COLOR_DARK,
                      ),
                    ),
                    Text("Please wait...", style: textStyleQuicksandBlueBold16)
                  ],
                ),
              ),
            ),
          );
        },
      );
    }
  }

  hideProgressDialog(BuildContext context) {
    if (_dialog != null) {
      Navigator.pop(context);
      _dialog = null;
    }
  }

  Widget centerTextWidget(String text) {
    return Center(
      child: Text(text, style: textStyleBlack, textAlign: TextAlign.center),
    );
  }

  String getPlanPhoneNumber(String phoneCode, String phone) {
    return "$phoneCode${phone.replaceAll(RegExp("[\\(\\)\\ \\-]"), "").trim()}";
  }

  String getFormattedPhoneNumber(String phone) {
    var finalNumber;
    if (phone.length > 10) {
      finalNumber =
          "+${phone.substring(0, phone.length - 10)} ${phone.substring(phone.length - 10, phone.length).replaceAllMapped(RegExp(r'(\d{3})(\d{3})(\d+)'), (Match m) => "(${m[1]}) ${m[2]}-${m[3]}")}";
    } else {
      finalNumber = phone.replaceAllMapped(RegExp(r'(\d{3})(\d{3})(\d+)'),
          (Match m) => "(${m[1]}) ${m[2]}-${m[3]}");
    }
    return finalNumber;
  }

  String getTwoDigitNumber(int number) => NumberFormat("00").format(number);

  Future<void> saveUserData(User user) async {
    await SharedPref.initializePreferences();
    SharedPref.mInstance.setAutoLogin(true);
    SharedPref.mInstance
        .setBool(SharedPref.STORAGE_IS_APPROVED, user.isApproved);
    SharedPref.mInstance.setString(SharedPref.STORAGE_USER_ID, user.id1);
    SharedPref.mInstance
        .setString(SharedPref.STORAGE_FIRST_NAME, user.firstName);
    SharedPref.mInstance.setString(SharedPref.STORAGE_LAST_NAME, user.lastName);
    SharedPref.mInstance.setString(SharedPref.STORAGE_USER_NAME, user.userName);
    SharedPref.mInstance
        .setString(SharedPref.STORAGE_PROFILE_PIC, user.profilePic);
    SharedPref.mInstance.setString(SharedPref.STORAGE_PHONE, user.phone);
    SharedPref.mInstance.setString(SharedPref.STORAGE_EMAIL, user.email);
    SharedPref.mInstance
        .setString(SharedPref.STORAGE_COUNTRY_CODE, user.countryCode);
    SharedPref.mInstance
        .setString(SharedPref.STORAGE_COLLEGE, jsonEncode(user.college));
    SharedPref.mInstance.setStringList(SharedPref.STORAGE_SPECIALITIES,
        user.speciality.map((e) => jsonEncode(e)).toList());
    SharedPref.mInstance.setStringList(SharedPref.STORAGE_HOSPITALS,
        user.hospital.map((e) => jsonEncode(e)).toList());
    if (user.token.isNullOrEmptyNot()) {
      SharedPref.mInstance.setString(SharedPref.STORAGE_TOKEN, user.token);
    }
  }

  showImageInFullScreen(BuildContext context, String imageUrl, File imageFile) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Material(
          color: Colors.transparent,
          child: Center(
            child: Container(
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: imageUrl!=null
                        ?CachedNetworkImage(
                      imageUrl: imageUrl,
                      placeholder: (context, url) => Center(
                        child: Padding(
                          padding: EdgeInsets.all(30.0),
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      errorWidget: (context, url, value) {
                        return SvgPicture.asset(
                          "assets/svg/caution.svg",
                          fit: BoxFit.cover,
                        );
                      },
                    )
                    :Image.file(imageFile),
                  ),
                  Positioned(
                    top: 4,
                    right: 4,
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: Container(
                        width: 30,
                        height: 30,
                        padding: EdgeInsets.all(8.0),
                        child: SvgPicture.asset(
                          "assets/cross.svg",
                          color: APP_PRIMARY_COLOR,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void showDialogToChooseImageOption(BuildContext context,Function onClickViewImage,Function onClickUploadImage) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: Wrap(
              children: <Widget>[
                Text("Choose Options", style: textStyleQuicksandBlack16),
                SizedBox(height: 35.0),
                Column(
                  children: <Widget>[
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      onTap: onClickViewImage,
                      child: ListTile(
                        title: Text(
                          "View Image",
                          style: textStyleBlack,
                        ),
                      ),
                    ),
                    InkWell(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4.0),
                      ),
                      onTap: onClickUploadImage,
                      child: ListTile(
                        title: Text(
                          "Upload New",
                          style: textStyleBlack,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
