import 'package:flutter/material.dart';
import 'constants.dart';

TextStyle textStyleBlueAvenik24 = const TextStyle(
    color: APP_PRIMARY_COLOR_DARK,
    fontSize: 24.0,
    fontWeight: FontWeight.w400,
    fontFamily: "Avenik"
);

TextStyle textStyleBlueAvenik22 = const TextStyle(
    color: APP_PRIMARY_COLOR_DARK,
    fontSize: 22.0,
    fontWeight: FontWeight.w400,
    fontFamily: "Avenik"
);

TextStyle textStyleBlueAvenikBold24 = const TextStyle(
    color: APP_PRIMARY_COLOR_DARK,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Avenik"
);

TextStyle textStyleBlueAvenikBold26 = const TextStyle(
    color: APP_PRIMARY_COLOR_DARK,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Avenik"
);

TextStyle textStyleLightBlueAvenik26 = const TextStyle(
    color: APP_PRIMARY_COLOR,
    fontSize: 26.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Avenik"
);

TextStyle textStyleAvenikYellow26 = const TextStyle(
    color: COLOR_YELLOW_LIGHT,
    fontSize: 26.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Avenik"
);

TextStyle textStyleLightBlueAvenikBold26 = const TextStyle(
    color: APP_PRIMARY_COLOR,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Avenik"
);

TextStyle textStyleGreyQuicksand16 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 16.0,
    fontWeight: FontWeight.w400,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBlack14 = const TextStyle(
    color: Colors.black,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBlack12 = const TextStyle(
    color: Colors.black,
    fontSize: 12.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBlack16 = const TextStyle(
    color: Colors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBlack18 = const TextStyle(
    color: Colors.black,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBoldBlack26 = const TextStyle(
    color: Colors.black,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandLightGrey14 = const TextStyle(
    color: Colors.grey,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandLightGrey16 = const TextStyle(
    color: Colors.grey,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandLightGrey26 = const TextStyle(
    color: Colors.grey,
    fontSize: 26.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleGreyQuicksandBold16 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleGreyQuicksandBold14 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleGreyQuicksand14 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleGreyQuicksandBold12 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhiteQuicksandBold12 = const TextStyle(
    color: Colors.white,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleGreyQuicksand12 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 12.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleLightGreyQuicksand14 = const TextStyle(
    color: COLOR_LIGHT_GRAY,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBlueBoldUL16 = const TextStyle(
  color: APP_PRIMARY_COLOR_DARK,
  fontSize: 16.0,
  fontWeight: FontWeight.w600,
  fontFamily: "Quicksand",
  decoration: TextDecoration.underline,
);

TextStyle textStyleQuicksandBlueBold16 = const TextStyle(
  color: APP_PRIMARY_COLOR_DARK,
  fontSize: 16.0,
  fontWeight: FontWeight.w600,
  fontFamily: "Quicksand",
);

TextStyle textStyleQuicksandBlueBold18 = const TextStyle(
  color: APP_PRIMARY_COLOR_DARK,
  fontSize: 18.0,
  fontWeight: FontWeight.w600,
  fontFamily: "Quicksand",
);

TextStyle textStyleGreyQuicksandBold18 = const TextStyle(
    color: COLOR_DARK_GRAY,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhite = const TextStyle(
    color: Colors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhite12 = const TextStyle(
    color: Colors.white,
    fontSize: 12.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleBlack = const TextStyle(
    color: Colors.black,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhite16 = const TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhiteBold16 = const TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhiteBold20 = const TextStyle(
    color: Colors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhiteBold18 = const TextStyle(
    color: Colors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleWhite18 = const TextStyle(
    color: Colors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.normal,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBoldYellow16 = const TextStyle(
    color: COLOR_YELLOW_LIGHT,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBoldYellow18 = const TextStyle(
    color: COLOR_YELLOW_LIGHT,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBoldYellow12= const TextStyle(
    color: COLOR_YELLOW_LIGHT,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);

TextStyle textStyleQuicksandBoldYellowDark14= const TextStyle(
    color: COLOR_YELLOW_DARK,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Quicksand"
);




int getColorHexFromStr(String colorStr) {
  colorStr = "FF" + colorStr;
  colorStr = colorStr.replaceAll("#", "");
  int val = 0;
  int len = colorStr.length;
  for (int i = 0; i < len; i++) {
    int hexDigit = colorStr.codeUnitAt(i);
    if (hexDigit >= 48 && hexDigit <= 57) {
      val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 65 && hexDigit <= 70) {
      // A..F
      val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 97 && hexDigit <= 102) {
      // a..f
      val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
    } else {
      throw new FormatException("An error occurred when converting a color");
    }
  }
  return val;
}
