import 'package:flutter/material.dart';

/// Extensions for
/// int, String, Widget, Text, bool, object
/// */


// int
extension int_extension on num{
  SizedBox toVerticalSpace() {
    return SizedBox(height: this.toDouble(),);
  }

  SizedBox toHorizontalSpace() {
    return SizedBox(width: this.toDouble(),);
  }
}

// string
extension string_extension on String {
  bool isNullOrEmpty() {
    return (this == null || this.isEmpty);
  }

  bool isNullOrEmptyNot() {
    return (this != null && this.isNotEmpty);
  }

  String withoutSpaces() {
    return this.replaceAll(new RegExp(r"\s\b|\b\s"), "");
  }
}

// widgets
extension widgets_extension on Widget{
  // add click
  Widget addInkwell({@required Function onClick}) {
    return InkWell(
      child: this,
      onTap: () => onClick(),
    );
  }

  // add padding
  Widget addPaddingAll(num value) {
    return Padding(padding: EdgeInsets.all(value.toDouble()), child: this,);
  }

  // add horizontal padding
  Widget addPaddingHorizonal(num value) {
    return Padding(padding: EdgeInsets.symmetric(horizontal: value.toDouble()),
      child: this,);
  }

  // add vertical padding
  Widget addPaddingVertical(num value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: value.toDouble()), child: this,);
  }

  Widget alignCenterLeft() {
    return Align(alignment: Alignment.centerLeft, child: this);
  }

  Widget alignCenterRight() {
    return Align(alignment: Alignment.centerRight, child: this);
  }

  Widget alignCenter() {
    return Align(alignment: Alignment.center, child: this);
  }
}

// text
extension text_extension on Text{
  Text addTextStyle(TextStyle style) {
    return Text(this.data, style: style,);
  }
}

//bool
extension bool_extensions on bool {
  bool not() => !this;
}

//object
extension object_extension on Object {
  bool isNull() => this == null;
  bool isNotNull() => isNull().not();

  bool isNullOrEmpty() {
    if (this is String)
      return (this.isNull() || (this as String).isEmpty);
    else if (this is List)
      return (this.isNull() || (this as List).isEmpty);
    else
      return this.isNull();
  }

  bool isNotNullOrEmpty() => this.isNullOrEmpty().not();
}