import 'package:flutter/material.dart';
import 'package:fracture_buddy/views/awaitingApproval/awaiting_approval_screen.dart';
import 'package:fracture_buddy/views/boneView/bone_view_screen.dart';
import 'package:fracture_buddy/views/completeProfile/complete_profile_screen.dart';
import 'package:fracture_buddy/views/createAccount/create_account_screen.dart';
import 'package:fracture_buddy/views/editProfile/edit_profile_screen.dart';
import 'package:fracture_buddy/views/home/home_screen.dart';
import 'package:fracture_buddy/views/intro/intro_screen.dart';
import 'package:fracture_buddy/views/skeletonView/skeleton_view_screen.dart';
import 'package:fracture_buddy/views/splash/splash_screen.dart';
import 'package:fracture_buddy/views/successfulSubmit/successful_submit_screen.dart';
import 'package:fracture_buddy/views/surgicalCareForm/surgical_care_form_screen.dart';
import 'package:fracture_buddy/views/verifyOTP/verify_otp_screen.dart';

class PageViewTransition<T> extends MaterialPageRoute<T> {
  PageViewTransition({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    var begin = Offset(1.0, 0.0);
    var end = Offset.zero;
    var curve = Curves.ease;
    var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
    return SlideTransition(position: animation.drive(tween), child: child);
  }
}

class AppRoute {
  static const String splashScreen = '/splashScreen';
  static const String introScreen = '/introScreen';
  static const String createAccount = '/createAccount';
  static const String verifyOTP = '/verifyOTP';
  static const String completeProfile = '/completeProfile';
  static const String successfulSubmit = '/successfulSubmit';
  static const String awaitingApproval = '/awaitingApproval';
  static const String homeScreen = '/homeScreen';
  static const String surgicalCareForm = '/surgicalCareForm';
  static const String skeletonView = "skeletonView";
  static const String boneView = "boneView";
  static const String editProfile = "editProfile";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var data = settings.arguments;
    switch (settings.name) {
      case splashScreen:
        return PageViewTransition(builder: (_) => SplashScreen());
      case introScreen:
        return PageViewTransition(builder: (_) => IntroScreen());
      case createAccount:
        return PageViewTransition(builder: (_) => CreateAccountScreen());
      case verifyOTP:
        return PageViewTransition(
          builder: (_) => VerifyOTPScreen(data),
        );
      case completeProfile:
        return PageViewTransition(
          builder: (_) => CompleteProfileScreen(phone: data),
        );
      case successfulSubmit:
        return PageViewTransition(builder: (_) => SuccessfulSubmitScreen());
      case awaitingApproval:
        return PageViewTransition(builder: (_) => AwaitingApprovalScreen());
      case homeScreen:
        return PageViewTransition(builder: (_) => HomeScreen());
      case surgicalCareForm:
        return PageViewTransition(builder: (_) => SurgicalCareFormScreen());
      case skeletonView:
        return PageViewTransition(builder: (_) => SkeletonViewScreen());
      case boneView:
        return PageViewTransition(builder: (_) => BoneViewScreen(data));
      case editProfile:
        return PageViewTransition(builder: (_) => EditProfileScreen());
      default:
        return PageViewTransition(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
