import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HumanAnatomy extends StatefulWidget {
  final ValueChanged<List> onChanged;
  final Function onClickBone;

  const HumanAnatomy({Key key, this.onChanged,this.onClickBone}) : super(key: key);
  @override
  HumanAnatomyState createState() => new HumanAnatomyState();
}

class HumanAnatomyState extends State<HumanAnatomy> {
  var bodyPartList = [];

  @override
  void initState() {
    super.initState();
    if (mounted) {
      _publishSelection(bodyPartList);
    }
  }

  @override
  Widget build(BuildContext context) {
    return humanAnatomy();
  }

  Widget humanAnatomy() {
    return Container(
      width: 340,
      height: 450,
      child: Center(
        child: Stack(
          children: <Widget>[
            bodyPart("assets/full_head.svg", "FullHead", 0.0, 0.0, 0.0, 80.0),
            bodyPart("assets/upper_head.svg", "UpperHead", 0.0, 0.0, 0.0, 80.0),
            bodyPart("assets/left_eye.svg", "LeftEye", 20.0, 17.0, 0.0, 10.0),
            bodyPart("assets/right_eye.svg", "rightEye", 20.0, 0.0, 17.0, 10.0),
            bodyPart("assets/nose.svg", "Nose", 28.0, 0.0, 0.0, 20.0),
            bodyPart("assets/mouth.svg", "Mouth", 43.0, 0.0, 0.0, 10.0),
            bodyPart("assets/neck.svg", "Neck", 58.0, 0.0, 0.0, 30.0),
            bodyPart("assets/neck.svg", "Neck", 58.0, 0.0, 0.0, 30.0),
            bodyPart("assets/left_scapula.svg", "LeftScapula", 84.0, 57.0, 0.0, 70.0),
            bodyPart("assets/right_scapula.svg", "RightScapula", 84.0, 0.0, 57.0, 70.0),
            bodyPart("assets/left_acromion.svg", "LeftAcromion", 80.0, 82.0, 0.0, 20.0),
            bodyPart("assets/right_acromion.svg", "RightAcromion", 80.0, 0.0, 82.0, 20.0),
            bodyPart("assets/left_rib11.svg", "LeftRib11", 126.0, 23.0, 0.0, 100.0),
            bodyPart("assets/right_rib11.svg", "RightRib11", 126.0, 0.0, 20.0, 100.0),
            bodyPart("assets/left_rib12.svg", "LeftRib12", 133.0, 26.0, 0.0, 100.0),
            bodyPart("assets/right_rib12.svg", "RightRib12", 133.0, 0.0, 22.0, 100.0),
            bodyPart("assets/left_rib13.svg", "LeftRib13", 140.0, 26.0, 0.0, 100.0),
            bodyPart("assets/right_rib13.svg", "RightRib13", 140.0, 0.0, 22.0, 100.0),
            bodyPart("assets/left_rib10.svg", "LeftRib10", 133.5, 55.0, 0.0, 100.0),
            bodyPart("assets/right_rib10.svg", "RightRib10", 133.0, 0.0, 53.0, 100.0),
            bodyPart("assets/left_rib9.svg", "LeftRib9", 131.0, 62.0, 0.0, 100.0),
            bodyPart("assets/right_rib9.svg", "RightRib9", 130.5, 0.0, 60.0, 100.0),
            bodyPart("assets/left_rib8.svg", "LeftRib8", 124.0, 62.0, 0.0, 100.0),
            bodyPart("assets/right_rib8.svg", "RightRib8", 123.5, 0.0, 61.0, 100.0),
            bodyPart("assets/left_rib7.svg", "LeftRib7", 116.0, 59.0, 0.0, 100.0),
            bodyPart("assets/right_rib7.svg", "RightRib7", 116.0, 0.0, 58.0, 100.0),
            bodyPart("assets/left_rib6.svg", "LeftRib6", 108.5, 55.0, 0.0, 100.0),
            bodyPart("assets/right_rib6.svg", "RightRib6", 108.5, 0.0, 55.0, 100.0),
            bodyPart("assets/left_rib5.svg", "LeftRib5", 100.0, 40.0, 0.0, 100.0),
            bodyPart("assets/right_rib5.svg", "RightRib5", 100.0, 0.0, 39.0, 100.0),
            bodyPart("assets/left_rib4.svg", "LeftRib4", 92.0, 38.0, 0.0, 100.0),
            bodyPart("assets/right_rib4.svg", "RightRib4", 92.0, 0.0, 37.0, 100.0),
            bodyPart("assets/left_rib3.svg", "LeftRib3", 85.0, 34.0, 0.0, 100.0),
            bodyPart("assets/right_rib3.svg", "RightRib3", 85.0, 0.0, 33.0, 100.0),
            bodyPart("assets/left_rib2.svg", "LeftRib2", 81.0, 30.0, 0.0, 100.0),
            bodyPart("assets/right_rib2.svg", "RightRib2", 81.0, 0.0, 29.0, 100.0),
            bodyPart("assets/left_rib1.svg", "LeftRib1", 73.0, 26.0, 0.0, 100.0),
            bodyPart("assets/right_rib1.svg", "RightRib1", 73.0, 0.0, 25.0, 100.0),
            bodyPart("assets/left_clavicle.svg", "LeftClavicle", 77.0, 47.0, 0.0, 20.0),
            bodyPart("assets/right_clavicle.svg", "RightClavicle", 77.0, 0.0, 47.0, 20.0),
            bodyPart("assets/left_cartilage.svg", "LeftCartilage", 116.0, 36.0, 0.0, 110.0),
            bodyPart("assets/right_cartilage.svg", "RightCartilage", 116.0, 0.0, 33.0, 100.0),
            bodyPart("assets/left_forearm1.svg", "leftForeArm1", 159.0, 122.0, 0.0, 100.0),
            bodyPart("assets/left_forearm2.svg", "leftForeArm2", 159.0, 109.0, 0.0, 100.0),
            bodyPart("assets/left_arm.svg", "leftArm", 83.0, 92.0, 0.0, 100.0),
            bodyPart("assets/right_forearm1.svg", "rightForeArm1", 153.0, 0.0, 122.0, 100.0),
            bodyPart("assets/right_forearm2.svg", "rightForeArm2", 153.0, 0.0, 109.0, 100.0),
            bodyPart("assets/right_arm.svg", "rightArm", 83.0, 0.0, 92.0, 100.0),
            bodyPart("assets/left_wrist.svg", "leftWrist", 216.0, 140.0, 0.0, 50.0),
            bodyPart("assets/right_wrist.svg", "rightWrist", 210.0, 0.0, 141.0, 50.0),
            bodyPart("assets/left_hand.svg", "leftHand", 222.0, 157.0, 0.0, 60.0),
            bodyPart("assets/right_hand.svg", "rightHand", 216.0, 0.0, 157.0, 60.0),
            bodyPart("assets/spine.svg", "Spine", 117.0, 3.0, 0.0, 70.0),
            bodyPart("assets/center_chest.svg", "CenterChest", 80.0, 1.0, .0, 50.0),
            bodyPart("assets/bottom_spine.svg", "BottomSpine", 184.0, 0.0, 0.0, 40.0),
            bodyPart("assets/left_pelvis.svg", "LeftPelvis", 180.0, 39.0, 0.0, 50.0),
            bodyPart("assets/right_pelvis.svg", "RightPelvis", 180.0, 0.0, 39.0, 50.0),
            bodyPart("assets/left_leg.svg", "leftLeg", 323.0, 48.0, 0.0, 106.0),
            bodyPart("assets/right_leg.svg", "rightLeg", 323.0, 0.0, 48.0, 106.0),
            bodyPart("assets/left_thigh.svg", "leftThigh", 205.0, 55.0, 0.0, 125.0),
            bodyPart("assets/right_thigh.svg", "rightThigh", 205.0, 0.0, 55.0, 125.0),
            bodyPart("assets/left_ankle.svg", "leftAnkle", 424.0, 49.0, 0.0, 25.0),
            bodyPart("assets/right_ankle.svg", "rightAnkle", 424.0, 0.0, 49.0, 25.0),
            bodyPart("assets/left_knee.svg", "leftKnee", 315.0, 50.0, 0.0, 20.0),
            bodyPart("assets/right_knee.svg", "rightKnee", 315.0, 0.0, 50.0, 20.0),
            bodyPart("assets/left_foot.svg", "leftFoot", 430.0, 62.0, 0.0, 60.0),
            bodyPart("assets/right_foot.svg", "rightFoot", 430.0, 0.0, 62.0, 56.0),
          ],
        ),
      ),
    );
  }

  Widget bodyPart(String svgPath, String svgName, double marginTop,
      double marginRight, double marginLeft, double height, {double bodyHeight}) {
    Color _svgColor = bodyPartList.contains(svgName) ? Color(0XFFD39325) : null;
    final Widget bodyPartSvg = new SvgPicture.asset(svgPath,
        semanticsLabel: svgName, color: _svgColor,height: bodyHeight);
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(
          onTap: () {
            setState(() {
              if (bodyPartList.contains(svgName)) {
                bodyPartList.remove(svgName);
              } else {
                bodyPartList.add(svgName);
              }
            });
            widget.onClickBone(svgName);
          },
          child: bodyPartSvg),
    );
  }

  void _publishSelection(List _bodyPartList) {
    if (widget.onChanged != null) {
      widget.onChanged(_bodyPartList);
    }
  }
}
