import 'dart:ui';

//app name
const String APP_NAME = "Fracture Buddy";

//app urls
const String APP_BASE_URL = "http://35.183.32.14:3000/api/user/";
const String APP_IMAGE_URL = "http://35.183.32.14/fracture_buddy/uploads/";

//colors
const Color APP_PRIMARY_COLOR = Color(0XFF7ED8FF);
const Color APP_PRIMARY_COLOR_LIGHT = Color(0XFFCEEEFC);
const Color APP_PRIMARY_COLOR_DARK = Color(0XFF262263);
const Color APP_PRIMARY_COLOR_DARK2 = Color(0XFF262263);
const Color APP_PRIMARY_COLOR_LIGHT2 = Color(0XFFCFF1FF);
const Color APP_PRIMARY_COLOR_MIX = Color(0XFF29266A);
const Color APP_PRIMARY_COLOR_MIX2 = Color(0XFF2C2C6F);
const Color COLOR_DARK_GRAY = Color(0XFF5D5D5D);
const Color COLOR_LIGHT_GRAY = Color(0XFFDEE2EC);
const Color COLOR_LIGHT_GRAY_40 = Color(0X40FCFCFC);
const Color COLOR_LIGHT_GRAY2 = Color(0XFFFCFCFC);
const Color COLOR_LIGHT_GRAY3 = Color(0XFFF4F4F4);
const Color COLOR_YELLOW_DARK = Color(0XFFD39325);
const Color COLOR_YELLOW_LIGHT = Color(0XFFFAD18C);
const Color COLOR_YELLOW_EXTRA_LIGHT = Color(0XFFFFEBCC);
const Color COLOR_YELLOW_EXTRA_LIGHT_40 = Color(0X40FFEBCC);
const Color COLOR_BROWN = Color(0XFF626262);

//app constant keys
const String GOOGLE_MAP_API_KEY = "AIzaSyC7tFCGTXjDAiiir--ClMA3XVgs3LuDAUc";
const String KEY_FROM = "from";
const String KEY_DATA = "data";
const String VALUE_SKELETON = "skeleton";
const String VALUE_BONE = "bone";
const String VALUE_BONE_LAST = "boneLast";
