import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart' as path;

class ImagePickerPopup{

  static final picker = ImagePicker();

  static void showImagePickerDialog(BuildContext context, Function onFetchImage) {
    checkCameraPhotosPermission().then((permissionGranted) {
      if (permissionGranted) {
        _showImageUploadBottomSheet(context,onFetchImage);
      } else {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content:
            Text('Please allow permission to access Camera and photos.'),
            duration: Duration(seconds: 2),
            action: SnackBarAction(
                label: 'Setting', onPressed: () => openAppSettings()),
          ),
        );
      }
    });
  }

  static Future<bool> checkCameraPhotosPermission() async {
    final cameraPermission = Permission.camera;
    final galleryPermission = Platform.isAndroid ? Permission.storage: Permission.photos;
    final cameraPermissionStatus = await cameraPermission.status;
    final galleryPermissionStatus = await galleryPermission.status;
    if (cameraPermissionStatus.isGranted && galleryPermissionStatus.isGranted) {
      return true;
    } else {
      Map permissionsResult = await [cameraPermission, galleryPermission].request();
      if (permissionsResult.values.length > 0 &&
          permissionsResult.values.elementAt(0) == PermissionStatus.granted &&
          permissionsResult.values.elementAt(1) == PermissionStatus.granted) {
        return true;
      } else {
        return await cameraPermission.shouldShowRequestRationale && await galleryPermission.shouldShowRequestRationale;
      }
    }
  }

  static void _showImageUploadBottomSheet(BuildContext context, Function onFetchImage) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.all(20),
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Text("Upload Image", style: textStyleQuicksandBlack18,textAlign: TextAlign.center,),
                SizedBox(height: 5),
                Text(
                  "Click a Photo or upload your image from saved photos.",
                  style: textStyleGreyQuicksandBold12,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () => getImageFromSource(bc, true, onFetchImage),
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                "assets/png/camera.png",
                                width: 50,
                                height: 50,
                              ),
                              SizedBox(height: 4),
                              Text("Camera", style: textStyleQuicksandBlack14),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () => getImageFromSource(bc, false, onFetchImage),
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                "assets/png/gallery.png",
                                width: 50,
                                height: 50,
                              ),
                              SizedBox(height: 4),
                              Text("Gallery", style: textStyleQuicksandBlack14),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  static void getImageFromSource(BuildContext context, bool fromCamera, Function onFetchImage) async {
    Navigator.of(context).pop();
    final pickedFile = await picker.getImage(source: fromCamera ? ImageSource.camera : ImageSource.gallery, imageQuality: 50);
    final picture = File(pickedFile.path);
    String dir = path.dirname(picture.path);
    String newPath =
    path.join(dir, 'Fracture_Buddy_${DateTime.now().millisecondsSinceEpoch}.jpg');
    final imageFile = picture.renameSync(newPath);
    onFetchImage(imageFile);
  }

}