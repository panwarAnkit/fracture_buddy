import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fracture_buddy/models/registerUser/User.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:intl/intl.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'constants.dart';
import 'custom_extensions.dart';
import 'SharedPref.dart';

class Validations {
  static Validations _instance;

  static Validations getInstance() {
    if (_instance == null) _instance = Validations();
    return _instance;
  }

  bool isFieldEmpty(String field) {
    return field.length.isNullOrEmpty();
  }

  bool isInvalidEmail(String field) {
    final emailRegExp = RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$');
    return !emailRegExp.hasMatch(field);
  }

  bool isInvalidPhone(String field) {
    return field.length<14;
  }
}
