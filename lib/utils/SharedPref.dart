import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {

  static const STORAGE_IS_LOGGED_IN = "STORAGE_IS_LOGGED_IN";
  static const STORAGE_TOKEN = "STORAGE_TOKEN";
  static const STORAGE_USER_ID = "STORAGE_USER_ID";
  static const STORAGE_USER_NAME = "STORAGE_USER_NAME";
  static const STORAGE_FIRST_NAME = "STORAGE_FIRST_NAME";
  static const STORAGE_EMAIL = "STORAGE_EMAIL";
  static const STORAGE_COUNTRY_CODE = "STORAGE_COUNTRY_CODE";
  static const STORAGE_LAST_NAME = "STORAGE_LAST_NAME";
  static const STORAGE_PROFILE_PIC = "STORAGE_PROFILE_PIC";
  static const STORAGE_ADDRESS = "STORAGE_ADDRESS";
  static const STORAGE_PHONE = "STORAGE_PHONE";
  static const STORAGE_IS_APPROVED = "STORAGE_IS_APPROVED";
  static const STORAGE_COLLEGE = "STORAGE_COLLEGE";
  static const STORAGE_SPECIALITIES = "STORAGE_SPECIALITIES";
  static const STORAGE_HOSPITALS = "STORAGE_HOSPITALS";

  static SharedPref mInstance;
  static SharedPreferences _prefs;

  static Future<SharedPreferences> initializePreferences() async {
    if(mInstance==null) mInstance = SharedPref();
    _prefs =  await SharedPreferences.getInstance();
    return _prefs;
  }

  setAutoLogin(bool autoLogin) async {
    await _prefs.setBool(STORAGE_IS_LOGGED_IN, autoLogin);
  }

  bool isAutoLogin() {
    return _prefs.getBool(STORAGE_IS_LOGGED_IN)??false;
  }

  Future<void> setString(String key, String value) async {
    await _prefs.setString(key, value ?? "");
  }

  String getString(String key) {
    return _prefs.getString(key) ?? "";
  }

  Future<void> setDouble(String key, double value) async {
    await _prefs.setDouble(key, value ?? 0.0);
  }

  double getDouble(String key) {
    return _prefs.getDouble(key) ?? 0.0;
  }

  Future<void> setInt(String key, int value) async {
    await _prefs.setInt(key, value ?? 0);
  }

  int getInt(String key) {
    return _prefs.getInt(key) ?? 0;
  }

  Future<void> setBool(String key, bool value) async {
    await _prefs.setBool(key, value ?? false);
  }

  bool getBool(String key) {
    return _prefs.getBool(key) ?? false;
  }

  Future<void> setStringList(String key, List<String> value) async {
    await _prefs.setStringList(key, value ?? List<String>());
  }

  List<String> getStringList(String key) {
    return _prefs.getStringList(key) ?? List<String>();
  }

  clearData() async {
     await _prefs.clear();
  }

}