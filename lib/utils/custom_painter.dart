import 'package:flutter/material.dart';

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.green[800];
    paint.style = PaintingStyle.fill; // Change this to fill

//    var path = Path();
//    path.lineTo(size.width, 0);
//    path.lineTo(size.width, size.height*0.5);
////    path.quadraticBezierTo(size.width, size.height * 0.30, size.width+10, size.height*0.4);
////    path.arcToPoint(Offset(size.width+30, size.height*0.4),radius: Radius.circular(100),);
////    path.quadraticBezierTo(size.width+20, size.height * 0.90, size.width, size.height);
//    path.addArc(Rect.fromCenter(width: size.width,height: size.height,center:Offset(size.width+20, size.height*0.5)), 45, 45);
//    path.lineTo(size.width, size.height);
//    path.lineTo(size.width * .03, size.height);
//
//
//    canvas.drawPath(path, paint);
    canvas.drawArc(new Rect.fromLTWH(0.0, 0.0, size.width, size.height/2), 300, 1, true, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}