import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:fracture_buddy/models/CommonSuccessResponse.dart';
import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allSurgeon/AllSurgeonResponse.dart';
import 'package:fracture_buddy/models/getAssistants/GetAssistantResponse.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserRequest.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserResponse.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/models/serviceList/AllServicesResponse.dart';
import 'package:fracture_buddy/models/surgicalForm/AddSurgicalFormRequest.dart';
import 'package:fracture_buddy/models/surgicalForm/AllSurgicalFormResponse.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpRequest.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpResponse.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/web/interceptor.dart';
import 'package:fracture_buddy/web/repository.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http_with_interceptor.dart';

class WebService extends Repository {
  static Repository _instance;
  HttpWithInterceptor _httpInterceptor;

  //End Points
  static const String SEND_OTP = "sendOtp";
  static const String VERIFY_OTP = "verifyOtp";
  static const String GET_SERVICE_LIST = "getServiceList";
  static const String SIGN_UP = "signUp";
  static const String GET_USER_PROFILE = "getUserProfile";
  static const String EDIT_USER_PROFILE = "editUser";
  static const String GET_ALL_SURGEONS = "getSurgeons";
  static const String GET_SURGEONS_ASSISTANT = "getSurgeonAssistant";
  static const String GET_PARENT_CLASSIFICATION = "getClassificationParent";
  static const String GET_CHILD_CLASSIFICATION = "getClassificationChilds";
  static const String ADD_SURGICAL_FORM = "addSurgicalForm";
  static const String ALL_SURGICAL_FORM = "allSurgicalForm";

  static WebService getInstance() {
    if (_instance == null) _instance = new WebService();
    return _instance;
  }

  WebService() {
    _httpInterceptor =
        HttpWithInterceptor.build(interceptors: [LoggingInterceptor()]);
  }

  static const String SOMETHING_WENT_WRONG_CHECK_INTERNET =
      "Something went wrong. Please check your internet connection or try again later.";

  static Map<String, dynamic> _errorResponse(String message) {
    return Map.of({"success": false, "message": message});
  }

  @override
  Future<SendOTPResponse> sendOtpRequest(String phone) async {
    SendOTPResponse response;
    try {
      Map<String, dynamic> requestBody = Map();
      requestBody["phone"] = phone;
      var res = await _httpInterceptor.post(APP_BASE_URL + SEND_OTP,
          body: requestBody);
      response = SendOTPResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = SendOTPResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = SendOTPResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<VerifyOtpResponse> verifyOtpRequest(VerifyOtpRequest request) async {
    VerifyOtpResponse response;
    try {
      final requestBody = json.encode(request);
      var res = await _httpInterceptor.post(APP_BASE_URL + VERIFY_OTP,
          body: requestBody,
          headers: {HttpHeaders.contentTypeHeader: "application/json"});
      response = VerifyOtpResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = VerifyOtpResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = VerifyOtpResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  Future<AllServicesResponse> getAllServiceList() async {
    AllServicesResponse response;
    try {
      var res = await _httpInterceptor.get(APP_BASE_URL + GET_SERVICE_LIST);
      response = AllServicesResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = AllServicesResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = AllServicesResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<RegisterUserResponse> registerUserRequest(
      RegisterUserRequest request, File profilePic) async {
    RegisterUserResponse response;
    try {
      var uri = Uri.parse(APP_BASE_URL + SIGN_UP);
      MultipartRequest requestBody = new MultipartRequest('POST', uri);
      //file
      if (profilePic != null) {
        var multipartFile =
            await MultipartFile.fromPath("profilePic", profilePic.path);
        requestBody.files.add(multipartFile);
      }
      //data
      final dataString = json.encode(request);
      requestBody.fields["data"] = dataString;
      print("Request: ${requestBody.url} ${requestBody.fields.toString()}");
      var resp = await requestBody.send();
      print("Data: ${resp.request.url} ${resp.statusCode}");
      if (resp.statusCode == 200) {
        final res = await resp.stream.bytesToString();
        print("Response: $res");
        Map parsed = json.decode(res);
        response = RegisterUserResponse.fromJson(parsed);
      } else {
        final res = await resp.stream.bytesToString();
        print(json.decode(res));
        response =
            RegisterUserResponse.fromJson(_errorResponse("Upload Failed"));
      }
    } catch (e) {
      if (e is SocketException) {
        response = RegisterUserResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = RegisterUserResponse.fromJson(_errorResponse(e.toString()));
      }
      print("Web Error: $e");
    }
    return response;
  }

  Future<RegisterUserResponse> getProfileData(String authToken) async {
    RegisterUserResponse response;
    try {
      var res = await _httpInterceptor.get(APP_BASE_URL + GET_USER_PROFILE,
          headers: {HttpHeaders.authorizationHeader: authToken});
      response = RegisterUserResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = RegisterUserResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = RegisterUserResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<RegisterUserResponse> updateUserRequest(
      String authToken, RegisterUserRequest request, File profilePic) async {
    RegisterUserResponse response;
    try {
      var uri = Uri.parse(APP_BASE_URL + EDIT_USER_PROFILE);
      MultipartRequest requestBody = new MultipartRequest('POST', uri);
      //headers
      requestBody.headers.addAll({HttpHeaders.authorizationHeader: authToken});
      //file
      if (profilePic != null) {
        var multipartFile =
            await MultipartFile.fromPath("profilePic", profilePic.path);
        requestBody.files.add(multipartFile);
      }
      //data
      final dataString = json.encode(request);
      requestBody.fields["data"] = dataString;
      print("Request: ${requestBody.url} ${requestBody.fields.toString()}");
      var resp = await requestBody.send();
      print("Data: ${resp.request.url} ${resp.statusCode}");
      if (resp.statusCode == 200) {
        final res = await resp.stream.bytesToString();
        print("Response: $res");
        Map parsed = json.decode(res);
        response = RegisterUserResponse.fromJson(parsed);
      } else {
        final res = await resp.stream.bytesToString();
        print(json.decode(res));
        response =
            RegisterUserResponse.fromJson(_errorResponse("Upload Failed"));
      }
    } catch (e) {
      if (e is SocketException) {
        response = RegisterUserResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = RegisterUserResponse.fromJson(_errorResponse(e.toString()));
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<AllSurgeonResponse> getAllSurgeon(String authToken) async {
    AllSurgeonResponse response;
    try {
      var res = await _httpInterceptor.get(APP_BASE_URL + GET_ALL_SURGEONS,
          headers: {HttpHeaders.authorizationHeader: authToken});
      response = AllSurgeonResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = AllSurgeonResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = AllSurgeonResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<GetAssistantResponse> getSurgeonAssistant(
      String authToken, String surgeonId) async {
    GetAssistantResponse response;
    try {
      var res = await _httpInterceptor.get(
        APP_BASE_URL + GET_SURGEONS_ASSISTANT,
        params: {"surgeonId": surgeonId},
        headers: {HttpHeaders.authorizationHeader: authToken},
      );
      response = GetAssistantResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = GetAssistantResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = GetAssistantResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<AllClassResponse> getAllClass(String authToken) async {
    AllClassResponse response;
    try {
      var res = await _httpInterceptor.get(
        APP_BASE_URL + GET_PARENT_CLASSIFICATION,
        headers: {HttpHeaders.authorizationHeader: authToken},
      );
      response = AllClassResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = AllClassResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = AllClassResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<AllClassResponse> getAllClassChildren(String authToken, String parentId) async {
    AllClassResponse response;
    try {
      var res = await _httpInterceptor.get(
        APP_BASE_URL + GET_CHILD_CLASSIFICATION,
        params: {"parentId": parentId},
        headers: {HttpHeaders.authorizationHeader: authToken},
      );
      response = AllClassResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = AllClassResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = AllClassResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<CommonSuccessResponse> addSurgicalForm(String authToken, AddSurgicalFormRequest formRequest, File speciality, File diagnosis) async {
    CommonSuccessResponse response;
    try {
      var uri = Uri.parse(APP_BASE_URL + ADD_SURGICAL_FORM);
      MultipartRequest requestBody = new MultipartRequest('POST', uri);
      //headers
      requestBody.headers.addAll({HttpHeaders.authorizationHeader: authToken});
      //speciality
      if (speciality != null) {
        var multipartFile = await MultipartFile.fromPath("speciality", speciality.path);
        requestBody.files.add(multipartFile);
      }
      //speciality
      if (diagnosis != null) {
        var multipartFile = await MultipartFile.fromPath("diagnosis", diagnosis.path);
        requestBody.files.add(multipartFile);
      }
      //data
      // final dataString = json.encode(request);
      requestBody.fields["firstName"] = formRequest.firstName;
      requestBody.fields["lastName"] = formRequest.lastName;
      requestBody.fields["dateOfBirth"] = formRequest.dateOfBirth.toString();
      requestBody.fields["patientLocation"] = formRequest.patientLocation;
      requestBody.fields["surgeon"] = formRequest.surgeon;
      requestBody.fields["assistant"] = formRequest.assistant;
      requestBody.fields["classId"] = formRequest.classId;
      requestBody.fields["operationType"] = formRequest.operationType;
      requestBody.fields["notes"] = formRequest.notes;
      requestBody.fields["lat"] = formRequest.lat.toString();
      requestBody.fields["long"] = formRequest.long.toString();
      print("Request: ${requestBody.url} ${requestBody.fields.toString()}");
      var resp = await requestBody.send();
      print("Data: ${resp.request.url} ${resp.statusCode}");
      if (resp.statusCode == 200) {
        final res = await resp.stream.bytesToString();
        print("Response: $res");
        Map parsed = json.decode(res);
        response = CommonSuccessResponse.fromJson(parsed);
      } else {
        final res = await resp.stream.bytesToString();
        print(json.decode(res));
        response =
            CommonSuccessResponse.fromJson(_errorResponse("Upload Failed"));
      }
    } catch (e) {
      if (e is SocketException) {
        response = CommonSuccessResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = CommonSuccessResponse.fromJson(_errorResponse(e.toString()));
      }
      print("Web Error: $e");
    }
    return response;
  }

  @override
  Future<AllSurgicalFormResponse> allSurgicalForm(String authToken, num skip) async {
    AllSurgicalFormResponse response;
    try {
      var res = await _httpInterceptor.post(
        APP_BASE_URL + ALL_SURGICAL_FORM,
        headers: {HttpHeaders.authorizationHeader: authToken},
        // body: {"skip": skip},
      );
      response = AllSurgicalFormResponse.fromJson(json.decode(res.body));
    } catch (e) {
      if (e is SocketException) {
        response = AllSurgicalFormResponse.fromJson(
            _errorResponse(SOMETHING_WENT_WRONG_CHECK_INTERNET));
      } else {
        response = AllSurgicalFormResponse();
        response.success = false;
        response.message = e.toString();
      }
      print("Web Error: $e");
    }
    return response;
  }
}
