import 'dart:io';
import 'package:fracture_buddy/models/CommonSuccessResponse.dart';
import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allSurgeon/AllSurgeonResponse.dart';
import 'package:fracture_buddy/models/getAssistants/GetAssistantResponse.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserRequest.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserResponse.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/models/serviceList/AllServicesResponse.dart';
import 'package:fracture_buddy/models/surgicalForm/AddSurgicalFormRequest.dart';
import 'package:fracture_buddy/models/surgicalForm/AllSurgicalFormResponse.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpRequest.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpResponse.dart';

abstract class Repository {
  Future<SendOTPResponse> sendOtpRequest(String phone);

  Future<VerifyOtpResponse> verifyOtpRequest(VerifyOtpRequest request);

  Future<AllServicesResponse> getAllServiceList();

  Future<RegisterUserResponse> registerUserRequest(
      RegisterUserRequest request, File profilePic);

  Future<RegisterUserResponse> getProfileData(String authToken);

  Future<RegisterUserResponse> updateUserRequest(
      String authToken, RegisterUserRequest request, File profilePic);

  Future<AllSurgeonResponse> getAllSurgeon(String authToken);

  Future<GetAssistantResponse> getSurgeonAssistant(
      String authToken, String surgeonId);

  Future<AllClassResponse> getAllClass(String authToken);

  Future<AllClassResponse> getAllClassChildren(
      String authToken, String parentId);

  Future<CommonSuccessResponse> addSurgicalForm(String authToken,
      AddSurgicalFormRequest formRequest, File speciality, File diagnosis);

  Future<AllSurgicalFormResponse> allSurgicalForm(String authToken, num skip);
}
