import 'package:fracture_buddy/models/registerUser/User.dart';

class AllSurgeonResponse {
    List<User> data;
    String message;
    bool success;

    AllSurgeonResponse({this.data, this.message, this.success});

    factory AllSurgeonResponse.fromJson(Map<String, dynamic> json) {
        return AllSurgeonResponse(
            data: json['data'] != null ? (json['data'] as List).map((i) => User.fromJson(i)).toList() : null,
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toJson()).toList();
        }
        return data;
    }
}