class ClassData {
    String id1;
    String createdAt;
    String id;
    bool isDeleted;
    String legend;
    String parentId;
    String subgroup;
    String updatedAt;
    String image;

    ClassData({this.id1, this.createdAt, this.id, this.isDeleted, this.legend, this.parentId, this.subgroup, this.updatedAt, this.image});

    factory ClassData.fromJson(Map<String, dynamic> json) {
        return ClassData(
            id1: json['_id'],
            createdAt: json['createdAt'], 
            id: json['id'], 
            isDeleted: json['isDeleted'], 
            legend: json['legend'],
            parentId: json['parentId'],
            subgroup: json['subgroup'],
            updatedAt: json['updatedAt'],
            image: json['image'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['isDeleted'] = this.isDeleted;
        data['legend'] = this.legend;
        data['parentId'] = this.parentId;
        data['subgroup'] = this.subgroup;
        data['updatedAt'] = this.updatedAt;
        data['image'] = this.image;
        return data;
    }
}