import 'ClassData.dart';

class AllClassResponse {
  List<ClassData> data;
  String message;
  bool success;

  AllClassResponse({this.data, this.message, this.success});

  factory AllClassResponse.fromJson(Map<String, dynamic> json) {
    return AllClassResponse(
      data: json['data'] != null
          ? (json['data'] as List).map((i) => ClassData.fromJson(i)).toList()
          : null,
      message: json['message'],
      success: json['success'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {data['data'] = this.data.map((v) => v.toJson()).toList();}
    return data;
  }
}
