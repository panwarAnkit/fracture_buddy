import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';

class BoneClassData {
    List<ClassData> classList;
    String selectedClassId;

    BoneClassData({this.classList, this.selectedClassId});

    factory BoneClassData.fromJson(Map<String, dynamic> json) {
        return BoneClassData(
            classList: json['classList'] != null ? (json['classList'] as List).map((i) => ClassData.fromJson(i)).toList() : null,
            selectedClassId: json['selectedClassId']
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        if (this.classList != null) {data['classList'] = this.classList.map((v) => v.toJson()).toList();}
        data['selectedClassId'] = this.selectedClassId;
        return data;
    }
}