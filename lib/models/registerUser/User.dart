import 'package:fracture_buddy/models/serviceList/College.dart';
import 'package:fracture_buddy/models/serviceList/Hospital.dart';
import 'package:fracture_buddy/models/serviceList/Speciality.dart';

class User {
    String id1;
    College college;
    String createdAt;
    String deviceId;
    String email;
    String firstName;
    List<Hospital> hospital;
    List<Speciality> speciality;
    String id;
    bool isApproved;
    String lastName;
    String phone;
    String profilePic;
    String updatedAt;
    String userName;
    String token;
    String countryCode;

    User({this.id1, this.college, this.createdAt, this.countryCode, this.deviceId, this.email, this.firstName, this.hospital, this.speciality, this.id, this.isApproved, this.lastName, this.phone, this.profilePic, this.updatedAt, this.userName, this.token});

    factory User.fromJson(Map<String, dynamic> json) {
        return User(
            id1: json['_id'],
            college: json['college'] != null ? College.fromJson(json['college']) : null,
            createdAt: json['createdAt'],
            countryCode: json['countryCode'],
            deviceId: json['deviceId'],
            email: json['email'],
            firstName: json['firstName'],
            hospital: json['hospital'] != null ? (json['hospital'] as List).map((i) => Hospital.fromJson(i)).toList() : null,
            speciality: json['speciality'] != null ? (json['speciality'] as List).map((i) => Speciality.fromJson(i)).toList() : null,
            id: json['id'],
            isApproved: json['isApproved'], 
            lastName: json['lastName'], 
            phone: json['phone'], 
            profilePic: json['profilePic'], 
            updatedAt: json['updatedAt'], 
            userName: json['userName'],
            token: json['token'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        if (this.college != null) data['college'] = this.college.toJson();
        data['createdAt'] = this.createdAt;
        data['deviceId'] = this.deviceId;
        data['email'] = this.email;
        data['firstName'] = this.firstName;
        data['countryCode'] = this.countryCode;
        if (this.hospital != null) {
            data['hospital'] = this.hospital.map((v) => v.toJson()).toList();
        }
        if (this.speciality != null) {
            data['speciality'] = this.speciality.map((v) => v.toJson()).toList();
        }
        data['id'] = this.id;
        data['isApproved'] = this.isApproved;
        data['lastName'] = this.lastName;
        data['phone'] = this.phone;
        data['profilePic'] = this.profilePic;
        data['updatedAt'] = this.updatedAt;
        data['userName'] = this.userName;
        data['token'] = this.token;
        return data;
    }
}