class RegisterUserRequest {
  String college;
  String deviceId;
  String email;
  String firstName;
  List<String> hospital;
  List<String> speciality;
  String lastName;
  String phone;
  String userName;
  String countryCode;

  RegisterUserRequest(
      {this.speciality,
      this.countryCode,
      this.college,
      this.deviceId,
      this.email,
      this.firstName,
      this.hospital,
      this.lastName,
      this.phone,
      this.userName});

  factory RegisterUserRequest.fromJson(Map<String, dynamic> json) {
    return RegisterUserRequest(
      college: json['college'],
      countryCode: json['countryCode'],
      deviceId: json['deviceId'],
      email: json['email'],
      firstName: json['firstName'],
      hospital: json['hospital'] != null
          ? new List<String>.from(json['hospital'])
          : null,
      speciality: json['speciality'] != null
          ? new List<String>.from(json['speciality'])
          : null,
      lastName: json['lastName'],
      phone: json['phone'],
      userName: json['userName'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['college'] = this.college;
    data['deviceId'] = this.deviceId;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    if (this.hospital != null) data['hospital'] = this.hospital;
    if (this.speciality != null) data['speciality'] = this.speciality;
    data['lastName'] = this.lastName;
    data['countryCode'] = this.countryCode;
    data['phone'] = this.phone;
    data['userName'] = this.userName;
    return data;
  }
}
