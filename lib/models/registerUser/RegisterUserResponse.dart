import '../registerUser/User.dart';

class RegisterUserResponse {
    String message;
    bool success;
    User data;

    RegisterUserResponse({this.message, this.success, this.data});

    factory RegisterUserResponse.fromJson(Map<String, dynamic> json) {
        return RegisterUserResponse(
            message: json['message'],
            success: json['success'],
            data: json['data'] != null ? User.fromJson(json['data']) : null,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) data['data'] = this.data.toJson();
        return data;
    }
}