import 'AssistantData.dart';

class GetAssistantResponse {
    List<AssistantData> data;
    String message;
    bool success;

    GetAssistantResponse({this.data, this.message, this.success});

    factory GetAssistantResponse.fromJson(Map<String, dynamic> json) {
        return GetAssistantResponse(
            data: json['data'] != null ? (json['data'] as List).map((i) => AssistantData.fromJson(i)).toList() : null,
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toJson()).toList();
        }
        return data;
    }
}