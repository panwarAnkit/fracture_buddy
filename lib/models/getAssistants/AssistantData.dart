class AssistantData {
    String id1;
    String assistantName;
    String createdAt;
    String id;
    bool isDeleted;
    String surgeonId;
    String updatedAt;

    AssistantData({this.id1, this.assistantName, this.createdAt, this.id, this.isDeleted, this.surgeonId, this.updatedAt});

    factory AssistantData.fromJson(Map<String, dynamic> json) {
        return AssistantData(
            id1: json['_id'],
            assistantName: json['assistantName'], 
            createdAt: json['createdAt'], 
            id: json['id'], 
            isDeleted: json['isDeleted'], 
            surgeonId: json['surgeonId'], 
            updatedAt: json['updatedAt'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['assistantName'] = this.assistantName;
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['isDeleted'] = this.isDeleted;
        data['surgeonId'] = this.surgeonId;
        data['updatedAt'] = this.updatedAt;
        return data;
    }
}