class VerifyOtpRequest {
    String otp;
    String otpId;
    String phone;

    VerifyOtpRequest({this.otp, this.otpId, this.phone});

    factory VerifyOtpRequest.fromJson(Map<String, dynamic> json) {
        return VerifyOtpRequest(
            otp: json['otp'], 
            otpId: json['otpId'],
            phone: json['phone'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['otp'] = this.otp;
        data['otpId'] = this.otpId;
        data['phone'] = this.phone;
        return data;
    }
}