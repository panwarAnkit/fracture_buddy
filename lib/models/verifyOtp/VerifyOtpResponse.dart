import '../registerUser/User.dart';

class VerifyOtpResponse {
    String message;
    bool newUser;
    bool success;
    User user;

    VerifyOtpResponse({this.message, this.newUser, this.success, this.user});

    factory VerifyOtpResponse.fromJson(Map<String, dynamic> json) {
        return VerifyOtpResponse(
            message: json['message'], 
            newUser: json['newUser'], 
            success: json['success'], 
            user: json['user'] != null ? User.fromJson(json['user']) : null, 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['newUser'] = this.newUser;
        data['success'] = this.success;
        if (this.user != null) data['user'] = this.user.toJson();
        return data;
    }
}