class OTPData {
    int otp;
    String otpId;
    String phone;

    OTPData({this.otp, this.otpId, this.phone});

    factory OTPData.fromJson(Map<String, dynamic> json) {
        return OTPData(
            otp: json['otp'], 
            otpId: json['otpId'],
            phone: json['phone'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['otp'] = this.otp;
        data['otpId'] = this.otpId;
        data['phone'] = this.phone;
        return data;
    }
}