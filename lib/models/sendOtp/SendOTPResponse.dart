import 'OTPData.dart';

class SendOTPResponse {
    OTPData data;
    String message;
    bool success;

    SendOTPResponse({this.data, this.message, this.success});

    factory SendOTPResponse.fromJson(Map<String, dynamic> json) {
        return SendOTPResponse(
            data: json['data'] != null ? OTPData.fromJson(json['data']) : null,
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) data['data'] = this.data.toJson();
        return data;
    }
}