class LocationData {
  String address;
  String placeId;
  double lat;
  double long;

  LocationData({this.address, this.placeId, this.lat, this.long});

  factory LocationData.fromJson(Map<String, dynamic> json) {
    return LocationData(
      address: json['address'],
      placeId: json['placeId'],
      lat: json['lat'],
      long: json['long'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['placeId'] = this.placeId;
    data['lat'] = this.lat;
    data['long'] = this.long;
    return data;
  }
}
