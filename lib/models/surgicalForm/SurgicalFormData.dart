import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/models/getAssistants/AssistantData.dart';
import 'package:fracture_buddy/models/registerUser/User.dart';

class SurgicalFormData {
    String id1;
    ClassData classId;
    User addedBy;
    AssistantData assistant;
    String createdAt;
    String diagnosis;
    String firstName;
    String id;
    String lastName;
    num lat;
    num long;
    num dateOfBirth;
    String operationType;
    String patientLocation;
    String speciality;
    ClassData subclassId;
    User surgeon;
    String updatedAt;
    String notes;

    SurgicalFormData({this.id1, this.classId, this.addedBy, this.assistant, this.createdAt, this.diagnosis, this.firstName, this.id, this.lastName, this.lat, this.long,this.dateOfBirth, this.operationType, this.patientLocation, this.speciality, this.subclassId, this.surgeon, this.updatedAt, this.notes});

    factory SurgicalFormData.fromJson(Map<String, dynamic> json) {
        return SurgicalFormData(
        id1: json['_id'],
            classId: json['classId'] != null ? ClassData.fromJson(json['classId']) : null,
            addedBy: json['addedBy'] != null ? User.fromJson(json['addedBy']) : null,
            assistant: json['assistant'] != null ? AssistantData.fromJson(json['assistant']) : null,
            createdAt: json['createdAt'], 
            diagnosis: json['diagnosis'], 
            firstName: json['firstName'], 
            id: json['id'], 
            lastName: json['lastName'], 
            lat: json['lat'], 
            long: json['long'],
            dateOfBirth: json['dateOfBirth'],
            operationType: json['operationType'],
            patientLocation: json['patientLocation'], 
            speciality: json['speciality'],
            subclassId: json['subclassId'] != null ? ClassData.fromJson(json['subclassId']) : null,
            surgeon: json['surgeon'] != null ? User.fromJson(json['surgeon']) : null,
            updatedAt: json['updatedAt'],
            notes: json['notes'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['createdAt'] = this.createdAt;
        data['diagnosis'] = this.diagnosis;
        data['firstName'] = this.firstName;
        data['id'] = this.id;
        data['lastName'] = this.lastName;
        data['lat'] = this.lat;
        data['long'] = this.long;
        data['dateOfBirth'] = this.dateOfBirth;
        data['operationType'] = this.operationType;
        data['patientLocation'] = this.patientLocation;
        data['speciality'] = this.speciality;
        data['updatedAt'] = this.updatedAt;
        data['notes'] = this.notes;
        if (this.classId != null) {
            data['classId'] = this.classId.toJson();
        }
        if (this.addedBy != null) {
            data['addedBy'] = this.addedBy.toJson();
        }
        if (this.assistant != null) {
            data['assistant'] = this.assistant.toJson();
        }
        if (this.subclassId != null) {
            data['subclassId'] = this.subclassId.toJson();
        }
        if (this.surgeon != null) {
            data['surgeon'] = this.surgeon.toJson();
        }
        return data;
    }
}