import 'package:fracture_buddy/models/surgicalForm/SurgicalFormData.dart';

class AllSurgicalFormResponse {
    String message;
    bool success;
    List<SurgicalFormData> data;

    AllSurgicalFormResponse({this.message, this.success, this.data});

    factory AllSurgicalFormResponse.fromJson(Map<String, dynamic> json) {
        return AllSurgicalFormResponse(
            message: json['message'], 
            success: json['success'],
            data: json['data'] != null ? (json['data'] as List).map((i) => SurgicalFormData.fromJson(i)).toList() : null,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toJson()).toList();
        }
        return data;
    }
}