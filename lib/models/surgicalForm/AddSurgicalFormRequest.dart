class AddSurgicalFormRequest {
    String classId;
    String assistant;
    String firstName;
    String lastName;
    num lat;
    num long;
    num dateOfBirth;
    String operationType;
    String patientLocation;
    String notes;
    String surgeon;

    AddSurgicalFormRequest({this.classId, this.assistant, this.firstName, this.lastName, this.lat, this.long, this.dateOfBirth, this.operationType, this.patientLocation, this.notes, this.surgeon});

    factory AddSurgicalFormRequest.fromJson(Map<String, dynamic> json) {
        return AddSurgicalFormRequest(
            classId: json['classId'],
            assistant: json['assistant'], 
            firstName: json['firstName'], 
            lastName: json['lastName'], 
            lat: json['lat'], 
            long: json['long'],
            dateOfBirth: json['dateOfBirth'],
            operationType: json['operationType'],
            patientLocation: json['patientLocation'],
            notes: json['notes'],
            surgeon: json['surgeon'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['classId'] = this.classId;
        data['assistant'] = this.assistant;
        data['firstName'] = this.firstName;
        data['lastName'] = this.lastName;
        data['lat'] = this.lat;
        data['long'] = this.long;
        data['dateOfBirth'] = this.dateOfBirth;
        data['operationType'] = this.operationType;
        data['patientLocation'] = this.patientLocation;
        data['notes'] = this.notes;
        data['surgeon'] = this.surgeon;
        return data;
    }
}