class Hospital {
    String id1;
    String createdAt;
    String hospitalName;
    String id;
    bool isDeleted;
    String updatedAt;

    Hospital({this.id1, this.createdAt, this.hospitalName, this.id, this.isDeleted, this.updatedAt});

    factory Hospital.fromJson(Map<String, dynamic> json) {
        return Hospital(
            id1: json['_id'],
            createdAt: json['createdAt'], 
            hospitalName: json['hospitalName'], 
            id: json['id'], 
            isDeleted: json['isDeleted'], 
            updatedAt: json['updatedAt'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['createdAt'] = this.createdAt;
        data['hospitalName'] = this.hospitalName;
        data['id'] = this.id;
        data['isDeleted'] = this.isDeleted;
        data['updatedAt'] = this.updatedAt;
        return data;
    }
}