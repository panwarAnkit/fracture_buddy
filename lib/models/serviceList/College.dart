class College {
    String id1;
    String collegeCode;
    String collegeName;
    String createdAt;
    String id;
    bool isDeleted;
    String updatedAt;

    College({this.id1, this.collegeCode, this.collegeName, this.createdAt, this.id, this.isDeleted, this.updatedAt});

    factory College.fromJson(Map<String, dynamic> json) {
        return College(
            id1: json['_id'],
            collegeCode: json['collegeCode'], 
            collegeName: json['collegeName'], 
            createdAt: json['createdAt'], 
            id: json['id'], 
            isDeleted: json['isDeleted'], 
            updatedAt: json['updatedAt'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['collegeCode'] = this.collegeCode;
        data['collegeName'] = this.collegeName;
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['isDeleted'] = this.isDeleted;
        data['updatedAt'] = this.updatedAt;
        return data;
    }
}