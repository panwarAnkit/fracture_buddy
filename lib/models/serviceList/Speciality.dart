class Speciality {
    String id1;
    String createdAt;
    String id;
    bool isDeleted;
    String name;
    String updatedAt;

    Speciality({this.id1, this.createdAt, this.id, this.isDeleted, this.name, this.updatedAt});

    factory Speciality.fromJson(Map<String, dynamic> json) {
        return Speciality(
            id1: json['_id'],
            createdAt: json['createdAt'], 
            id: json['id'], 
            isDeleted: json['isDeleted'], 
            name: json['name'], 
            updatedAt: json['updatedAt'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id1;
        data['createdAt'] = this.createdAt;
        data['id'] = this.id;
        data['isDeleted'] = this.isDeleted;
        data['name'] = this.name;
        data['updatedAt'] = this.updatedAt;
        return data;
    }
}