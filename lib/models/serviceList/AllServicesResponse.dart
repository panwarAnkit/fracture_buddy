import 'College.dart';
import 'Hospital.dart';
import 'Speciality.dart';

class AllServicesResponse {
    List<College> college;
    List<Hospital> hospitals;
    String message;
    List<Speciality> speciality;
    bool success;

    AllServicesResponse({this.college, this.hospitals, this.message, this.speciality, this.success});

    factory AllServicesResponse.fromJson(Map<String, dynamic> json) {
        return AllServicesResponse(
            college: json['college'] != null ? (json['college'] as List).map((i) => College.fromJson(i)).toList() : null, 
            hospitals: json['hospitals'] != null ? (json['hospitals'] as List).map((i) => Hospital.fromJson(i)).toList() : null, 
            message: json['message'], 
            speciality: json['speciality'] != null ? (json['speciality'] as List).map((i) => Speciality.fromJson(i)).toList() : null, 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        if (this.college != null) {
            data['college'] = this.college.map((v) => v.toJson()).toList();
        }
        if (this.hospitals != null) {
            data['hospitals'] = this.hospitals.map((v) => v.toJson()).toList();
        }
        if (this.speciality != null) {
            data['speciality'] = this.speciality.map((v) => v.toJson()).toList();
        }
        return data;
    }

}