class CommonSuccessResponse {
    String message;
    bool success;

    CommonSuccessResponse({this.message, this.success});

    factory CommonSuccessResponse.fromJson(Map<String, dynamic> json) {
        return CommonSuccessResponse(
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        return data;
    }
}