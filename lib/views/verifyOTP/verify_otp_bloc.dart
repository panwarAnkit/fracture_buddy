import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/sendOtp/OTPData.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpRequest.dart';
import 'package:fracture_buddy/models/verifyOtp/VerifyOtpResponse.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';

class VerifyOTPBloc extends BaseBloc {
  AnimationController _controller;
  Timer _otpExpiryTimer;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation;

  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<SendOTPResponse> _sendOtpResponseController;
  BehaviorSubject<VerifyOtpResponse> _verifyOtpResponseController;
  BehaviorSubject<int> _otpTimerController;

  VerifyOTPBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 600),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingTopRightToCenterAnimation =
        Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    _loadingController = BehaviorSubject.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _sendOtpResponseController = BehaviorSubject();
    _verifyOtpResponseController = BehaviorSubject();
    _otpTimerController = BehaviorSubject.seeded(60);
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<SendOTPResponse> get sendOtpResponseStream =>
      _sendOtpResponseController.stream;

  Stream<VerifyOtpResponse> get verifyOtpResponseStream =>
      _verifyOtpResponseController.stream;

  Stream<int> get otpTimerStream => _otpTimerController.stream;

  TickerFuture startAnimationForward() {
    return _controller.forward();
  }

  TickerFuture startAnimationReverse() {
    return _controller.reverse();
  }

  void startOtpTimer() {
    _otpExpiryTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      _otpTimerController.sink.add(60-timer.tick);
      if(timer.tick==60) timer.cancel();
    });
  }

  void onClickResendOTP(BuildContext context, OTPData otpData) async {
    FocusScope.of(context).requestFocus(FocusNode());
    _loadingController.sink.add(true);
    final response =
        await WebService.getInstance().sendOtpRequest(otpData.phone);
    _loadingController.sink.add(false);
    if (response.success) {
      _sendOtpResponseController.add(response);
    } else {
      _errorMessageController.sink.add(response.message);
    }
  }

  void onClickVerifyOTP(BuildContext context, OTPData otpData, String otp1,
      String otp2, String otp3, String otp4) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (otp1.isEmpty || otp2.isEmpty || otp3.isEmpty || otp4.isEmpty) {
      _errorMessageController.sink.add("Please enter OTP first");
    } else {
      _loadingController.sink.add(true);
      final request = VerifyOtpRequest(
        otpId: otpData.otpId,
        phone: otpData.phone,
        otp: "$otp1$otp2$otp3$otp4",
      );
      final response = await WebService.getInstance().verifyOtpRequest(request);
      _loadingController.sink.add(false);
      if (response.success) {
        _verifyOtpResponseController.add(response);
      } else {
        _errorMessageController.sink.add(response.message);
      }
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _loadingController.close();
    _errorMessageController.close();
    _otpExpiryTimer.cancel();
    _otpTimerController.close();
  }
}
