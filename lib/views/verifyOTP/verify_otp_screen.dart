import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fracture_buddy/models/sendOtp/OTPData.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/no_grow_behavior.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/views/verifyOTP/verify_otp_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class VerifyOTPScreen extends StatefulWidget {
  OTPData otpData;

  VerifyOTPScreen(this.otpData);

  @override
  State<VerifyOTPScreen> createState() => VerifyOTPScreenState();
}

class VerifyOTPScreenState extends State<VerifyOTPScreen>
    with SingleTickerProviderStateMixin {
  VerifyOTPBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode otp1 = FocusNode();
  FocusNode otp2 = FocusNode();
  FocusNode otp3 = FocusNode();
  FocusNode otp4 = FocusNode();
  TextEditingController otp1Controller,
      otp2Controller,
      otp3Controller,
      otp4Controller;

  @override
  void initState() {
    otp1Controller = TextEditingController();
    otp2Controller = TextEditingController();
    otp3Controller = TextEditingController();
    otp4Controller = TextEditingController();
    _bloc = VerifyOTPBloc(this);
    _initializeCommonListener(context);
    _bloc.startAnimationForward();
    _bloc.startOtpTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    FrequentUtils.getInstance().showToast(widget.otpData.otp.toString());
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: SlideTransition(
                  position: _bloc.slidingTopRightToCenterAnimation,
                  child: Image.asset(
                    "assets/png/bg_top_right.png",
                    height: screenSize.width / 1.2,
                    width: screenSize.width / 1.59,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(25.0),
                child: Center(
                  child: ScrollConfiguration(
                    behavior: NoGrowBehavior(),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        SlideTransition(
                          position: _bloc.slidingLeftToRightAnimation,
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              Text("Need to verify\nyour phone",
                                  style: textStyleBlueAvenikBold26),
                              SizedBox(height: 30.0),
                              Text("We’ve sent you SMS with a code to number",
                                  style: textStyleGreyQuicksandBold16),
                              Text(
                                  FrequentUtils.getInstance()
                                      .getFormattedPhoneNumber(
                                          widget.otpData.phone),
                                  style: textStyleQuicksandBlueBold16),
                            ],
                          ),
                        ),
                        SizedBox(height: 40.0),
                        SlideTransition(
                          position: _bloc.slidingRightToLeftAnimation,
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    color: COLOR_LIGHT_GRAY,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8.0),
                                    ),
                                  ),
                                  child: Center(
                                    child: TextField(
                                      style: textStyleQuicksandBoldBlack26,
                                      keyboardType: TextInputType.number,
                                      maxLength: 1,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                      ],
                                      focusNode: otp1,
                                      controller: otp1Controller,
                                      onChanged: (s) {
                                        if (s.length == 1) {
                                          FocusScope.of(context)
                                              .requestFocus(otp2);
                                        }
                                      },
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(8.0),
                                          counterText: "",
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintText: "1",
                                          hintStyle:
                                              textStyleQuicksandLightGrey26),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    color: COLOR_LIGHT_GRAY,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  child: Center(
                                    child: TextField(
                                      style: textStyleQuicksandBoldBlack26,
                                      keyboardType: TextInputType.number,
                                      maxLength: 1,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                      ],
                                      focusNode: otp2,
                                      controller: otp2Controller,
                                      onChanged: (s) {
                                        if (s.length == 0) {
                                          FocusScope.of(context)
                                              .requestFocus(otp1);
                                        } else {
                                          FocusScope.of(context)
                                              .requestFocus(otp3);
                                        }
                                      },
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(8.0),
                                          counterText: "",
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintText: "2",
                                          hintStyle:
                                              textStyleQuicksandLightGrey26),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    color: COLOR_LIGHT_GRAY,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  child: Center(
                                    child: TextField(
                                      style: textStyleQuicksandBoldBlack26,
                                      keyboardType: TextInputType.number,
                                      maxLength: 1,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                      ],
                                      focusNode: otp3,
                                      controller: otp3Controller,
                                      onChanged: (s) {
                                        if (s.length == 0) {
                                          FocusScope.of(context)
                                              .requestFocus(otp2);
                                        } else {
                                          FocusScope.of(context)
                                              .requestFocus(otp4);
                                        }
                                      },
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(8.0),
                                        counterText: "",
                                        border: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        hintText: "3",
                                        hintStyle:
                                            textStyleQuicksandLightGrey26,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    color: COLOR_LIGHT_GRAY,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  child: Center(
                                    child: TextField(
                                      style: textStyleQuicksandBoldBlack26,
                                      keyboardType: TextInputType.number,
                                      maxLength: 1,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                      ],
                                      focusNode: otp4,
                                      controller: otp4Controller,
                                      onChanged: (s) {
                                        if (s.length == 0) {
                                          FocusScope.of(context)
                                              .requestFocus(otp3);
                                        } else {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        }
                                      },
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.all(8.0),
                                          counterText: "",
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintText: "4",
                                          hintStyle:
                                              textStyleQuicksandLightGrey26),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 35),
                        StreamBuilder<int>(
                          stream: _bloc.otpTimerStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData && snapshot.data > 1) {
                              return SlideTransition(
                                position: _bloc.slidingLeftToRightAnimation,
                                child: Row(
                                  children: [
                                    Text("The verify code will be expire in",
                                        style: textStyleGreyQuicksandBold16),
                                    SizedBox(width: 5),
                                    Text(
                                        "00:${FrequentUtils.getInstance().getTwoDigitNumber(snapshot.data)}",
                                        style: textStyleQuicksandBoldYellow16)
                                  ],
                                ),
                              );
                            } else {
                              return Center();
                            }
                          },
                        ),
                        SizedBox(height: 35),
                        SlideTransition(
                          position: _bloc.slidingLeftToRightAnimation,
                          child: Hero(
                            tag: APP_NAME,
                            child: Material(
                              elevation: 5.0,
                              color: APP_PRIMARY_COLOR,
                              shadowColor: APP_PRIMARY_COLOR_LIGHT,
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                              child: InkWell(
                                onTap: () => onClickVerifyOTP(context),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                                splashColor: APP_PRIMARY_COLOR_DARK,
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(width: 50),
                                        Text(
                                          "Verify OTP",
                                          style: textStyleWhite16,
                                          textAlign: TextAlign.center,
                                        ),
                                        SizedBox(width: 20.0),
                                        RotatedBox(
                                          quarterTurns: 45,
                                          child: Lottie.asset(
                                              'assets/json/arrow_up.json',
                                              width: 30,
                                              height: 30),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 35),
                        StreamBuilder<int>(
                          stream: _bloc.otpTimerStream,
                          builder: (context, snapshot) {
                            if(snapshot.hasData&&snapshot.data<=1) {
                              return SlideTransition(
                                position: _bloc.slidingRightToLeftAnimation,
                                child: Row(
                                  children: [
                                    Text("Didn\'t receive a code!",
                                        style: textStyleGreyQuicksandBold16),
                                    SizedBox(width: 2),
                                    InkWell(
                                      onTap: () =>
                                          _bloc.onClickResendOTP(
                                              context, widget.otpData),
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(4.0)),
                                      child: Padding(
                                        padding: EdgeInsets.all(4.0),
                                        child: Text("Resend Code",
                                            style: textStyleQuicksandBlueBoldUL16),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }else{
                              return Center();
                            }
                          }
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //send otp response
    _bloc.sendOtpResponseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        widget.otpData = response.data;
        FrequentUtils.getInstance().showToast(response.data.otp.toString());
        _bloc.startOtpTimer();
      }
    });
    //verify otp response
    _bloc.verifyOtpResponseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        if (response.newUser) {
          await _bloc.startAnimationReverse();
          await Navigator.of(context).pushReplacementNamed(
              AppRoute.completeProfile,
              arguments: widget.otpData.phone);
          _bloc.startAnimationForward();
        } else {
          await FrequentUtils.getInstance().saveUserData(response.user);
          await _bloc.startAnimationReverse();
          if (response.user.isApproved) {
            await Navigator.of(context)
                .pushNamedAndRemoveUntil(AppRoute.homeScreen, (_) => false);
          } else {
            await Navigator.of(context).pushNamedAndRemoveUntil(
                AppRoute.awaitingApproval, (_) => false);
          }
          _bloc.startAnimationForward();
        }
      }
    });
  }

  onClickVerifyOTP(BuildContext context) {
    _bloc.onClickVerifyOTP(context, widget.otpData, otp1Controller.text,
        otp2Controller.text, otp3Controller.text, otp4Controller.text);
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
