import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<StatefulWidget>
    with TickerProviderStateMixin {
  Animation _containerRadiusAnimation,
      _containerSizeAnimation,
      _containerColorAnimation;
  AnimationController _containerAnimationController;
  var logoAsset = "assets/png/app_logo.png";

  @override
  void initState() {
    super.initState();
    _containerAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000), reverseDuration: Duration(milliseconds: 600));
    final curve = CurvedAnimation(
        parent: _containerAnimationController, curve: Curves.ease);
    _containerRadiusAnimation = BorderRadiusTween(
            begin: BorderRadius.circular(100.0),
            end: BorderRadius.circular(0.0))
        .animate(curve);
    _containerSizeAnimation = Tween(begin: 0.0, end: 2.0).animate(curve);
    _containerColorAnimation =
        ColorTween(begin: APP_PRIMARY_COLOR, end: APP_PRIMARY_COLOR_DARK)
            .animate(curve);
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() => logoAsset = "assets/png/app_logo2.png");
    });
    _containerAnimationController.forward();
    Future.delayed(
      Duration(seconds: 3),
      () async {
        if (SharedPref.mInstance.isAutoLogin()) {
          await _containerAnimationController.reverse();
          if (SharedPref.mInstance.getBool(SharedPref.STORAGE_IS_APPROVED)) {
            await Navigator.of(context)
                .pushNamedAndRemoveUntil(AppRoute.homeScreen, (_) => false);
          } else {
            await Navigator.of(context)
                .pushNamedAndRemoveUntil(AppRoute.awaitingApproval, (_) => false);
          }
          _containerAnimationController.forward();
        } else {
          await _containerAnimationController.reverse();
          Navigator.of(context).pushNamedAndRemoveUntil(
              AppRoute.introScreen, (Route<dynamic> route) => false);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          child: Container(
            color: APP_PRIMARY_COLOR,
            child: Stack(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(100.0),
                    child: Image.asset("assets/png/app_logo.png"),
                  ),
                ),
                Center(
                  child: AnimatedBuilder(
                    animation: _containerAnimationController,
                    builder: (context, child) {
                      return Container(
                        width: _containerSizeAnimation.value * screenSize.height,
                        height: _containerSizeAnimation.value * screenSize.height,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                              image: ExactAssetImage('assets/png/splash_bg.png'),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: _containerRadiusAnimation.value,
                            color: _containerColorAnimation.value),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.all(100.0),
                            child: Image.asset(logoAsset),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _containerAnimationController?.dispose();
    super.dispose();
  }
}
