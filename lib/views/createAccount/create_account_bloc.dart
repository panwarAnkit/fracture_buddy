import 'dart:async';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class CreateAccountBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation;

  BehaviorSubject<Country> _countryPickerController;
  BehaviorSubject<String> _phoneController;
  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<SendOTPResponse> _responseController;

  CreateAccountBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 700),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingTopRightToCenterAnimation =
        Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    _countryPickerController =
        BehaviorSubject.seeded(Country(countryCode: "CA", phoneCode: "1"));
    _phoneController = BehaviorSubject.seeded("");
    _loadingController = BehaviorSubject.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _responseController = BehaviorSubject();
  }

  StreamSink<Country> get countryPickerSink => _countryPickerController.sink;

  StreamSink<String> get phoneSink => _phoneController.sink;

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  Stream<Country> get countryPickerStream => _countryPickerController.stream;

  Stream<String> get phoneStream => _phoneController.stream;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<SendOTPResponse> get responseStream => _responseController.stream;

  TickerFuture startAnimationForward() {
    return _controller.forward();
  }

  TickerFuture startAnimationReverse() {
    return _controller.reverse();
  }

  void sendOtpRequest(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (_phoneController.value.isNullOrEmpty()) {
      _errorMessageController.sink.add("Please enter phone number.");
    } else if (_phoneController.value.length <= 13) {
      _errorMessageController.sink.add("Please enter valid phone number.");
    } else {
      _loadingController.sink.add(true);
      final phone = FrequentUtils.getInstance().getPlanPhoneNumber(
          _countryPickerController.value.phoneCode, _phoneController.value);
      final response = await WebService.getInstance().sendOtpRequest(phone);
      _loadingController.sink.add(false);
      if (response.success) {
        _responseController.add(response);
      } else {
        _errorMessageController.sink.add(response.message);
      }
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _countryPickerController.close();
    _phoneController.close();
    _loadingController.close();
    _errorMessageController.close();
    _responseController.close();
  }
}
