import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/no_grow_behavior.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/utils/us_number_text_formatter.dart';
import 'package:fracture_buddy/views/createAccount/create_account_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class CreateAccountScreen extends StatefulWidget {
  @override
  State<CreateAccountScreen> createState() => CreateAccountScreenState();
}

class CreateAccountScreenState extends State<CreateAccountScreen>
    with SingleTickerProviderStateMixin {
  CreateAccountBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _bloc = CreateAccountBloc(this);
    _initializeCommonListener(context);
    _bloc.startAnimationForward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: SlideTransition(
                  position: _bloc.slidingTopRightToCenterAnimation,
                  child: Image.asset(
                    "assets/png/bg_top_right.png",
                    height: screenSize.width / 1.2,
                    width: screenSize.width / 1.59,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(25.0),
                child: Center(
                  child: ScrollConfiguration(
                    behavior: NoGrowBehavior(),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        SlideTransition(
                          position: _bloc.slidingLeftToRightAnimation,
                          child: ListView(
                            shrinkWrap: true,
                            children: [
                              Text("Let’s create\nyour account",
                                  style: textStyleBlueAvenikBold26),
                              SizedBox(height: 30.0),
                              Text(
                                  "Please enter your phone number and we\'ll\nsend you four-digit code in SMS.",
                                  style: textStyleGreyQuicksandBold16),
                            ],
                          ),
                        ),
                        SizedBox(height: 50.0),
                        SlideTransition(
                          position: _bloc.slidingRightToLeftAnimation,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  StreamBuilder<Country>(
                                    stream: _bloc.countryPickerStream,
                                    builder: (context, snapshot) {
                                      return InkWell(
                                        onTap: () => _openCountryPickerDialog(
                                            context, _bloc),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4.0)),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  width: 1.5,
                                                  color: COLOR_LIGHT_GRAY),
                                            ),
                                          ),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 4.0, vertical: 8.0),
                                          child: Row(
                                            children: [
                                              Text(
                                                FrequentUtils.getInstance()
                                                    .countryCodeToEmoji(
                                                        snapshot.hasData
                                                            ? snapshot.data.countryCode
                                                            : "CA"),
                                                style: const TextStyle(
                                                    fontSize: 20),
                                              ),
                                              SizedBox(width: 8.0),
                                              Text(
                                                '+${snapshot.hasData ? snapshot.data.phoneCode : "1"}',
                                                style: textStyleQuicksandBlack16,
                                              ),
                                              SizedBox(width: 8.0),
                                              Image.asset(
                                                  "assets/png/drop_down_light.png",
                                                  width: 16,
                                                  height: 16)
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                  SizedBox(width: 16.0),
                                  Expanded(
                                    child: StreamBuilder<String>(
                                        stream: _bloc.phoneStream,
                                        builder: (context, snapshot) {
                                          return TextField(
                                            style: textStyleQuicksandBlack16,
                                            keyboardType: TextInputType.number,
                                            maxLength: 14,
                                            inputFormatters: [
                                              FilteringTextInputFormatter
                                                  .digitsOnly,
                                              UsNumberTextInputFormatter()
                                            ],
                                            onChanged: (s) =>
                                                _bloc.phoneSink.add(s),
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 4.0,
                                                      vertical: 8.0),
                                              counterText: "",
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1.5,
                                                    color: COLOR_LIGHT_GRAY),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1.5,
                                                    color: COLOR_LIGHT_GRAY),
                                              ),
                                              border: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1.5,
                                                    color: COLOR_LIGHT_GRAY),
                                              ),
                                              hintText: "(123) 456-7890",
                                              hintStyle:
                                                  textStyleQuicksandLightGrey16,
                                              errorText: snapshot.error,
                                            ),
                                          );
                                        }),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 60),
                        SlideTransition(
                          position: _bloc.slidingRightToLeftAnimation,
                          child: Hero(
                            tag: APP_NAME,
                            child: Material(
                              elevation: 5.0,
                              color: APP_PRIMARY_COLOR,
                              shadowColor: APP_PRIMARY_COLOR_LIGHT,
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                              child: InkWell(
                                onTap: () => _bloc.sendOtpRequest(context),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                                splashColor: APP_PRIMARY_COLOR_DARK,
                                child: Container(
                                  height: 56,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(15.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(width: 50),
                                        Text(
                                          "Send OTP",
                                          style: textStyleWhite16,
                                          textAlign: TextAlign.center,
                                        ),
                                        SizedBox(width: 20.0),
                                        RotatedBox(
                                          quarterTurns: 45,
                                          child: Lottie.asset(
                                              'assets/json/arrow_up.json',
                                              width: 30,
                                              height: 30),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 35),
                        SlideTransition(
                          position: _bloc.slidingLeftToRightAnimation,
                          child: Row(
                            children: [
                              Text("Already registered?",
                                  style: textStyleGreyQuicksandBold16),
                              SizedBox(width: 2),
                              InkWell(
                                onTap: () => _onClickSignIn(context),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.0)),
                                child: Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Text("Sign in",
                                      style: textStyleQuicksandBlueBoldUL16),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _openCountryPickerDialog(BuildContext context, CreateAccountBloc bloc) {
    showCountryPicker(
      context: context,
      showPhoneCode: true,
      onSelect: (Country country) => bloc.countryPickerSink.add(country),
    );
  }

  _onClickSignIn(BuildContext context) {

  }

  _initializeCommonListener(BuildContext context) {
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if(value){
        FrequentUtils.getInstance().showProgressDialog(context);
      }else{
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //response
    _bloc.responseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        await _bloc.startAnimationReverse();
        await Navigator.of(context).pushNamed(AppRoute.verifyOTP, arguments: response.data);
        _bloc.startAnimationForward();
      }
    });
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
