import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_page_indicator/flutter_page_indicator.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/no_grow_behavior.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/views/sliderView/SliderView.dart';
import 'package:lottie/lottie.dart';
import 'intro_bloc.dart';

class IntroScreen extends StatefulWidget {
  @override
  State<IntroScreen> createState() => IntroScreenState();
}

class IntroScreenState extends State<IntroScreen>
    with SingleTickerProviderStateMixin {
  var pageController = PageController(initialPage: 0);
  var pageViewModelData = List<PageViewData>();
  Timer sliderTimer;
  var currentShowIndex = 0;
  IntroBloc _bloc;

  Future<bool> initializeController() {
    Completer<bool> completer = new Completer<bool>();

    /// Callback called after widget has been fully built
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      completer.complete(true);
    });
    return completer.future;
  }

  @override
  void initState() {
    pageViewModelData.add(PageViewData(
      titleText:
          'Not just better healthcare,\nbut a better healthcare\nexperience.',
      subText:
          'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      assetsImage: 'assets/png/doctors.png',
    ));
    pageViewModelData.add(PageViewData(
      titleText:
          'Not just better healthcare,\nbut a better healthcare\nexperience.',
      subText:
          'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      assetsImage: 'assets/images/doctors.png',
    ));
    pageViewModelData.add(PageViewData(
      titleText:
          'Not just better healthcare,\nbut a better healthcare\nexperience.',
      subText:
          'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      assetsImage: 'assets/images/doctors.png',
    ));
    sliderTimer = Timer.periodic(Duration(seconds: 3), (timer) {
      if (currentShowIndex == 0) {
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 800), curve: Curves.fastOutSlowIn);
      } else if (currentShowIndex == 1) {
        pageController.animateToPage(2,
            duration: Duration(milliseconds: 800), curve: Curves.fastOutSlowIn);
      } else if (currentShowIndex == 2) {
        pageController.animateToPage(0,
            duration: Duration(milliseconds: 800), curve: Curves.fastOutSlowIn);
      }
    });
    _bloc = IntroBloc(this);
    _bloc.startAnimationForward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.maxFinite,
          width: double.maxFinite,
          child: Column(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(35.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: SlideTransition(
                            position: _bloc.slidingLeftToRightAnimation,
                            child: Container(
                              margin: EdgeInsets.only(top: 20.0),
                              child: Image.asset("assets/png/doctors.png"),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      FutureBuilder(
                        future: initializeController(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return SizedBox();
                          }
                          return Container(
                            child: PageIndicator(
                              layout: PageIndicatorLayout.WARM,
                              size: 10.0,
                              controller: pageController,
                              space: 5.0,
                              count: 3,
                              color: COLOR_YELLOW_LIGHT,
                              activeColor: COLOR_YELLOW_DARK,
                            ),
                          );
                        },
                      ),
                      SizedBox(height: 20.0),
                      Flexible(
                        fit: FlexFit.tight,
                        child: ScrollConfiguration(
                          behavior: NoGrowBehavior(),
                          child: PageView(
                            controller: pageController,
                            pageSnapping: true,
                            allowImplicitScrolling: false,
                            onPageChanged: (index) =>
                                currentShowIndex = index ?? 0,
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              SliderView(imageData: pageViewModelData[0]),
                              SliderView(imageData: pageViewModelData[1]),
                              SliderView(imageData: pageViewModelData[2]),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 50, bottom: 50),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: SlideTransition(
                    position: _bloc.slidingRightToLeftAnimation,
                    child: Hero(
                      tag: APP_NAME,
                      child: Material(
                        elevation: 5.0,
                        color: APP_PRIMARY_COLOR,
                        shadowColor: APP_PRIMARY_COLOR_LIGHT,
                        borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(15.0),
                        ),
                        child: InkWell(
                          onTap: () => onClickGetStarted(context),
                          borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(15.0),
                          ),
                          splashColor: APP_PRIMARY_COLOR_DARK,
                          child: Container(
                            height: 56,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(15.0),
                              ),
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox(width: 40),
                                  Expanded(
                                      child: Text(
                                    "Get Started",
                                    style: textStyleWhite16,
                                    textAlign: TextAlign.center,
                                  )),
                                  SizedBox(width: 10.0),
                                  RotatedBox(
                                    quarterTurns: 45,
                                    child: Lottie.asset(
                                        'assets/json/arrow_up.json',
                                        width: 30,
                                        height: 30),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  onClickGetStarted(BuildContext context) async {
    await _bloc.startAnimationReverse();
    Navigator.of(context).pushReplacementNamed(AppRoute.createAccount);
  }

  @override
  void dispose() {
    sliderTimer?.cancel();
    _bloc.dispose();
    super.dispose();
  }
}
