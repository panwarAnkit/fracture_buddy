import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/style.dart';

class SliderView extends StatelessWidget {
  final PageViewData imageData;

  const SliderView({Key key, this.imageData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Text(imageData.titleText, style: textStyleBlueAvenik22),
        SizedBox(height: 20.0),
        Text(imageData.subText, style: textStyleGreyQuicksandBold14),
      ],
    );
  }
}

class PageViewData {
  final String titleText;
  final String subText;
  final String assetsImage;

  PageViewData({this.titleText, this.subText, this.assetsImage});
}