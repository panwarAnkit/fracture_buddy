import 'dart:convert';
import 'dart:io';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fracture_buddy/models/serviceList/AllServicesResponse.dart';
import 'package:fracture_buddy/models/serviceList/College.dart';
import 'package:fracture_buddy/models/serviceList/Hospital.dart';
import 'package:fracture_buddy/models/serviceList/Speciality.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/utils/us_number_text_formatter.dart';
import 'package:lottie/lottie.dart';
import 'edit_profile_bloc.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State<EditProfileScreen> createState() => EditProfileScreenState();
}

class EditProfileScreenState extends State<EditProfileScreen>
    with SingleTickerProviderStateMixin {
  EditProfileBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<AutoCompleteTextFieldState<Hospital>> _hospitalKey =
      new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<Speciality>> _specialityKey =
      new GlobalKey();
  TextEditingController _userIdController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _phoneController;
  TextEditingController _emailController;

  List<Hospital> _selectedHospitalList = List();
  List<Speciality> _selectedSpecialityList = List();

  @override
  void initState() {
    _userIdController = TextEditingController();
    _firstNameController = TextEditingController();
    _lastNameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();
    _bloc = EditProfileBloc(this);
    _initializeCommonListener(context);
    _bloc.startAnimationForward();
    _bloc.getAllServiceList(context);
    _bloc.getProfileData(context);
    _initializeAllValues(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: APP_PRIMARY_COLOR,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            splashColor: Colors.grey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset("assets/png/back_arrow.png",
                  width: 24, height: 24),
            ),
            onTap: () => Navigator.of(context).pop(),
          ),
        ),
        elevation: 0,
        title: Stack(
          alignment: Alignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/png/stethoscope_transparent_white.png",
                  height: 100,
                  width: 90,
                ),
                Image.asset("assets/png/stethoscope_transparent_blue.png",
                    height: 80, width: 90),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: screenSize.width / 7.0),
              child: Text("Profile", style: textStyleWhiteBold18),
            ),
          ],
        ),
        toolbarHeight: 80,
      ),
      body: Builder(builder: (context) {
        return SafeArea(
          child: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topRight: Radius.circular(40.0)),
              color: Colors.white,
            ),
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 25.0),
              physics: BouncingScrollPhysics(),
              children: [
                SizedBox(height: 30),
                SlideTransition(
                  position: _bloc.slidingLeftToRightAnimation,
                  child: Text("Basic Information",
                      style: textStyleQuicksandBlack16),
                ),
                SizedBox(height: 30.0),
                SlideTransition(
                  position: _bloc.slidingRightToLeftAnimation,
                  child: Column(
                    children: [
                      Container(
                        height: 120,
                        width: 120,
                        decoration: BoxDecoration(
                          color: COLOR_LIGHT_GRAY,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: StreamBuilder<dynamic>(
                          stream: _bloc.profilePicStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData && snapshot.data != null) {
                              return ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                child: snapshot.data is File
                                ?Image.file(snapshot.data, fit: BoxFit.cover)
                                :CachedNetworkImage(
                                  imageUrl: APP_IMAGE_URL + snapshot.data,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => Center(
                                    child: Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: CircularProgressIndicator(),
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, value) {
                                    return Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Center(
                                        child: Lottie.asset(
                                          'assets/json/doctor_bottom_up.json',
                                          width: 60,
                                          height: 60,
                                          repeat: false,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }
                            return Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Center(
                                child: Lottie.asset(
                                  'assets/json/doctor_bottom_up.json',
                                  width: 60,
                                  height: 60,
                                  repeat: false,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      FlatButton(
                        onPressed: () => _bloc.onClickUploadPhoto(context),
                        child: Text("Upload Photo",
                            style: textStyleGreyQuicksandBold14),
                      )
                    ],
                  ),
                ),
                SlideTransition(
                  position: _bloc.slidingLeftToRightAnimation,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "User Id*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          TextField(
                            style: textStyleQuicksandBlack16,
                            keyboardType: TextInputType.text,
                            onChanged: (s) => _bloc.userNameSink.add(s),
                            controller: _userIdController,
                            decoration: InputDecoration(
                              isDense: true,
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "First Name*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          TextField(
                            style: textStyleQuicksandBlack16,
                            keyboardType: TextInputType.text,
                            onChanged: (s) => _bloc.firstNameSink.add(s),
                            textCapitalization: TextCapitalization.words,
                            controller: _firstNameController,
                            decoration: InputDecoration(
                              isDense: true,
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Last Name*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          TextField(
                            style: textStyleQuicksandBlack16,
                            keyboardType: TextInputType.text,
                            onChanged: (s) => _bloc.lastNameSink.add(s),
                            textCapitalization: TextCapitalization.words,
                            controller: _lastNameController,
                            decoration: InputDecoration(
                              isDense: true,
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Phone Number*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          Row(
                            children: [
                              StreamBuilder<Country>(
                                stream: _bloc.countryPickerStream,
                                builder: (context, snapshot) {
                                  return InkWell(
                                    onTap: () => _openCountryPickerDialog(
                                        context, _bloc),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4.0)),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                              width: 1.5,
                                              color: COLOR_LIGHT_GRAY),
                                        ),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 4.0, vertical: 8.0),
                                      child: Row(
                                        children: [
                                          Text(
                                            FrequentUtils.getInstance()
                                                .countryCodeToEmoji(snapshot
                                                        .hasData
                                                    ? snapshot.data.countryCode
                                                    : "CA"),
                                            style:
                                                const TextStyle(fontSize: 20),
                                          ),
                                          SizedBox(width: 8.0),
                                          Text(
                                            '+${snapshot.hasData ? snapshot.data.phoneCode : "1"}',
                                            style: textStyleQuicksandBlack16,
                                          ),
                                          SizedBox(width: 8.0),
                                          Image.asset(
                                              "assets/png/drop_down_light.png",
                                              width: 16,
                                              height: 16)
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                              SizedBox(width: 16.0),
                              Expanded(
                                child: TextField(
                                  style: textStyleQuicksandBlack16,
                                  keyboardType: TextInputType.number,
                                  maxLength: 14,
                                  controller: _phoneController,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                    UsNumberTextInputFormatter()
                                  ],
                                  onChanged: (s) => _bloc.phoneSink.add(s),
                                  decoration: InputDecoration(
                                    isDense: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    counterText: "",
                                    hintText: "(123) 456-7890",
                                    hintStyle: textStyleQuicksandLightGrey16,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Email*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          TextField(
                            style: textStyleQuicksandBlack16,
                            keyboardType: TextInputType.emailAddress,
                            controller: _emailController,
                            onChanged: (s) => _bloc.emailSink.add(s),
                            decoration: InputDecoration(
                              isDense: true,
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1.5, color: COLOR_LIGHT_GRAY),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                SlideTransition(
                  position: _bloc.slidingRightToLeftAnimation,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Professional Information",
                          style: textStyleQuicksandBlack16),
                      SizedBox(height: 30),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "College*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          StreamBuilder<AllServicesResponse>(
                              stream: _bloc.allServicesResponseStream,
                              builder: (context, mainSnapshot) {
                                if (mainSnapshot.hasData) {
                                  return Container(
                                    child: StreamBuilder<String>(
                                      stream: _bloc.collegesStream,
                                      builder: (context, snapshot) {
                                        return DropdownButton<String>(
                                          onTap: () => FocusScope.of(context)
                                              .requestFocus(FocusNode()),
                                          underline: Container(
                                            color: COLOR_LIGHT_GRAY,
                                            height: 1.5,
                                          ),
                                          value: snapshot.data,
                                          isExpanded: true,
                                          icon: Image.asset(
                                              "assets/png/drop_down_light.png",
                                              width: 16,
                                              height: 16),
                                          onChanged: (value) =>
                                              _bloc.collegesSink.add(value),
                                          items: mainSnapshot.data.college.map(
                                            (value) {
                                              return DropdownMenuItem<String>(
                                                value: value.id1,
                                                child: Text(value.collegeName,
                                                    style:
                                                        textStyleQuicksandBlack16),
                                              );
                                            },
                                          ).toList(),
                                        );
                                      },
                                    ),
                                  );
                                } else {
                                  return Center(
                                    child: Container(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  );
                                }
                              }),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Speciality*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          StreamBuilder<AllServicesResponse>(
                              stream: _bloc.allServicesResponseStream,
                              builder: (context, mainSnapshot) {
                                if (mainSnapshot.hasData) {
                                  return AutoCompleteTextField<Speciality>(
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 10.0),
                                      isDense: true,
                                      prefixIconConstraints: BoxConstraints(
                                          maxHeight: 30, maxWidth: 30),
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.all(6.0),
                                        child: Image.asset(
                                          "assets/png/search_blue.png",
                                          height: 20,
                                          width: 20,
                                        ),
                                      ),
                                    ),
                                    textCapitalization:
                                        TextCapitalization.words,
                                    itemSubmitted: (item) {
                                      _selectedSpecialityList.add(item);
                                      _bloc.selectedSpecialityListSink
                                          .add(_selectedSpecialityList);
                                    },
                                    key: _specialityKey,
                                    suggestions: mainSnapshot.data.speciality,
                                    itemBuilder: (context, suggestion) =>
                                        ListTile(
                                      title: Text(suggestion.name),
                                      dense: true,
                                    ),
                                    itemSorter: (a, b) =>
                                        a.name.compareTo(b.name),
                                    itemFilter: (suggestion, input) =>
                                        suggestion.name
                                            .toLowerCase()
                                            .startsWith(
                                              input.toLowerCase(),
                                            ),
                                  );
                                } else {
                                  return Center(
                                    child: Container(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  );
                                }
                              }),
                          SizedBox(height: 10),
                          StreamBuilder<List<Speciality>>(
                            initialData: _selectedSpecialityList,
                            stream: _bloc.selectedSpecialityListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData &&
                                  snapshot.data.isNotEmpty) {
                                return Container(
                                  height: 40,
                                  child: Center(
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index) {
                                        return Wrap(
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20.0)),
                                                color: APP_PRIMARY_COLOR_DARK,
                                              ),
                                              margin: EdgeInsets.all(4.0),
                                              padding: EdgeInsets.only(
                                                  top: 0.0,
                                                  bottom: 0.0,
                                                  left: 12.0,
                                                  right: 8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    snapshot.data[index].name,
                                                    style: textStyleWhite,
                                                  ),
                                                  SizedBox(width: 4.0),
                                                  Container(
                                                    height: 26,
                                                    width: 1,
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(width: 2.0),
                                                  InkWell(
                                                    onTap: () {
                                                      if (_selectedSpecialityList
                                                              .length >
                                                          0) {
                                                        _selectedSpecialityList
                                                            .removeAt(index);
                                                        _bloc
                                                            .selectedSpecialityListSink
                                                            .add(
                                                                _selectedSpecialityList);
                                                      }
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(2.0),
                                                      child: Image.asset(
                                                        "assets/png/cross.png",
                                                        width: 15,
                                                        height: 15,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                );
                              } else {
                                return SizedBox(height: 5.0);
                              }
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Hospital*",
                            style: textStyleGreyQuicksandBold14,
                          ),
                          StreamBuilder<AllServicesResponse>(
                              stream: _bloc.allServicesResponseStream,
                              builder: (context, mainSnapshot) {
                                if (mainSnapshot.hasData) {
                                  return AutoCompleteTextField<Hospital>(
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 10.0),
                                      isDense: true,
                                      prefixIconConstraints: BoxConstraints(
                                          maxHeight: 30, maxWidth: 30),
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.all(6.0),
                                        child: Image.asset(
                                          "assets/png/search_blue.png",
                                          height: 20,
                                          width: 20,
                                        ),
                                      ),
                                    ),
                                    textCapitalization:
                                        TextCapitalization.words,
                                    itemSubmitted: (item) {
                                      _selectedHospitalList.add(item);
                                      _bloc.selectedListSink
                                          .add(_selectedHospitalList);
                                    },
                                    key: _hospitalKey,
                                    suggestions: mainSnapshot.data.hospitals,
                                    itemBuilder: (context, suggestion) =>
                                        ListTile(
                                      title: Text(suggestion.hospitalName),
                                      dense: true,
                                    ),
                                    itemSorter: (a, b) => a.hospitalName
                                        .compareTo(b.hospitalName),
                                    itemFilter: (suggestion, input) =>
                                        suggestion.hospitalName
                                            .toLowerCase()
                                            .startsWith(
                                              input.toLowerCase(),
                                            ),
                                  );
                                } else {
                                  return Center(
                                    child: Container(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  );
                                }
                              }),
                          SizedBox(height: 10),
                          StreamBuilder<List<Hospital>>(
                            initialData: _selectedHospitalList,
                            stream: _bloc.selectedListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData &&
                                  snapshot.data.isNotEmpty) {
                                return Container(
                                  height: 40,
                                  child: Center(
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index) {
                                        return Wrap(
                                          children: <Widget>[
                                            Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20.0)),
                                                color: APP_PRIMARY_COLOR_DARK,
                                              ),
                                              margin: EdgeInsets.all(4.0),
                                              padding: EdgeInsets.only(
                                                  top: 0.0,
                                                  bottom: 0.0,
                                                  left: 12.0,
                                                  right: 8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    snapshot.data[index]
                                                        .hospitalName,
                                                    style: textStyleWhite,
                                                  ),
                                                  SizedBox(width: 4.0),
                                                  Container(
                                                    height: 26,
                                                    width: 1,
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(width: 2.0),
                                                  InkWell(
                                                    onTap: () {
                                                      if (_selectedHospitalList
                                                              .length >
                                                          0) {
                                                        _selectedHospitalList
                                                            .removeAt(index);
                                                        _bloc.selectedListSink.add(
                                                            _selectedHospitalList);
                                                      }
                                                    },
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(2.0),
                                                      child: Image.asset(
                                                        "assets/png/cross.png",
                                                        width: 15,
                                                        height: 15,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                );
                              } else {
                                return SizedBox(height: 5.0);
                              }
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                SlideTransition(
                  position: _bloc.slidingBottomRightToCenterAnimation,
                  child: Hero(
                    tag: APP_NAME,
                    child: Material(
                      elevation: 5.0,
                      color: APP_PRIMARY_COLOR,
                      shadowColor: APP_PRIMARY_COLOR_LIGHT,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15.0),
                      ),
                      child: InkWell(
                        onTap: () => _bloc.onClickRegister(context),
                        borderRadius: BorderRadius.all(
                          Radius.circular(15.0),
                        ),
                        splashColor: APP_PRIMARY_COLOR_DARK,
                        child: Container(
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15.0),
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(width: 50),
                                Text(
                                  "Update Profile",
                                  style: textStyleWhite16,
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(width: 20.0),
                                RotatedBox(
                                  quarterTurns: 45,
                                  child: Lottie.asset(
                                      'assets/json/arrow_up.json',
                                      width: 30,
                                      height: 30),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
        );
      }),
    );
  }

  _openCountryPickerDialog(BuildContext context, EditProfileBloc bloc) {
    showCountryPicker(
      context: context,
      showPhoneCode: true,
      onSelect: (Country country) => bloc.countryPickerSink.add(country),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //get profile response
    _bloc.getProfileResponseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        await FrequentUtils.getInstance().saveUserData(response.data);
        _initializeAllValues(context);
      }
    });
    //register user response
    _bloc.registerUserResponseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        await FrequentUtils.getInstance().saveUserData(response.data);
        FrequentUtils.getInstance().showToast(response.message);
      }
    });
  }

  void _initializeAllValues(BuildContext context) {
    //profile picture
    final profilePicture = SharedPref.mInstance.getString(SharedPref.STORAGE_PROFILE_PIC);
    _bloc.profilePicSink.add(profilePicture);
    //user id
    _userIdController.text =
        SharedPref.mInstance.getString(SharedPref.STORAGE_USER_NAME);
    _bloc.userNameSink.add(_userIdController.text);
    //first name
    _firstNameController.text =
        SharedPref.mInstance.getString(SharedPref.STORAGE_FIRST_NAME);
    _bloc.firstNameSink.add(_firstNameController.text);
    //last name
    _lastNameController.text = SharedPref.mInstance.getString(SharedPref.STORAGE_LAST_NAME);
    _bloc.lastNameSink.add(_lastNameController.text);
    //email
    _emailController.text = SharedPref.mInstance.getString(SharedPref.STORAGE_EMAIL);
    _bloc.emailSink.add(_emailController.text);
    //country code
    final countryCode = SharedPref.mInstance.getString(SharedPref.STORAGE_COUNTRY_CODE);
    final phone = SharedPref.mInstance.getString(SharedPref.STORAGE_PHONE);
    _bloc.countryPickerSink.add(Country(countryCode: countryCode, phoneCode: phone.substring(0, phone.length - 10)));
    //phone number
    final formattedPhoneNumber = FrequentUtils.getInstance().getFormattedPhoneNumber(phone.substring(phone.length - 10, phone.length));
    _phoneController.text = formattedPhoneNumber;
    _bloc.phoneSink.add(_phoneController.text);
    //college
    final encodedCollege =
        SharedPref.mInstance.getString(SharedPref.STORAGE_COLLEGE);
    College college = College.fromJson(jsonDecode(encodedCollege));
    _bloc.collegesSink.add(college.id1);
    //specialities
    final encodedSpecialities =
        SharedPref.mInstance.getStringList(SharedPref.STORAGE_SPECIALITIES);
    _bloc.selectedSpecialityListSink
        .add(encodedSpecialities.map((e) => Speciality.fromJson(jsonDecode(e))).toList());
    //hospitals
    final encodedHospitals =
        SharedPref.mInstance.getStringList(SharedPref.STORAGE_HOSPITALS);
    _bloc.selectedListSink
        .add(encodedHospitals.map((e) => Hospital.fromJson(jsonDecode(e))).toList());
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
