import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';

class BoneViewBloc extends BaseBloc {
  int _listCount;
  AnimationController controller;
  Animation<Offset> slidingLeftToRightAnimation;

  BehaviorSubject<int> _drawerIconAngleController;
  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<ClassData> _classController;

  // BehaviorSubject<List<BoneClassData>> _boneClassDataListController;
  BehaviorSubject<List<ClassData>> _allClassController;
  BehaviorSubject<List<ClassData>> _getClassChildrenController;

  BoneViewBloc(TickerProvider provider) {
    _listCount = 0;
    controller = AnimationController(
        vsync: provider, duration: Duration(milliseconds: 200));
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(-0.25, 0.0))
            .animate(controller);

    _drawerIconAngleController = BehaviorSubject<int>.seeded(0);
    _loadingController = BehaviorSubject<bool>.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _classController = BehaviorSubject<ClassData>();
    // _boneClassDataListController = BehaviorSubject<List<BoneClassData>>.seeded(List());
    _allClassController = BehaviorSubject();
    _getClassChildrenController = BehaviorSubject();
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  StreamSink<ClassData> get classSink => _classController.sink;

  // StreamSink<List<BoneClassData>> get boneClassDataListSink => _boneClassDataListController.sink;

  StreamSink<List<ClassData>> get allClassSink => _allClassController.sink;

  StreamSink<List<ClassData>> get getClassChildrenSink =>
      _getClassChildrenController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<ClassData> get classStream => _classController.stream;

  // Stream<List<BoneClassData>> get boneClassDataListStream => _boneClassDataListController.stream;

  Stream<List<ClassData>> get allClassStream => _allClassController.stream;

  Stream<List<ClassData>> get getClassChildrenStream =>
      _getClassChildrenController.stream;

  Stream<int> get drawerIconAngleStream => _drawerIconAngleController.stream;

  Future<void> getClassList(BuildContext context) async {
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getAllClass(authToken);
    _loadingController.sink.add(false);
    if (response.success) {
      allClassSink.add(response.data);
      if (response.data.isNotEmpty) onSelectClass(response.data[0]);
      _listCount++;
    } else {
      errorMessageSink.add(response.message);
    }
  }

  void onSelectClass(ClassData classData) {
    classSink.add(classData);
    getClassChildren(classData);
  }

  Future<void> getClassChildren(ClassData classData) async {
    // final classDataList = _boneClassDataListController.value;
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance()
        .getAllClassChildren(authToken, classData.id1);
    _loadingController.sink.add(false);
    if (response.success) {
      if (_listCount == 0) {
        allClassSink.add(response.data);
        if (response.data.isNotEmpty) onSelectClass(response.data[0]);
      } else {
        getClassChildrenSink.add(response.data.reversed.toList());
      }
      _listCount++;
    } else {
      errorMessageSink.add(response.message);
    }
    // boneClassDataListSink.add(classDataList);
  }

  Future<void> onSelectSubClass(BuildContext context, ClassData classData) async {
    print("Sub Class Clicked: " + classData.legend);
    // final classDataList = _boneClassDataListController.value;
    // classDataList[index].selectedClassId = classId;
    // _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getAllClassChildren(authToken, classData.id1);
    // _loadingController.sink.add(false);
    if (response.success) {
      if (response.data.isNotEmpty) {
        print("Sub Class Item Count" + response.data.length.toString());
        dynamic result = await Navigator.of(context).pushNamed(AppRoute.boneView, arguments: {
          KEY_FROM: VALUE_BONE,
          KEY_DATA: _classController.value
        });
        if(result!=null && result[KEY_FROM]==VALUE_BONE_LAST){
          Navigator.of(context).pop(result);
        }
      } else {
        Navigator.of(context).pop({
          KEY_FROM: VALUE_BONE_LAST,
          KEY_DATA: classData
        });
      }
    } else {
      errorMessageSink.add(response.message);
    }
    // boneClassDataListSink.add(classDataList);
  }

  void onClickDrawerIcon(BuildContext context) {
    if (_drawerIconAngleController.value == 0) {
      _drawerIconAngleController.sink.add(2);
    } else {
      _drawerIconAngleController.sink.add(0);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    _drawerIconAngleController.close();
    _loadingController.close();
    _errorMessageController.close();
    _classController.close();
    // _boneClassDataListController.close();
    _allClassController.close();
    _getClassChildrenController.close();
  }
}
