import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/fab_circular_menu.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'bone_view_bloc.dart';

class BoneViewScreen extends StatefulWidget {
  final dynamic boneView;

  BoneViewScreen(this.boneView);

  @override
  State<BoneViewScreen> createState() => BoneViewScreenState();
}

class BoneViewScreenState extends State<BoneViewScreen>
    with SingleTickerProviderStateMixin {
  BoneViewBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _bloc = BoneViewBloc(this);
    _initializeCommonListener(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: APP_PRIMARY_COLOR,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            splashColor: Colors.grey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset("assets/png/back_arrow.png",
                  width: 24, height: 24),
            ),
            onTap: () => Navigator.of(context).pop(),
          ),
        ),
        elevation: 0,
        title: Stack(
          alignment: Alignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/png/stethoscope_transparent_white.png",
                  height: 100,
                  width: 90,
                ),
                Image.asset("assets/png/stethoscope_transparent_blue.png",
                    height: 80, width: 90),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: screenSize.width / 7.0),
              child: Text("Choose class", style: textStyleWhiteBold18),
            ),
          ],
        ),
        toolbarHeight: 80,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0)),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(30.0),
              child: StreamBuilder<ClassData>(
                  initialData: widget.boneView[KEY_DATA],
                  stream: _bloc.classStream,
                  builder: (context, snapshot) {
                    return Text(snapshot.hasData ? snapshot.data.legend : "",
                        style: textStyleQuicksandBoldYellow16);
                  }),
            ),
            Expanded(
              child: Center(
                child: Stack(
                  children: [
                    StreamBuilder<int>(
                      initialData: 2,
                      stream: _bloc.drawerIconAngleStream,
                      builder: (context, snapshot) {
                        if (snapshot.data == 0) {
                          _bloc.controller.reverse();
                        } else {
                          _bloc.controller.forward();
                        }
                        return SlideTransition(
                          position: _bloc.slidingLeftToRightAnimation,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              padding: EdgeInsets.all(4.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.horizontal(
                                      right: Radius.circular(12.0)),
                                  border: Border.all(
                                      color: COLOR_LIGHT_GRAY3, width: 1.0),
                                  color: COLOR_LIGHT_GRAY2),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    width: 100,
                                    child: StreamBuilder<List<ClassData>>(
                                      initialData: [],
                                      stream: _bloc.allClassStream,
                                      builder: (context, mainSnapshot) {
                                        return ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: mainSnapshot.data.length,
                                          itemBuilder: (context, index) {
                                            final classData =
                                                mainSnapshot.data[index];
                                            return StreamBuilder<ClassData>(
                                              stream: _bloc.classStream,
                                              builder: (context, snapshot) {
                                                return InkWell(
                                                  onTap: () => _bloc
                                                      .onSelectClass(classData),
                                                  child: Chip(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 0.0,
                                                            horizontal: 20.0),
                                                    labelPadding:
                                                        EdgeInsets.zero,
                                                    backgroundColor:
                                                        snapshot.data ==
                                                                classData
                                                            ? APP_PRIMARY_COLOR
                                                            : Colors.white,
                                                    autofocus: true,
                                                    labelStyle:
                                                        textStyleGreyQuicksandBold12,
                                                    label: Text(
                                                      classData.legend,
                                                      overflow:
                                                          TextOverflow.clip,
                                                      style: snapshot.data ==
                                                              classData
                                                          ? textStyleWhiteQuicksandBold12
                                                          : textStyleGreyQuicksandBold12,
                                                    ),
                                                    elevation: 2.0,
                                                    shadowColor:
                                                        COLOR_LIGHT_GRAY,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(10.0),
                                                      ),
                                                      side: BorderSide(
                                                          color:
                                                              COLOR_LIGHT_GRAY3),
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 4),
                                  InkWell(
                                    onTap: () =>
                                        _bloc.onClickDrawerIcon(context),
                                    child: Container(
                                      height: 25,
                                      width: 25,
                                      decoration: BoxDecoration(
                                          color: APP_PRIMARY_COLOR,
                                          borderRadius:
                                              BorderRadius.circular(40)),
                                      child: Padding(
                                        padding: EdgeInsets.all(6.0),
                                        child: StreamBuilder<int>(
                                          initialData: 2,
                                          stream: _bloc.drawerIconAngleStream,
                                          builder: (context, snapshot) {
                                            return RotatedBox(
                                              quarterTurns: snapshot.data,
                                              child: Image.asset(
                                                "assets/png/2_arrow.png",
                                                width: 24,
                                                height: 24,
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
//                    Positioned(
//                      left: screenSize.width/2.2,
//                      child: Container(
//                        width: screenSize.width*1.42,
//                        height: screenSize.width*1.42,
//                        decoration: ShapeDecoration(
//                          shape: CircleBorder(
//                            side: BorderSide(
//                                color: APP_PRIMARY_COLOR_DARK, width: 95),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Align(
//                      alignment: Alignment.centerRight,
//                      child: Padding(
//                        padding: const EdgeInsets.all(20.0),
//                        child: SvgPicture.asset(
//                          "assets/left_arm.svg",
//                          height: 220,
//                          width: 80,
//                        ),
//                      ),
//                    )
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: FabCircularMenu(
//                          alignment: Alignment.centerRight,
//                          ringColor: APP_PRIMARY_COLOR_DARK,
//                          fabColor: APP_PRIMARY_COLOR_DARK,
//                          fabCloseColor: APP_PRIMARY_COLOR_DARK,
//                          fabOpenColor: APP_PRIMARY_COLOR_DARK,
//                          ringWidth: 80,
//                          children: <Widget>[
//                            IconButton(
//                                icon: Icon(Icons.home),
//                                onPressed: () {
//                                  print('Home');
//                                }),
//                            IconButton(
//                                icon: Icon(Icons.favorite),
//                                onPressed: () {
//                                  print('Favorite');
//                                })
//                          ]),
//                    )
                    StreamBuilder<List<ClassData>>(
                      stream: _bloc.getClassChildrenStream,
                      builder: (context, subClassSnapshot) {
                        return FabCircularMenu(
                          fabMargin: EdgeInsets.all(8.0),
                          alignment: Alignment.centerRight,
                          ringColor: APP_PRIMARY_COLOR_DARK,
                          ringWidth: 100,
                          fabSizeHeight: 240,
                          fabSizeWidth: 80,
                          fabColor: Colors.white,
                          fabOpenIcon: StreamBuilder<ClassData>(
                            stream: _bloc.classStream,
                            builder: (context, classSnapshot) {
                              return Container(
                                height: 240,
                                width: 80,
                                child: CachedNetworkImage(
                                  imageUrl:
                                       APP_IMAGE_URL +"11A.2-1.jpg"
                                      ,
                                  placeholder: (context, url) => Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(30.0),
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, value) {
                                    return SvgPicture.asset(
                                      "assets/left_arm.svg",
                                    );
                                  },
                                ),
                              );
                            },
                          ),
                          fabCloseIcon: Container(
                            height: 240,
                            width: 80,
                            child: StreamBuilder<ClassData>(
                              stream: _bloc.classStream,
                              builder: (context, classSnapshot) {
                                return CachedNetworkImage(
                                  imageUrl: classSnapshot.hasData?APP_IMAGE_URL + classSnapshot.data.image:"",
                                  placeholder: (context, url) => Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(30.0),
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  ),
                                  // errorWidget: (context, url, value) {
                                  //   return SizedBox(width: 1,);
                                  // },
                                );
                              },
                            ),
                          ),
                          // children: <Widget>[
                          //   InkWell(
                          //     child: Container(
                          //       width: 80,
                          //       height: 80,
                          //       child: Padding(
                          //         padding: EdgeInsets.all(4.0),
                          //         child: Image.asset("assets/png/bottom_bone.png"),
                          //       ),
                          //     ),
                          //     borderRadius: BorderRadius.circular(50),
                          //     onTap: () {
                          //       print('Bottom Bone');
                          //     },
                          //   ),
                          //   InkWell(
                          //     child: Container(
                          //       width: 80,
                          //       height: 80,
                          //       child: Padding(
                          //         padding: EdgeInsets.all(4.0),
                          //         child: Image.asset("assets/png/center_bone.png"),
                          //       ),
                          //     ),
                          //     borderRadius: BorderRadius.circular(50),
                          //     onTap: () {
                          //       print('Center Bone');
                          //     },
                          //   ),
                          //   InkWell(
                          //     child: Container(
                          //       width: 80,
                          //       height: 80,
                          //       child: Padding(
                          //         padding: EdgeInsets.all(4.0),
                          //         child: Image.asset("assets/png/top_bone.png"),
                          //       ),
                          //     ),
                          //     borderRadius: BorderRadius.circular(50),
                          //     onTap: () {
                          //       print('Top Bone');
                          //     },
                          //   ),
                          // ],
                          children: List.generate(
                            subClassSnapshot.hasData
                                ? subClassSnapshot.data.length
                                : 0,
                                (index) {
                              final subClass =
                              subClassSnapshot.data[index];
                              return InkWell(
                                child: Container(
                                  width: 80,
                                  height: 80,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: 60,
                                        height: 60,
                                        child: Padding(
                                          padding: EdgeInsets.all(6.0),
                                          child: CachedNetworkImage(
                                            imageUrl:
                                            APP_IMAGE_URL + subClass.image,
                                            placeholder: (context, url) =>
                                                Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(30.0),
                                                    child:
                                                    CircularProgressIndicator(
                                                      strokeWidth: 2,
                                                    ),
                                                  ),
                                                ),
                                            errorWidget: (context, url, value) {
                                              return Image.asset(
                                                  "assets/png/top_bone.png");
                                            },
                                          ),
                                        ),
                                      ),
                                      Text(subClass.legend,style: textStyleWhite12,overflow: TextOverflow.ellipsis,)
                                    ],
                                  ),
                                ),
                                borderRadius: BorderRadius.circular(50),
                                onTap: () {
                                  _bloc.onSelectSubClass(context, subClass);
                                },
                              );
                            },
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(30.0),
              child: FloatingActionButton(
                elevation: 4.0,
                heroTag: APP_NAME,
                mini: true,
                child: Image.asset(
                  "assets/png/cross.png",
                  color: APP_PRIMARY_COLOR,
                  width: 15,
                  height: 15,
                ),
                backgroundColor: Colors.white,
                onPressed: () => Navigator.pop(context),
              ),
            )
          ],
        ),
      ),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //get all class list
    if (widget.boneView[KEY_FROM] == VALUE_SKELETON) {
      _bloc.getClassList(context);
    } else {
      _bloc.getClassChildren(widget.boneView[KEY_DATA]);
    }
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //register user response
    // _bloc.registerUserResponseStream.listen((response) async {
    //   if (response.isNotNull() && response.success) {
    //     await FrequentUtils.getInstance().saveUserData(response.data);
    //     FrequentUtils.getInstance().showToast(response.message);
    //   }
    // });
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
