import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/BoneClassData.dart';
import 'package:fracture_buddy/models/CommonSuccessResponse.dart';
import 'package:fracture_buddy/models/LocationData.dart';
import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/models/allSurgeon/AllSurgeonResponse.dart';
import 'package:fracture_buddy/models/getAssistants/GetAssistantResponse.dart';
import 'package:fracture_buddy/models/surgicalForm/AddSurgicalFormRequest.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/image_picker_popup.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';

class SurgicalCareFormBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation;

  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<String> _statusPickerController;
  BehaviorSubject<String> _firstNameController;
  BehaviorSubject<String> _lastNameController;
  BehaviorSubject<DateTime> _dateOfBirthController;
  BehaviorSubject<LocationData> _selectedAddressController;
  BehaviorSubject<String> _surgeonController;
  BehaviorSubject<String> _assistantController;
  BehaviorSubject<String> _classController;
  BehaviorSubject<List<BoneClassData>> _boneClassDataListController;
  BehaviorSubject<List<String>> _selectedListController;
  BehaviorSubject<List<File>> _specialityListController;
  BehaviorSubject<List<File>> _diagnosisListController;
  BehaviorSubject<AllSurgeonResponse> _allSurgeonResponseController;
  BehaviorSubject<GetAssistantResponse> _getSurgeonAssistantController;
  BehaviorSubject<AllClassResponse> _allClassController;
  BehaviorSubject<ClassData> _selectedClassController;
  BehaviorSubject<String> _notesController;
  BehaviorSubject<CommonSuccessResponse> _addSurgicalFormController;

  SurgicalCareFormBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 600),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingTopRightToCenterAnimation =
        Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0))
            .animate(_controller);

    _loadingController = BehaviorSubject<bool>.seeded(false);
    _errorMessageController = BehaviorSubject<String>.seeded("");
    _statusPickerController = BehaviorSubject<String>();
    _firstNameController = BehaviorSubject<String>.seeded("");
    _lastNameController = BehaviorSubject<String>.seeded("");
    _dateOfBirthController = BehaviorSubject<DateTime>();
    _selectedAddressController = BehaviorSubject<LocationData>();
    _surgeonController = BehaviorSubject<String>();
    _assistantController = BehaviorSubject<String>();
    _classController = BehaviorSubject<String>();
    _boneClassDataListController =
        BehaviorSubject<List<BoneClassData>>.seeded(List());
    _selectedListController = BehaviorSubject<List<String>>.seeded(List());
    _specialityListController = BehaviorSubject<List<File>>.seeded(List());
    _diagnosisListController = BehaviorSubject<List<File>>.seeded(List());
    _allSurgeonResponseController = BehaviorSubject();
    _getSurgeonAssistantController = BehaviorSubject();
    _allClassController = BehaviorSubject();
    _selectedClassController = BehaviorSubject<ClassData>();
    _notesController = BehaviorSubject<String>.seeded("");
    _addSurgicalFormController = BehaviorSubject();
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  StreamSink<String> get statusPickerSink => _statusPickerController.sink;

  StreamSink<String> get firstNameSink => _firstNameController.sink;

  StreamSink<String> get lastNameSink => _lastNameController.sink;

  StreamSink<DateTime> get dateOfBirthSink => _dateOfBirthController.sink;

  StreamSink<LocationData> get selectedAddressSink =>
      _selectedAddressController.sink;

  StreamSink<String> get surgeonSink => _surgeonController.sink;

  StreamSink<String> get assistantSink => _assistantController.sink;

  StreamSink<String> get classSink => _classController.sink;

  StreamSink<List<BoneClassData>> get boneClassDataListSink =>
      _boneClassDataListController.sink;

  StreamSink<List<String>> get selectedListSink => _selectedListController.sink;

  StreamSink<List<File>> get specialityListSink =>
      _specialityListController.sink;

  StreamSink<List<File>> get diagnosisListSink => _diagnosisListController.sink;

  StreamSink<AllSurgeonResponse> get allSurgeonResponseSink =>
      _allSurgeonResponseController.sink;

  StreamSink<GetAssistantResponse> get getSurgeonAssistantSink =>
      _getSurgeonAssistantController.sink;

  StreamSink<AllClassResponse> get allClassControllerSink =>
      _allClassController.sink;

  StreamSink<ClassData> get selectedClassSink => _selectedClassController.sink;

  StreamSink<String> get notesSink => _notesController.sink;

  StreamSink<CommonSuccessResponse> get addSurgicalFormSink => _addSurgicalFormController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<String> get statusPickerStream => _statusPickerController.stream;

  Stream<DateTime> get dateOfBirthStream => _dateOfBirthController.stream;

  Stream<LocationData> get selectedAddressStream =>
      _selectedAddressController.stream;

  Stream<String> get surgeonStream => _surgeonController.stream;

  Stream<String> get assistantStream => _assistantController.stream;

  Stream<String> get classStream => _classController.stream;

  Stream<List<BoneClassData>> get boneClassDataListStream =>
      _boneClassDataListController.stream;

  Stream<List<String>> get selectedListStream => _selectedListController.stream;

  Stream<List<File>> get specialityListStream =>
      _specialityListController.stream;

  Stream<List<File>> get diagnosisListStream => _diagnosisListController.stream;

  Stream<AllSurgeonResponse> get allSurgeonResponseStream =>
      _allSurgeonResponseController.stream;

  Stream<GetAssistantResponse> get getSurgeonAssistantStream =>
      _getSurgeonAssistantController.stream;

  Stream<AllClassResponse> get allClassStream => _allClassController.stream;

  Stream<ClassData> get selectedClassStream => _selectedClassController.stream;

  Stream<CommonSuccessResponse> get addSurgicalFormStream => _addSurgicalFormController.stream;

  Future<void> getAllSurgeonList(BuildContext context) async {
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getAllSurgeon(authToken);
    _loadingController.sink.add(false);
    if (response.success) {
      allSurgeonResponseSink.add(response);
      onSelectSurgeon(
          SharedPref.mInstance.getString(SharedPref.STORAGE_USER_ID));
    } else {
      errorMessageSink.add(response.message);
    }
  }

  void onSelectSurgeon(String surgeonId) {
    surgeonSink.add(surgeonId);
    getSurgeonAssistant(surgeonId);
  }

  Future<void> getSurgeonAssistant(String surgeonId) async {
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance()
        .getSurgeonAssistant(authToken, surgeonId);
    _loadingController.sink.add(false);
    if (response.success) {
      getSurgeonAssistantSink.add(response);
    } else {
      errorMessageSink.add(response.message);
    }
  }

  Future<void> getClassList(BuildContext context) async {
    // _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getAllClass(authToken);
    // _loadingController.sink.add(false);
    if (response.success) {
      allClassControllerSink.add(response);
      onSelectClass(response.data[0].id1);
    } else {
      errorMessageSink.add(response.message);
    }
  }

  void onSelectClass(String classId) {
    classSink.add(classId);
    if (classId != null) {
      selectedClassSink.add(_allClassController.value.data
          .firstWhere((element) => element.id1 == classId));
      getClassChildren(classId);
    } else {
      _boneClassDataListController.value.clear();
    }
  }

  Future<void> getClassChildren(String classId) async {
    final classDataList = _boneClassDataListController.value;
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response =
        await WebService.getInstance().getAllClassChildren(authToken, classId);
    _loadingController.sink.add(false);
    if (response.success) {
      if (response.data.isNotEmpty) {
        classDataList.clear();
        classDataList.add(BoneClassData(classList: response.data));
      } else {
        classDataList.clear();
      }
    } else {
      errorMessageSink.add(response.message);
    }
    boneClassDataListSink.add(classDataList);
  }

  Future<void> onSelectSubClass(int index, String classId) async {
    final classDataList = _boneClassDataListController.value;
    classDataList[index].selectedClassId = classId;
    selectedClassSink.add(classDataList[index]
        .classList
        .firstWhere((element) => element.id1 == classId));
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response =
        await WebService.getInstance().getAllClassChildren(authToken, classId);
    _loadingController.sink.add(false);
    if (response.success) {
      if (response.data.isNotEmpty) {
        if (index < classDataList.length - 1)
          classDataList.removeRange(index + 1, classDataList.length);
        classDataList.add(BoneClassData(classList: response.data));
      } else {
        classDataList.removeRange(index + 1, classDataList.length);
      }
    } else {
      errorMessageSink.add(response.message);
    }
    boneClassDataListSink.add(classDataList);
  }

  onClickSpecialityImage(BuildContext context, int index) {
    final specialityImageList = _specialityListController.value;
    if (index < specialityImageList.length) {
      FrequentUtils.getInstance().showDialogToChooseImageOption(context, () {
        Navigator.of(context).pop();
        FrequentUtils.getInstance()
            .showImageInFullScreen(context, null, specialityImageList[index]);
      }, () {
        Navigator.of(context).pop();
        _uploadSpecialityImage(context, index);
      });
    } else {
      _uploadSpecialityImage(context, index);
    }
  }

  void _uploadSpecialityImage(BuildContext context, int index) {
    ImagePickerPopup.showImagePickerDialog(context, (file) {
      if (file != null) {
        final specialityImageList = _specialityListController.value;
        if (index < specialityImageList.length) {
          specialityImageList[index] = file;
        } else {
          specialityImageList.add(file);
        }
        specialityListSink.add(specialityImageList);
      }
    });
  }

  onClickDiagnosisImage(BuildContext context, int index) {
    final diagnosisImageList = _diagnosisListController.value;
    if (index < diagnosisImageList.length) {
      FrequentUtils.getInstance().showDialogToChooseImageOption(context, () {
        Navigator.of(context).pop();
        FrequentUtils.getInstance()
            .showImageInFullScreen(context, null, diagnosisImageList[index]);
      }, () {
        Navigator.of(context).pop();
        _uploadDiagnosisImage(context, index);
      });
    } else {
      _uploadDiagnosisImage(context, index);
    }
  }

  void _uploadDiagnosisImage(BuildContext context, int index) {
    ImagePickerPopup.showImagePickerDialog(context, (file) {
      if (file != null) {
        final diagnosisImageList = _diagnosisListController.value;
        if (index < diagnosisImageList.length) {
          diagnosisImageList[index] = file;
        } else {
          diagnosisImageList.add(file);
        }
        diagnosisListSink.add(diagnosisImageList);
      }
    });
  }

  onClickAddTag(BuildContext context, String item) {
    final selectedList = _selectedListController.value;
    selectedList.add(item);
    selectedListSink.add(selectedList);
  }

  onClickRemoveTag(BuildContext context, int index) {
    final selectedList = _selectedListController.value;
    if (selectedList.length > 0) {
      selectedList.removeAt(index);
      selectedListSink.add(selectedList);
    }
  }

  Future<void> onClickSubmit(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (_firstNameController.value.isEmpty) {
      errorMessageSink.add("Please enter first name.");
    } else if (_lastNameController.value.isEmpty) {
      errorMessageSink.add("Please enter last name.");
    } else if (_dateOfBirthController.value==null) {
      errorMessageSink.add("Please enter date of birth.");
    } else if (_selectedAddressController.value==null) {
      errorMessageSink.add("Please enter address.");
    } else if (_surgeonController.value==null) {
      errorMessageSink.add("Please select surgeon.");
    } else if ((_getSurgeonAssistantController.value!=null && _getSurgeonAssistantController.value.data.isNotEmpty) && _assistantController.value==null) {
      errorMessageSink.add("Please select assistant.");
    } else if (_selectedClassController.value==null) {
      errorMessageSink.add("Please select fracture class.");
    } else if (_specialityListController.value.isEmpty) {
      errorMessageSink.add("Please enter speciality image.");
    } else if (_diagnosisListController.value.isEmpty) {
      errorMessageSink.add("Please enter diagnosis image.");
    } else if (_selectedListController.value.isEmpty) {
      errorMessageSink.add("Please select operation type.");
    } else {
      _loadingController.sink.add(true);
      final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
      final formRequest = AddSurgicalFormRequest(
        firstName: _firstNameController.value,
        lastName: _lastNameController.value,
        dateOfBirth: _dateOfBirthController.value.millisecondsSinceEpoch,
        patientLocation: _selectedAddressController.value.address,
        lat: _selectedAddressController.value.lat,
        long: _selectedAddressController.value.long,
        surgeon: _surgeonController.value,
        assistant: _assistantController.value??"",
        classId: _selectedClassController.value.id1,
        operationType: _selectedListController.value[0],
        notes: _notesController.value,
      );
      final response = await WebService.getInstance().addSurgicalForm(authToken,formRequest,_specialityListController.value[0],_diagnosisListController.value[0]);
      _loadingController.sink.add(false);
      if (response.success) {
        addSurgicalFormSink.add(response);
      } else {
        errorMessageSink.add(response.message);
      }
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _loadingController.close();
    _errorMessageController.close();
    _statusPickerController.close();
    _firstNameController.close();
    _lastNameController.close();
    _dateOfBirthController.close();
    _selectedAddressController.close();
    _surgeonController.close();
    _assistantController.close();
    _classController.close();
    _boneClassDataListController.close();
    _selectedListController.close();
    _specialityListController.close();
    _diagnosisListController.close();
    _allSurgeonResponseController.close();
    _getSurgeonAssistantController.close();
    _allClassController.close();
    _selectedClassController.close();
    _notesController.close();
    _addSurgicalFormController.close();
  }
}
