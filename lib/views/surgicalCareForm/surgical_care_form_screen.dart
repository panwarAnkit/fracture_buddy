import 'dart:convert';
import 'dart:io';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fracture_buddy/models/BoneClassData.dart';
import 'package:fracture_buddy/models/LocationData.dart';
import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/models/allSurgeon/AllSurgeonResponse.dart';
import 'package:fracture_buddy/models/getAssistants/GetAssistantResponse.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/no_grow_behavior.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/views/surgicalCareForm/surgical_care_form_bloc.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:lottie/lottie.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class SurgicalCareFormScreen extends StatefulWidget {
  @override
  State<SurgicalCareFormScreen> createState() => SurgicalCareFormScreenState();
}

class SurgicalCareFormScreenState extends State<SurgicalCareFormScreen>
    with SingleTickerProviderStateMixin {
  GlobalKey _menuKey = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  List<String> statusList = <String>[
    "Poor",
    "Okay",
    "Not Okay",
    "Good",
    "Very Good",
  ];

  List<String> suggestionsList = <String>[
    "ACL Reconstruction Surgery",
    "Knee Replacement Surgery",
    "Shoulder Replacement Surgery",
    "Hip Replacement Surgery",
    "Knee Arthroscopy",
    "Shoulder Arthroscopy",
    "Ankle Repair",
    "Spinal Surgeries",
    "Joint Fusion",
    "Trigger Finger Release",
  ];
  SurgicalCareFormBloc _bloc;

  @override
  void initState() {
    _bloc = SurgicalCareFormBloc(this);
    _initializeCommonListener(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final button = PopupMenuButton<String>(
      key: _menuKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.0),
        child: Image.asset(
          "assets/png/dropdown_arrow.png",
          width: 10,
          height: 20,
        ),
      ),
      padding: EdgeInsets.zero,
      offset: Offset(-85, -6),
      itemBuilder: (_) {
        return statusList.map((item) {
          return PopupMenuItem<String>(
              child: Text(item, style: textStyleQuicksandBlack14), value: item);
        }).toList();
      },
      onSelected: (value) {
        _bloc.statusPickerSink.add(value);
      },
    );
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: APP_PRIMARY_COLOR,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
          child: InkWell(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              splashColor: Colors.grey,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset("assets/png/back_arrow.png",
                    width: 24, height: 24),
              ),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ),
        elevation: 0,
        title: Stack(
          alignment: Alignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/png/stethoscope_transparent_white.png",
                  height: 100,
                  width: 90,
                ),
                Image.asset("assets/png/stethoscope_transparent_blue.png",
                    height: 80, width: 90),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: screenSize.width / 7.0),
              child: Text("Surgical Care Form", style: textStyleWhiteBold18),
            ),
          ],
        ),
        toolbarHeight: 80,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0)),
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Fill form data",
                      style: textStyleQuicksandBoldYellow16,
                    ),
                  ),
                  SizedBox(width: 10),
                  StreamBuilder<String>(
                    initialData: statusList[0],
                    stream: _bloc.statusPickerStream,
                    builder: (context, snapshot) {
                      return InkWell(
                        onTap: () {
                          dynamic state = _menuKey.currentState;
                          state.showButtonMenu();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0)),
                              border: Border.all(color: COLOR_YELLOW_DARK)),
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 8.0, right: 4.0, top: 4.0, bottom: 2.0),
                            child: Row(
                              children: <Widget>[
                                Center(
                                  child: Text(
                                      snapshot.hasData
                                          ? snapshot.data
                                          : "Status",
                                      style: textStyleGreyQuicksandBold14),
                                ),
                                SizedBox(width: 5.0),
                                button
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "First Name*",
                          style: textStyleGreyQuicksandBold14,
                        ),
                        TextField(
                          style: textStyleQuicksandBlack16,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          onChanged: (value) => _bloc.firstNameSink.add(value),
                          decoration: InputDecoration(
                            isDense: true,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Last Name*",
                          style: textStyleGreyQuicksandBold14,
                        ),
                        TextField(
                          style: textStyleQuicksandBlack16,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          onChanged: (value) => _bloc.lastNameSink.add(value),
                          decoration: InputDecoration(
                            isDense: true,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Date of Birth*",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  StreamBuilder<DateTime>(
                      stream: _bloc.dateOfBirthStream,
                      builder: (context, snapshot) {
                        final controller = TextEditingController();
                        controller.text = snapshot.hasData
                            ? FrequentUtils.getInstance()
                                .getDateWithSavedFormat(
                                    snapshot.data.millisecondsSinceEpoch)
                            : "";
                        return TextField(
                          style: textStyleQuicksandBlack16,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          controller: controller,
                          readOnly: true,
                          onTap: () {
                            showDatePickerDialog(context, snapshot.data);
                          },
                          decoration: InputDecoration(
                            isDense: true,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            suffixIconConstraints:
                                BoxConstraints(maxHeight: 30, maxWidth: 30),
                            suffixIcon: Padding(
                              padding: EdgeInsets.all(6.0),
                              child: InkWell(
                                focusColor: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                child: Image.asset(
                                  "assets/png/calendar_blue.png",
                                  height: 20,
                                  width: 20,
                                ),
                                onTap: () {
                                  showDatePickerDialog(context, snapshot.data);
                                },
                              ),
                            ),
                          ),
                        );
                      }),
                ],
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Patient Location*",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  StreamBuilder<LocationData>(
                      stream: _bloc.selectedAddressStream,
                      builder: (context, snapshot) {
                        final controller = TextEditingController();
                        controller.text =
                            snapshot.hasData ? snapshot.data.address : "";
                        return TextField(
                          style: textStyleQuicksandBlack16,
                          keyboardType: TextInputType.text,
                          readOnly: true,
                          onTap: () {
                            showLocationPickerDialog(context, snapshot.data);
                          },
                          textCapitalization: TextCapitalization.words,
                          controller: controller,
                          decoration: InputDecoration(
                            isDense: true,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.5, color: COLOR_LIGHT_GRAY),
                            ),
                            suffixIconConstraints:
                                BoxConstraints(maxHeight: 30, maxWidth: 30),
                            suffixIcon: Padding(
                              padding: EdgeInsets.all(6.0),
                              child: InkWell(
                                focusColor: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                child: Image.asset(
                                  "assets/png/location_pin_blue.png",
                                  height: 20,
                                  width: 20,
                                ),
                                onTap: () {
                                  showLocationPickerDialog(
                                      context, snapshot.data);
                                },
                              ),
                            ),
                          ),
                        );
                      }),
                ],
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Surgeon*",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  StreamBuilder<AllSurgeonResponse>(
                      stream: _bloc.allSurgeonResponseStream,
                      builder: (context, mainSnapshot) {
                        if (mainSnapshot.hasData) {
                          return Container(
                            child: StreamBuilder<String>(
                              stream: _bloc.surgeonStream,
                              builder: (context, snapshot) {
                                return DropdownButton<String>(
                                  underline: Container(
                                    color: COLOR_LIGHT_GRAY,
                                    height: 1.5,
                                  ),
                                  value: snapshot.data,
                                  isExpanded: true,
                                  icon: Image.asset(
                                    "assets/png/drop_down_light.png",
                                    width: 16,
                                    height: 16,
                                  ),
                                  onChanged: _bloc.onSelectSurgeon,
                                  items: mainSnapshot.data.data.map((value) {
                                    return DropdownMenuItem<String>(
                                      value: value.id1,
                                      child: Text(
                                        value.firstName + " " + value.lastName,
                                        style: textStyleQuicksandBlack16,
                                      ),
                                    );
                                  }).toList(),
                                );
                              },
                            ),
                          );
                        } else {
                          return Center(
                            child: Container(
                              height: 24,
                              width: 24,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                              ),
                            ),
                          );
                        }
                      }),
                ],
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Assistant*",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  StreamBuilder<GetAssistantResponse>(
                    stream: _bloc.getSurgeonAssistantStream,
                    builder: (context, mainSnapshot) {
                      if (mainSnapshot.hasData) {
                        if (mainSnapshot.data.data.isNotEmpty) {
                          return Container(
                            child: StreamBuilder<String>(
                              stream: _bloc.assistantStream,
                              builder: (context, snapshot) {
                                return DropdownButton<String>(
                                  underline: Container(
                                    color: COLOR_LIGHT_GRAY,
                                    height: 1.5,
                                  ),
                                  value: snapshot.data,
                                  isExpanded: true,
                                  icon: Image.asset(
                                      "assets/png/drop_down_light.png",
                                      width: 16,
                                      height: 16),
                                  onChanged: (value) =>
                                      _bloc.assistantSink.add(value),
                                  items: mainSnapshot.data.data.map((value) {
                                    return DropdownMenuItem<String>(
                                      value: value.id,
                                      child: Text(
                                        value.assistantName,
                                        style: textStyleQuicksandBlack16,
                                      ),
                                    );
                                  }).toList(),
                                );
                              },
                            ),
                          );
                        } else {
                          return Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "No assistant available.",
                                  style: textStyleQuicksandLightGrey16,
                                ),
                                SizedBox(height: 6),
                                Container(
                                  color: COLOR_LIGHT_GRAY,
                                  height: 1.5,
                                ),
                              ],
                            ),
                          );
                        }
                      } else {
                        return Center(
                          child: Container(
                            height: 24,
                            width: 24,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Class*",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  StreamBuilder<AllClassResponse>(
                    stream: _bloc.allClassStream,
                    builder: (context, mainSnapshot) {
                      if (mainSnapshot.hasData) {
                        return Container(
                          child: StreamBuilder<String>(
                            stream: _bloc.classStream,
                            builder: (context, snapshot) {
                              return DropdownButton<String>(
                                underline: Container(
                                  color: COLOR_LIGHT_GRAY,
                                  height: 1.5,
                                ),
                                value: snapshot.data,
                                isExpanded: true,
                                icon: Image.asset(
                                    "assets/png/drop_down_light.png",
                                    width: 16,
                                    height: 16),
                                onChanged: _bloc.onSelectClass,
                                items: mainSnapshot.data.data.map((value) {
                                  return DropdownMenuItem<String>(
                                    value: value.id1,
                                    child: Text(
                                      value.legend,
                                      style: textStyleQuicksandBlack16,
                                    ),
                                  );
                                }).toList(),
                              );
                            },
                          ),
                        );
                      } else {
                        return Center(
                          child: Container(
                            height: 24,
                            width: 24,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: 15),
              StreamBuilder<List<BoneClassData>>(
                initialData: [],
                stream: _bloc.boneClassDataListStream,
                builder: (context, classDataSnapshot) {
                  return ScrollConfiguration(
                    behavior: NoGrowBehavior(),
                    child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: classDataSnapshot.data.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        final classData = classDataSnapshot.data[index];
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Sub Class*",
                              style: textStyleGreyQuicksandBold14,
                            ),
                            Builder(
                              builder: (_) {
                                if (classData.classList.isNotEmpty) {
                                  return Builder(
                                    builder: (_) {
                                      return DropdownButton<String>(
                                        underline: Container(
                                          color: COLOR_LIGHT_GRAY,
                                          height: 1.5,
                                        ),
                                        value: classData != null
                                            ? classData.selectedClassId
                                            : null,
                                        isExpanded: true,
                                        icon: Image.asset(
                                          "assets/png/drop_down_light.png",
                                          width: 16,
                                          height: 16,
                                        ),
                                        onChanged: (value) => _bloc
                                            .onSelectSubClass(index, value),
                                        items: classData.classList.map((value) {
                                          return DropdownMenuItem<String>(
                                            value: value.id1,
                                            child: Text(
                                              value.legend,
                                              style: textStyleQuicksandBlack16,
                                            ),
                                          );
                                        }).toList(),
                                      );
                                    },
                                  );
                                } else {
                                  return Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "No sub class available.",
                                          style: textStyleQuicksandLightGrey16,
                                        ),
                                        SizedBox(height: 6),
                                        Container(
                                          color: COLOR_LIGHT_GRAY,
                                          height: 1.5,
                                        ),
                                      ],
                                    ),
                                  );
                                }
                              },
                            ),
                          ],
                        );
                      },
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 15);
                      },
                    ),
                  );
                },
              ),
              SizedBox(height: 15),
              Row(
                children: [
                  Expanded(
                    child: Text("Or Choose class from",
                        style: textStyleGreyQuicksand14),
                  ),
                  SizedBox(width: 10),
                  InkWell(
                    onTap: () => onClickSkeletonView(context, _bloc),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        color: COLOR_YELLOW_EXTRA_LIGHT,
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 12.0),
                        child: Row(
                          children: <Widget>[
                            Center(
                              child: Text("Skeleton View",
                                  style: textStyleQuicksandBoldYellowDark14),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15),
              Container(
                color: COLOR_LIGHT_GRAY,
                height: 1.5,
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Selected Class",
                    style: textStyleQuicksandBlack16,
                  ),
                  SizedBox(height: 8),
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      border: Border.all(width: 1.5, color: APP_PRIMARY_COLOR),
                    ),
                    child: StreamBuilder<ClassData>(
                      stream: _bloc.selectedClassStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Padding(
                            padding: const EdgeInsets.all(8),
                            child: Row(
                              children: [
                                CachedNetworkImage(
                                  imageUrl: APP_IMAGE_URL + snapshot.data.image,
                                  placeholder: (context, url) => Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(30.0),
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, value) {
                                    return SvgPicture.asset(
                                      "assets/svg/caution.svg",
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Legend",
                                        style: textStyleGreyQuicksand12,
                                      ),
                                      SizedBox(height: 4),
                                      Text(
                                        snapshot.data.legend,
                                        style: textStyleQuicksandBlack14,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        } else {
                          return Center(
                            child: Padding(
                              padding: EdgeInsets.all(12),
                              child: Text("No Class Selected."),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                color: COLOR_LIGHT_GRAY,
                height: 1.5,
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Speciality",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  SizedBox(height: 5),
                  Text(
                    "Fracture classification",
                    style: textStyleGreyQuicksand12,
                  ),
                  SizedBox(height: 10),
                  StreamBuilder<List<File>>(
                      initialData: [],
                      stream: _bloc.specialityListStream,
                      builder: (context, snapshot) {
                        return Container(
                          width: double.maxFinite,
                          height: 90,
                          child: ListView.separated(
                            itemCount: 1,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () => _bloc.onClickSpecialityImage(
                                    context, index),
                                child: Container(
                                    width: 90,
                                    height: 90,
                                    margin: EdgeInsets.only(right: 10),
                                    decoration: BoxDecoration(
                                      color: COLOR_LIGHT_GRAY,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                      child: index < snapshot.data.length
                                          ? Image.file(
                                              snapshot.data[index],
                                              fit: BoxFit.cover,
                                            )
                                          : Center(
                                              child: Image.asset(
                                                  "assets/png/photo.png",
                                                  width: 30,
                                                  height: 30),
                                            ),
                                    )),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(width: 5);
                            },
                          ),
                        );
                      })
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Diagnosis",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  SizedBox(height: 5),
                  Text(
                    "Fracture classification",
                    style: textStyleGreyQuicksand12,
                  ),
                  SizedBox(height: 10),
                  StreamBuilder<List<File>>(
                      initialData: [],
                      stream: _bloc.diagnosisListStream,
                      builder: (context, snapshot) {
                        return Container(
                          width: double.maxFinite,
                          height: 90,
                          child: ListView.separated(
                            shrinkWrap: true,
                            itemCount: 1,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () =>
                                    _bloc.onClickDiagnosisImage(context, index),
                                child: Container(
                                  width: 90,
                                  height: 90,
                                  decoration: BoxDecoration(
                                    color: COLOR_LIGHT_GRAY,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8.0),
                                    ),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8.0),
                                    ),
                                    child: index < snapshot.data.length
                                        ? Image.file(
                                            snapshot.data[index],
                                            fit: BoxFit.cover,
                                          )
                                        : Center(
                                            child: Image.asset(
                                                "assets/png/photo.png",
                                                width: 30,
                                                height: 30),
                                          ),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(width: 5);
                            },
                          ),
                        );
                      })
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Operation Type",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  AutoCompleteTextField<String>(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10.0),
                      isDense: true,
                      hintText: "Select tags",
                      hintStyle: textStyleLightGreyQuicksand14,
                    ),
                    textCapitalization: TextCapitalization.words,
                    itemSubmitted: (item) => _bloc.onClickAddTag(context, item),
                    key: key,
                    suggestions: suggestionsList,
                    itemBuilder: (context, suggestion) => ListTile(
                      title: Text(suggestion),
                      dense: true,
                    ),
                    itemSorter: (a, b) => a.compareTo(b),
                    itemFilter: (suggestion, input) =>
                        suggestion.toLowerCase().contains(input.toLowerCase()),
                  ),
                  SizedBox(height: 5),
                  StreamBuilder<List<String>>(
                    initialData: [],
                    stream: _bloc.selectedListStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data.isNotEmpty) {
                        return Container(
                          height: 40,
                          child: Center(
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                return Wrap(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(4.0),
                                        ),
                                        color: APP_PRIMARY_COLOR_LIGHT,
                                        border: Border.all(
                                            color: APP_PRIMARY_COLOR,
                                            width: 1.0),
                                      ),
                                      margin: EdgeInsets.all(4.0),
                                      padding: EdgeInsets.only(
                                          top: 2.0,
                                          bottom: 2.0,
                                          left: 8.0,
                                          right: 8.0),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            snapshot.data[index],
                                            style: textStyleQuicksandBlack12,
                                          ),
                                          SizedBox(width: 6.0),
                                          InkWell(
                                            onTap: () => _bloc.onClickRemoveTag(
                                                context, index),
                                            child: Padding(
                                              padding: EdgeInsets.all(2.0),
                                              child: Image.asset(
                                                "assets/png/cross_blue.png",
                                                width: 15,
                                                height: 15,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  )
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Notes",
                    style: textStyleGreyQuicksandBold14,
                  ),
                  SizedBox(height: 8),
                  TextField(
                    style: textStyleQuicksandBlack16,
                    keyboardType: TextInputType.multiline,
                    maxLines: 3,
                    textCapitalization: TextCapitalization.words,
                    onChanged: (value) => _bloc.notesSink.add(value),
                    decoration: InputDecoration(
                      isDense: true,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 1.5, color: COLOR_LIGHT_GRAY),
                      ),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 1.5, color: COLOR_LIGHT_GRAY),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
              Hero(
                tag: APP_NAME,
                child: Material(
                  elevation: 5.0,
                  color: APP_PRIMARY_COLOR,
                  shadowColor: APP_PRIMARY_COLOR_LIGHT,
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                  child: InkWell(
                    onTap: () => _bloc.onClickSubmit(context),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                    splashColor: APP_PRIMARY_COLOR_DARK,
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(15.0),
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(width: 50),
                            Text(
                              "Submit",
                              style: textStyleWhite16,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(width: 20.0),
                            RotatedBox(
                              quarterTurns: 45,
                              child: Lottie.asset('assets/json/arrow_up.json',
                                  width: 30, height: 30),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showDatePickerDialog(context, DateTime dateTime) async {
    var datePicked = await showDatePicker(
        context: context,
        initialDate: dateTime ?? DateTime(2000),
        firstDate: DateTime(1900),
        lastDate: DateTime(DateTime.now().year - 1));
    if (datePicked != null) {
      _bloc.dateOfBirthSink.add(datePicked);
    }
  }

  Future<void> showLocationPickerDialog(
      BuildContext context, LocationData data) async {
    Prediction prediction = await PlacesAutocomplete.show(
      context: context,
      apiKey: GOOGLE_MAP_API_KEY,
      mode: Mode.overlay,
      // Mode.overlay
      language: "en",
      components: [Component(Component.country, "ca")],
    );
    if (prediction != null) {
      GoogleMapsPlaces _places = new GoogleMapsPlaces(
          apiKey: GOOGLE_MAP_API_KEY); //Same API_KEY as above
      PlacesDetailsResponse result =
          await _places.getDetailsByPlaceId(prediction.placeId);
      if (result.isOkay) {
        double latitude = result.result.geometry.location.lat;
        double longitude = result.result.geometry.location.lng;
        String address = prediction.description;
        final locationData = LocationData(
            address: address,
            lat: latitude,
            long: longitude,
            placeId: result.result.placeId);
        _bloc.selectedAddressSink.add(locationData);
      }
    }
  }

  onClickSkeletonView(BuildContext context, SurgicalCareFormBloc bloc) async {
    dynamic result =
        await Navigator.of(context).pushNamed(AppRoute.skeletonView);
    if (result != null) {
      _bloc.selectedClassSink.add(result as ClassData);
      _bloc.onSelectClass(null);
    }
  }

  _initializeCommonListener(BuildContext context) {
    //get all surgeon list
    _bloc.getAllSurgeonList(context);
    //get all class list
    _bloc.getClassList(context);
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //add surgical form response
    _bloc.addSurgicalFormStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        FrequentUtils.getInstance().showToast(response.message);
        Navigator.of(context).pop();
      }
    });
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
