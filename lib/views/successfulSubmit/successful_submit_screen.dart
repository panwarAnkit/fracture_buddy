import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:lottie/lottie.dart';

class SuccessfulSubmitScreen extends StatefulWidget {
  @override
  State<SuccessfulSubmitScreen> createState() => SuccessfulSubmitScreenState();
}

class SuccessfulSubmitScreenState extends State<SuccessfulSubmitScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingBottomToUpAnimation,
      slidingUpToBottomAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600), reverseDuration: Duration(milliseconds: 300));
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(controller);
    slidingBottomToUpAnimation =
        Tween<Offset>(begin: Offset(0.0, 5.0), end: Offset(0.0, 0.0))
            .animate(controller);
    slidingUpToBottomAnimation =
        Tween<Offset>(begin: Offset(0.0, -5.0), end: Offset(0.0, 0.0))
            .animate(controller);
    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: APP_PRIMARY_COLOR_DARK,
      body: SafeArea(
          child: Center(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 20,horizontal: 30.0),
          shrinkWrap: true,
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                SlideTransition(
                  position: slidingLeftToRightAnimation,
                  child: Container(
                    height: screenSize.width / 1.3,
                    width: screenSize.width / 1.3,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular((screenSize.width / 1.3) / 2),
                      color: APP_PRIMARY_COLOR_MIX,
                    ),
                  ),
                ),
                SlideTransition(
                  position: slidingRightToLeftAnimation,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 2.0),
                    height: screenSize.width / 1.9,
                    width: screenSize.width / 1.9,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular((screenSize.width / 1.9) / 2),
                      color: APP_PRIMARY_COLOR_MIX2,
                    ),
                    child: SlideTransition(
                      position: slidingUpToBottomAnimation,
                      child: Center(
                        child: Container(
                          height: screenSize.width / 3.2,
                          width: screenSize.width / 3.2,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.circular((screenSize.width / 3.2) / 2),
                            color: APP_PRIMARY_COLOR,
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: 20.0, left: 16.0, right: 12.0, bottom: 12.0),
                            child: Image.asset(
                              "assets/png/stethoscope_white.png",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 50.0),
            SlideTransition(
              position: slidingLeftToRightAnimation,
              child: Text(
                "Successfully submitted!",
                style: textStyleLightBlueAvenik26,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 10.0),
            SlideTransition(
              position: slidingRightToLeftAnimation,
              child: Text(
                  "Your profile is waiting to be reviewed.\nOnce approved, you will be provided\naccess  to our system.",
                  style: textStyleWhite16,
                  textAlign: TextAlign.center),
            ),
            SizedBox(height: 40.0),
            SlideTransition(
              position: slidingBottomToUpAnimation,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Hero(
                  tag: APP_NAME,
                  child: Material(
                    elevation: 5.0,
                    color: APP_PRIMARY_COLOR,
                    shadowColor: APP_PRIMARY_COLOR_LIGHT,
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                    child: InkWell(
                      onTap: () => onClickGoToHome(context),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15.0),
                      ),
                      splashColor: APP_PRIMARY_COLOR_DARK,
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15.0),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(width: 50.0),
                              Text(
                                "Go to Home",
                                style: textStyleWhite16,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(width: 20.0),
                              RotatedBox(
                                quarterTurns: 45,
                                child: Lottie.asset('assets/json/arrow_up.json',
                                    width: 30, height: 30),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  Future<void> onClickGoToHome(BuildContext context) async {
    await controller.reverse();
    await Navigator.of(context).pushNamed(AppRoute.awaitingApproval);
    controller.forward();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
