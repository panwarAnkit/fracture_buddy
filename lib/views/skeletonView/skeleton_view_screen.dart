import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/human_anatomy/human_anatomy.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/views/skeletonView/skeleton_view_bloc.dart';

class SkeletonViewScreen extends StatefulWidget {
  @override
  State<SkeletonViewScreen> createState() => SkeletonViewScreenState();
}

class SkeletonViewScreenState extends State<SkeletonViewScreen> with SingleTickerProviderStateMixin{

  final GlobalKey<HumanAnatomyState> _humanAnatomyKey = GlobalKey<HumanAnatomyState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  SkeletonViewBloc _bloc;

  @override
  void initState() {
    _bloc = SkeletonViewBloc(this);
    _initializeCommonListener(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: APP_PRIMARY_COLOR,
      appBar: AppBar(
        leading: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
          child: InkWell(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              splashColor: Colors.grey,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Image.asset("assets/png/back_arrow.png",
                    width: 24, height: 24),
              ),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ),
        elevation: 0,
        title: Stack(
          alignment: Alignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/png/stethoscope_transparent_white.png",
                  height: 100,
                  width: 90,
                ),
                Image.asset("assets/png/stethoscope_transparent_blue.png",
                    height: 80, width: 90),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: screenSize.width / 7.0),
              child: Text("Choose class",
                  style: textStyleWhiteBold18),
            ),
          ],
        ),
        toolbarHeight: 80,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0)),
          color: Colors.white,
        ),
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(
              padding: EdgeInsets.all(24.0),
              child: Center(child: Text("Human Skeleton", style: textStyleQuicksandBoldYellow16)),
            ),
            Wrap(
              children: [
                Stack(
                  children: [
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top: screenSize.height/8),
                        width: screenSize.width / 1.2,
                        height: screenSize.width / 1.2,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(screenSize.width),
                          ),
                          border: Border.all(
                            color: COLOR_YELLOW_EXTRA_LIGHT_40,
                            width: 15,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(20.0),
                      child: Center(
                        child: InteractiveViewer(
                          maxScale: 4.0,
                          minScale: 1.0,
                          scaleEnabled: true,
                          child: HumanAnatomy(
                            key: _humanAnatomyKey,
                            onClickBone: (value)=>_bloc.onClickBoneFromSkeleton(context,value,_humanAnatomyKey),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.all(24.0),
              child: FloatingActionButton(
                elevation: 4.0,
                heroTag: APP_NAME,
                mini: true,
                child: Image.asset(
                  "assets/png/cross.png",
                  color: APP_PRIMARY_COLOR,
                  width: 15,
                  height: 15,
                ),
                backgroundColor: Colors.white,
                onPressed: () => Navigator.pop(context),
              ),
            )
          ],
        ),
      ),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //get all class list
    _bloc.getClassList(context);
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //register user response
    // _bloc.registerUserResponseStream.listen((response) async {
    //   if (response.isNotNull() && response.success) {
    //     await FrequentUtils.getInstance().saveUserData(response.data);
    //     FrequentUtils.getInstance().showToast(response.message);
    //   }
    // });
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
