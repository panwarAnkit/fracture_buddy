import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/BoneClassData.dart';
import 'package:fracture_buddy/models/allClass/AllClassResponse.dart';
import 'package:fracture_buddy/models/allClass/ClassData.dart';
import 'package:fracture_buddy/models/allSurgeon/AllSurgeonResponse.dart';
import 'package:fracture_buddy/models/getAssistants/GetAssistantResponse.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/image_picker_popup.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';

class SkeletonViewBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation;

  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<String> _classController;
  BehaviorSubject<List<BoneClassData>> _boneClassDataListController;
  BehaviorSubject<AllClassResponse> _allClassController;
  BehaviorSubject<AllClassResponse> _getClassChildrenController;

  SkeletonViewBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 600),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingTopRightToCenterAnimation =
        Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0))
            .animate(_controller);

    _loadingController = BehaviorSubject<bool>.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _classController = BehaviorSubject<String>();
    _boneClassDataListController =
        BehaviorSubject<List<BoneClassData>>.seeded(List());
    _allClassController = BehaviorSubject();
    _getClassChildrenController = BehaviorSubject();
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  StreamSink<String> get classSink => _classController.sink;

  StreamSink<List<BoneClassData>> get boneClassDataListSink =>
      _boneClassDataListController.sink;

  StreamSink<AllClassResponse> get allClassControllerSink =>
      _allClassController.sink;

  StreamSink<AllClassResponse> get getClassChildrenSink =>
      _getClassChildrenController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<String> get classStream => _classController.stream;

  Stream<List<BoneClassData>> get boneClassDataListStream =>
      _boneClassDataListController.stream;

  Stream<AllClassResponse> get allClassStream => _allClassController.stream;

  Stream<AllClassResponse> get getClassChildrenStream =>
      _getClassChildrenController.stream;

  Future<void> getClassList(BuildContext context) async {
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getAllClass(authToken);
    _loadingController.sink.add(false);
    if (response.success) {
      allClassControllerSink.add(response);
      onSelectClass(response.data[0].id1);
    } else {
      errorMessageSink.add(response.message);
    }
  }

  void onSelectClass(String classId) {
    classSink.add(classId);
    getClassChildren(classId);
  }

  Future<void> getClassChildren(String classId) async {
    final classDataList = _boneClassDataListController.value;
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response =
        await WebService.getInstance().getAllClassChildren(authToken, classId);
    _loadingController.sink.add(false);
    if (response.success) {
      if (response.data.isNotEmpty) {
        classDataList.clear();
        classDataList.add(BoneClassData(classList: response.data));
      } else {
        classDataList.clear();
      }
    } else {
      errorMessageSink.add(response.message);
    }
    boneClassDataListSink.add(classDataList);
  }

  Future<void> onSelectSubClass(int index, String classId) async {
    final classDataList = _boneClassDataListController.value;
    classDataList[index].selectedClassId = classId;
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response =
        await WebService.getInstance().getAllClassChildren(authToken, classId);
    _loadingController.sink.add(false);
    if (response.success) {
      if (response.data.isNotEmpty) {
        if (index < classDataList.length - 1)
          classDataList.removeRange(index + 1, classDataList.length);
        classDataList.add(BoneClassData(classList: response.data));
      } else {
        classDataList.removeRange(index + 1, classDataList.length);
      }
    } else {
      errorMessageSink.add(response.message);
    }
    boneClassDataListSink.add(classDataList);
  }

  Future<void> onClickBoneFromSkeleton(
      BuildContext context, String value, _humanAnatomyKey) async {
    print("Clicked Bone: " + value);
    _humanAnatomyKey.currentState.setState(() {
      _humanAnatomyKey.currentState.bodyPartList.clear();
    });
    dynamic result = await Navigator.of(context).pushNamed(AppRoute.boneView,
        arguments: {
          KEY_FROM: VALUE_SKELETON,
          KEY_DATA: _allClassController.value.data[0]
        });
    if (result != null && result[KEY_FROM] == VALUE_BONE_LAST) {
      ClassData selectedClassData = result[KEY_DATA];
      print("Selected Bone: " + selectedClassData.legend);
      Navigator.of(context).pop(selectedClassData);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _loadingController.close();
    _errorMessageController.close();
    _classController.close();
    _boneClassDataListController.close();
    _allClassController.close();
    _getClassChildrenController.close();
  }
}
