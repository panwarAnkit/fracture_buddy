import 'package:flutter/material.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:lottie/lottie.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';
import 'awaiting_approval_bloc.dart';

class AwaitingApprovalScreen extends StatefulWidget {
  @override
  State<AwaitingApprovalScreen> createState() => AwaitingApprovalScreenState();
}

class AwaitingApprovalScreenState extends State<AwaitingApprovalScreen>
    with SingleTickerProviderStateMixin {

  AwaitingApprovalBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _bloc =  AwaitingApprovalBloc(this);
    _initializeCommonListener(context);
    _bloc.startAnimationForward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: (){
            return _bloc.getProfileData(context);
          },
          child: Center(
            child: ListView(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 40.0),
              shrinkWrap: true,
              children: [
                Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    SlideTransition(
                      position: _bloc.slidingUpToBottomAnimation,
                      child: Container(
                        height: screenSize.width / 1.12,
                        width: screenSize.width / 1.12,
                        child: Image.asset("assets/png/bg_circles.png",
                            fit: BoxFit.fill),
                      ),
                    ),
                    SlideTransition(
                      position: _bloc.slidingBottomToUpAnimation,
                      child: Container(
                        height: screenSize.width / 1.4,
                        width: screenSize.width / 1.4,
                        child: Image.asset("assets/png/doctor_underline.png"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 50.0),
                SlideTransition(
                  position: _bloc.slidingLeftToRightAnimation,
                  child: Text(
                    "Awaiting Approval",
                    style: textStyleAvenikYellow26,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 10.0),
                SlideTransition(
                  position: _bloc.slidingRightToLeftAnimation,
                  child: Text("Your account is under review!",
                      style: textStyleGreyQuicksandBold14,
                      textAlign: TextAlign.center),
                ),
                SizedBox(height: 80.0),
                SlideTransition(
                  position: _bloc.slidingBottomToUpAnimation,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Hero(
                      tag: APP_NAME,
                      child: StreamBuilder<bool>(
                        stream: _bloc.buttonEnabledStream,
                        builder: (context, snapshot) {
                          return Material(
                            elevation: 5.0,
                            color: COLOR_BROWN,
                            shadowColor: Colors.grey,
                            borderRadius: BorderRadius.all(
                              Radius.circular(15.0),
                            ),
                            child: InkWell(
                              onTap: ()  {
                                if(snapshot.hasData && snapshot.data) {
                                  onClickSurgicalForm(context);
                                }else{
                                  FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, "Please wait! Your profile is under review.");
                                }
                              },
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                              splashColor: Colors.brown,
                              child: Container(
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(15.0),
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(width: 40.0),
                                      Text(
                                        "Surgical Care Form",
                                        style: textStyleWhite16,
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(width: 20.0),
                                      RotatedBox(
                                        quarterTurns: 45,
                                        child: Lottie.asset(
                                            'assets/json/arrow_up.json',
                                            width: 30,
                                            height: 30,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        }
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                Center(
                  child: InkWell(
                    onTap: () => _onClickLogout(context),
                    borderRadius:
                    BorderRadius.all(Radius.circular(4.0)),
                    child: Padding(
                      padding: EdgeInsets.all(4.0),
                      child: Text("Log Out",
                          style: textStyleQuicksandBlueBoldUL16),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if(value){
        FrequentUtils.getInstance().showProgressDialog(context);
      }else{
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //response
    _bloc.responseStream.listen((response) async {
      if (response.isNotNull() && response.data.isApproved) {
        FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, "Your profile has been approved.");
        await FrequentUtils.getInstance().saveUserData(response.data);
        _bloc.buttonEnabledSink.add(true);
      }else{
        FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, "Please wait! Your profile is under review.");
        _bloc.buttonEnabledSink.add(false);
      }
    });
  }

  Future<void> onClickSurgicalForm(BuildContext context) async {
    await _bloc.startAnimationReverse();
    await Navigator.of(context).pushNamedAndRemoveUntil(AppRoute.homeScreen, (_)=>false);
    _bloc.startAnimationForward();
  }

  _onClickLogout(BuildContext context){
    SharedPref.mInstance.clearData();
    Navigator.of(context)
        .pushNamedAndRemoveUntil(AppRoute.createAccount, (_) => false);
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
