import 'dart:async';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserResponse.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class AwaitingApprovalBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingBottomToUpAnimation,
      slidingUpToBottomAnimation;

  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<bool> _buttonEnabledController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<RegisterUserResponse> _responseController;

  AwaitingApprovalBloc(TickerProvider provider) {
    _controller = AnimationController(
        vsync: provider,
        duration: Duration(milliseconds: 700),
        reverseDuration: Duration(milliseconds: 300));
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingBottomToUpAnimation =
        Tween<Offset>(begin: Offset(0.0, 5.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingUpToBottomAnimation =
        Tween<Offset>(begin: Offset(0.0, -5.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    _buttonEnabledController = BehaviorSubject.seeded(false);
    _loadingController = BehaviorSubject.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _responseController = BehaviorSubject();
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<bool> get buttonEnabledSink => _buttonEnabledController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<bool> get buttonEnabledStream => _buttonEnabledController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<RegisterUserResponse> get responseStream => _responseController.stream;

  TickerFuture startAnimationForward() {
    return _controller.forward();
  }

  TickerFuture startAnimationReverse() {
    return _controller.reverse();
  }

  Future<void> getProfileData(BuildContext context) async {
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().getProfileData(authToken);
    _loadingController.sink.add(false);
    if (response.success) {
      _responseController.add(response);
    } else {
      _errorMessageController.sink.add(response.message);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _buttonEnabledController.close();
    _loadingController.close();
    _errorMessageController.close();
    _responseController.close();
  }
}
