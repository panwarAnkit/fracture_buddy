import 'dart:io';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fracture_buddy/models/serviceList/AllServicesResponse.dart';
import 'package:fracture_buddy/models/serviceList/Hospital.dart';
import 'package:fracture_buddy/models/serviceList/Speciality.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/utils/us_number_text_formatter.dart';
import 'package:lottie/lottie.dart';

import 'complete_profile_bloc.dart';

class CompleteProfileScreen extends StatefulWidget {
  String phone = "";

  CompleteProfileScreen({this.phone});

  @override
  State<CompleteProfileScreen> createState() => CompleteProfileScreenState();
}

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}

class CompleteProfileScreenState extends State<CompleteProfileScreen>
    with SingleTickerProviderStateMixin {
  CompleteProfileBloc _bloc;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<AutoCompleteTextFieldState<Hospital>> _hospitalKey =
      new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<Speciality>> _specialityKey =
      new GlobalKey();
  TextEditingController _phoneController;

  List<Hospital> _selectedHospitalList = List();
  List<Speciality> _selectedSpecialityList = List();

  @override
  void initState() {
    _bloc = CompleteProfileBloc(this);
    _initializeCommonListener(context);
    _bloc.startAnimationForward();
    _phoneController = getDefaultPhoneController();
    _bloc.phoneSink.add(_phoneController.text);
    _bloc.getAllServiceList(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      body: Builder(builder: (context){
        return SafeArea(
          child: Container(
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: SlideTransition(
                    position: _bloc.slidingTopRightToCenterAnimation,
                    child: Image.asset(
                      "assets/png/bg_top_right.png",
                      height: screenSize.width / 1.2,
                      width: screenSize.width / 1.59,
                    ),
                  ),
                ),
                Container(
                  child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: 25.0),
                    physics: BouncingScrollPhysics(),
                    children: [
                      SizedBox(height: 40),
                      SlideTransition(
                        position: _bloc.slidingLeftToRightAnimation,
                        child: ListView(
                          shrinkWrap: true,
                          children: [
                            Text("Complete your\nProfile",
                                style: textStyleBlueAvenikBold26),
                            SizedBox(height: 10.0),
                            Text("Basic Information",
                                style: textStyleQuicksandBlack16),
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                      SlideTransition(
                        position: _bloc.slidingRightToLeftAnimation,
                        child: Column(
                          children: [
                            Container(
                              height: 120,
                              width: 120,
                              decoration: BoxDecoration(
                                color: COLOR_LIGHT_GRAY,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20),
                                ),
                              ),
                              child: StreamBuilder<File>(
                                stream: _bloc.profilePicStream,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData && snapshot.data != null) {
                                    return ClipRRect(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(20.0)),
                                      child: Image.file(snapshot.data,
                                          fit: BoxFit.cover),
                                    );
                                  }
                                  return Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Center(
                                      child: Lottie.asset(
                                        'assets/json/doctor_bottom_up.json',
                                        width: 60,
                                        height: 60,
                                        repeat: false,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            FlatButton(
                              onPressed: () => _bloc.onClickUploadPhoto(context),
                              child: Text("Upload Photo",
                                  style: textStyleGreyQuicksandBold14),
                            )
                          ],
                        ),
                      ),
                      SlideTransition(
                        position: _bloc.slidingLeftToRightAnimation,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 40),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "User Id*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                TextField(
                                  style: textStyleQuicksandBlack16,
                                  keyboardType: TextInputType.text,
                                  onChanged: (s) => _bloc.userNameSink.add(s),
                                  decoration: InputDecoration(
                                    isDense: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "First Name*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                TextField(
                                  style: textStyleQuicksandBlack16,
                                  keyboardType: TextInputType.text,
                                  onChanged: (s) => _bloc.firstNameSink.add(s),
                                  textCapitalization: TextCapitalization.words,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Last Name*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                TextField(
                                  style: textStyleQuicksandBlack16,
                                  keyboardType: TextInputType.text,
                                  onChanged: (s) => _bloc.lastNameSink.add(s),
                                  textCapitalization: TextCapitalization.words,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Phone Number*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                Row(
                                  children: [
                                    StreamBuilder<Country>(
                                      stream: _bloc.countryPickerStream,
                                      builder: (context, snapshot) {
                                        return InkWell(
                                          onTap: () => _openCountryPickerDialog(
                                              context, _bloc),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(4.0)),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border(
                                                bottom: BorderSide(
                                                    width: 1.5,
                                                    color: COLOR_LIGHT_GRAY),
                                              ),
                                            ),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 4.0, vertical: 8.0),
                                            child: Row(
                                              children: [
                                                Text(
                                                  FrequentUtils.getInstance()
                                                      .countryCodeToEmoji(
                                                      snapshot.hasData
                                                          ? snapshot.data
                                                          .countryCode
                                                          : "CA"),
                                                  style: const TextStyle(
                                                      fontSize: 20),
                                                ),
                                                SizedBox(width: 8.0),
                                                Text(
                                                  '+${snapshot.hasData ? snapshot.data.phoneCode : "1"}',
                                                  style:
                                                  textStyleQuicksandBlack16,
                                                ),
                                                SizedBox(width: 8.0),
                                                Image.asset(
                                                    "assets/png/drop_down_light.png",
                                                    width: 16,
                                                    height: 16)
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                    SizedBox(width: 16.0),
                                    Expanded(
                                      child: TextField(
                                        style: textStyleQuicksandBlack16,
                                        keyboardType: TextInputType.number,
                                        maxLength: 14,
                                        controller: _phoneController,
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly,
                                          UsNumberTextInputFormatter()
                                        ],
                                        onChanged: (s) => _bloc.phoneSink.add(s),
                                        decoration: InputDecoration(
                                          isDense: true,
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 1.5,
                                                color: COLOR_LIGHT_GRAY),
                                          ),
                                          border: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 1.5,
                                                color: COLOR_LIGHT_GRAY),
                                          ),
                                          counterText: "",
                                          hintText: "(123) 456-7890",
                                          hintStyle:
                                          textStyleQuicksandLightGrey16,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Email*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                TextField(
                                  style: textStyleQuicksandBlack16,
                                  keyboardType: TextInputType.emailAddress,
                                  onChanged: (s) => _bloc.emailSink.add(s),
                                  decoration: InputDecoration(
                                    isDense: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1.5, color: COLOR_LIGHT_GRAY),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      SlideTransition(
                        position: _bloc.slidingRightToLeftAnimation,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Professional Information",
                                style: textStyleQuicksandBlack16),
                            SizedBox(height: 30),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "College*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                StreamBuilder<AllServicesResponse>(
                                    stream: _bloc.allServicesResponseStream,
                                    builder: (context, mainSnapshot) {
                                      if (mainSnapshot.hasData) {
                                        return Container(
                                          child: StreamBuilder<String>(
                                            stream: _bloc.collegesStream,
                                            builder: (context, snapshot) {
                                              return DropdownButton<String>(
                                                onTap: () => FocusScope.of(
                                                    context)
                                                    .requestFocus(FocusNode()),
                                                underline: Container(
                                                  color: COLOR_LIGHT_GRAY,
                                                  height: 1.5,
                                                ),
                                                value: snapshot.data,
                                                isExpanded: true,
                                                icon: Image.asset(
                                                    "assets/png/drop_down_light.png",
                                                    width: 16,
                                                    height: 16),
                                                onChanged: (value) =>
                                                    _bloc.collegesSink.add(value),
                                                items:
                                                mainSnapshot.data.college.map(
                                                      (value) {
                                                    return DropdownMenuItem<
                                                        String>(
                                                      value: value.id1,
                                                      child: Text(
                                                          value.collegeName,
                                                          style:
                                                          textStyleQuicksandBlack16),
                                                    );
                                                  },
                                                ).toList(),
                                              );
                                            },
                                          ),
                                        );
                                      } else {
                                        return Center(
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                            ),
                                          ),
                                        );
                                      }
                                    }),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Speciality*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                StreamBuilder<AllServicesResponse>(
                                    stream: _bloc.allServicesResponseStream,
                                    builder: (context, mainSnapshot) {
                                      if (mainSnapshot.hasData) {
                                        return AutoCompleteTextField<Speciality>(
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.symmetric(
                                                vertical: 10.0),
                                            isDense: true,
                                            prefixIconConstraints: BoxConstraints(
                                                maxHeight: 30, maxWidth: 30),
                                            prefixIcon: Padding(
                                              padding: EdgeInsets.all(6.0),
                                              child: Image.asset(
                                                "assets/png/search_blue.png",
                                                height: 20,
                                                width: 20,
                                              ),
                                            ),
                                          ),
                                          textCapitalization:
                                          TextCapitalization.words,
                                          itemSubmitted: (item) {
                                            _selectedSpecialityList.add(item);
                                            _bloc.selectedSpecialityListSink
                                                .add(_selectedSpecialityList);
                                          },
                                          key: _specialityKey,
                                          suggestions:
                                          mainSnapshot.data.speciality,
                                          itemBuilder: (context, suggestion) =>
                                              ListTile(
                                                title: Text(suggestion.name),
                                                dense: true,
                                              ),
                                          itemSorter: (a, b) =>
                                              a.name.compareTo(b.name),
                                          itemFilter: (suggestion, input) =>
                                              suggestion.name
                                                  .toLowerCase()
                                                  .startsWith(
                                                input.toLowerCase(),
                                              ),
                                        );
                                      } else {
                                        return Center(
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                            ),
                                          ),
                                        );
                                      }
                                    }),
                                SizedBox(height: 10),
                                StreamBuilder<List<Speciality>>(
                                  initialData: _selectedSpecialityList,
                                  stream: _bloc.selectedSpecialityListStream,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData &&
                                        snapshot.data.isNotEmpty) {
                                      return Container(
                                        height: 40,
                                        child: Center(
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (context, index) {
                                              return Wrap(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              20.0)),
                                                      color:
                                                      APP_PRIMARY_COLOR_DARK,
                                                    ),
                                                    margin: EdgeInsets.all(4.0),
                                                    padding: EdgeInsets.only(
                                                        top: 0.0,
                                                        bottom: 0.0,
                                                        left: 12.0,
                                                        right: 8.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Text(
                                                          snapshot
                                                              .data[index].name,
                                                          style: textStyleWhite,
                                                        ),
                                                        SizedBox(width: 4.0),
                                                        Container(
                                                          height: 26,
                                                          width: 1,
                                                          color: Colors.white,
                                                        ),
                                                        SizedBox(width: 2.0),
                                                        InkWell(
                                                          onTap: () {
                                                            if (_selectedSpecialityList
                                                                .length >
                                                                0) {
                                                              _selectedSpecialityList
                                                                  .removeAt(
                                                                  index);
                                                              _bloc
                                                                  .selectedSpecialityListSink
                                                                  .add(
                                                                  _selectedSpecialityList);
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding:
                                                            EdgeInsets.all(
                                                                2.0),
                                                            child: Image.asset(
                                                              "assets/png/cross.png",
                                                              width: 15,
                                                              height: 15,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      );
                                    } else {
                                      return SizedBox(height: 5.0);
                                    }
                                  },
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Hospital*",
                                  style: textStyleGreyQuicksandBold14,
                                ),
                                StreamBuilder<AllServicesResponse>(
                                    stream: _bloc.allServicesResponseStream,
                                    builder: (context, mainSnapshot) {
                                      if (mainSnapshot.hasData) {
                                        return AutoCompleteTextField<Hospital>(
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.symmetric(
                                                vertical: 10.0),
                                            isDense: true,
                                            prefixIconConstraints: BoxConstraints(
                                                maxHeight: 30, maxWidth: 30),
                                            prefixIcon: Padding(
                                              padding: EdgeInsets.all(6.0),
                                              child: Image.asset(
                                                "assets/png/search_blue.png",
                                                height: 20,
                                                width: 20,
                                              ),
                                            ),
                                          ),
                                          textCapitalization:
                                          TextCapitalization.words,
                                          itemSubmitted: (item) {
                                            _selectedHospitalList.add(item);
                                            _bloc.selectedListSink
                                                .add(_selectedHospitalList);
                                          },
                                          key: _hospitalKey,
                                          suggestions:
                                          mainSnapshot.data.hospitals,
                                          itemBuilder: (context, suggestion) =>
                                              ListTile(
                                                title: Text(suggestion.hospitalName),
                                                dense: true,
                                              ),
                                          itemSorter: (a, b) => a.hospitalName
                                              .compareTo(b.hospitalName),
                                          itemFilter: (suggestion, input) =>
                                              suggestion.hospitalName
                                                  .toLowerCase()
                                                  .startsWith(
                                                input.toLowerCase(),
                                              ),
                                        );
                                      } else {
                                        return Center(
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                            ),
                                          ),
                                        );
                                      }
                                    }),
                                SizedBox(height: 10),
                                StreamBuilder<List<Hospital>>(
                                  initialData: _selectedHospitalList,
                                  stream: _bloc.selectedListStream,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData &&
                                        snapshot.data.isNotEmpty) {
                                      return Container(
                                        height: 40,
                                        child: Center(
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (context, index) {
                                              return Wrap(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              20.0)),
                                                      color:
                                                      APP_PRIMARY_COLOR_DARK,
                                                    ),
                                                    margin: EdgeInsets.all(4.0),
                                                    padding: EdgeInsets.only(
                                                        top: 0.0,
                                                        bottom: 0.0,
                                                        left: 12.0,
                                                        right: 8.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Text(
                                                          snapshot.data[index]
                                                              .hospitalName,
                                                          style: textStyleWhite,
                                                        ),
                                                        SizedBox(width: 4.0),
                                                        Container(
                                                          height: 26,
                                                          width: 1,
                                                          color: Colors.white,
                                                        ),
                                                        SizedBox(width: 2.0),
                                                        InkWell(
                                                          onTap: () {
                                                            if (_selectedHospitalList
                                                                .length >
                                                                0) {
                                                              _selectedHospitalList
                                                                  .removeAt(
                                                                  index);
                                                              _bloc
                                                                  .selectedListSink
                                                                  .add(
                                                                  _selectedHospitalList);
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding:
                                                            EdgeInsets.all(
                                                                2.0),
                                                            child: Image.asset(
                                                              "assets/png/cross.png",
                                                              width: 15,
                                                              height: 15,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      );
                                    } else {
                                      return SizedBox(height: 5.0);
                                    }
                                  },
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      SlideTransition(
                        position: _bloc.slidingBottomRightToCenterAnimation,
                        child: Hero(
                          tag: APP_NAME,
                          child: Material(
                            elevation: 5.0,
                            color: APP_PRIMARY_COLOR,
                            shadowColor: APP_PRIMARY_COLOR_LIGHT,
                            borderRadius: BorderRadius.all(
                              Radius.circular(15.0),
                            ),
                            child: InkWell(
                              onTap: () => _bloc.onClickRegister(context),
                              borderRadius: BorderRadius.all(
                                Radius.circular(15.0),
                              ),
                              splashColor: APP_PRIMARY_COLOR_DARK,
                              child: Container(
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(15.0),
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(width: 50),
                                      Text(
                                        "Register",
                                        style: textStyleWhite16,
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(width: 20.0),
                                      RotatedBox(
                                        quarterTurns: 45,
                                        child: Lottie.asset(
                                            'assets/json/arrow_up.json',
                                            width: 30,
                                            height: 30),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 40),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  _openCountryPickerDialog(BuildContext context, CompleteProfileBloc bloc) {
    showCountryPicker(
      context: context,
      showPhoneCode: true,
      onSelect: (Country country) => bloc.countryPickerSink.add(country),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //register user response
    _bloc.registerUserResponseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        await FrequentUtils.getInstance().saveUserData(response.data);
        await _bloc.startAnimationReverse();
        await Navigator.of(context)
            .pushNamedAndRemoveUntil(AppRoute.successfulSubmit, (_) => false);
        _bloc.startAnimationForward();
      }
    });
  }

  TextEditingController getDefaultPhoneController() {
    final number = FrequentUtils.getInstance().getFormattedPhoneNumber(
        widget.phone.substring(widget.phone.length - 10, widget.phone.length));
    return TextEditingController.fromValue(TextEditingValue(text: number));
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
