import 'dart:async';
import 'dart:io';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserRequest.dart';
import 'package:fracture_buddy/models/registerUser/RegisterUserResponse.dart';
import 'package:fracture_buddy/models/serviceList/AllServicesResponse.dart';
import 'package:fracture_buddy/models/serviceList/Hospital.dart';
import 'package:fracture_buddy/models/serviceList/Speciality.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/image_picker_popup.dart';
import 'package:fracture_buddy/utils/validations.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';

class CompleteProfileBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation,
      slidingBottomRightToCenterAnimation;

  BehaviorSubject<Country> _countryPickerController;
  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<String> _userNameController;
  BehaviorSubject<String> _firstNameController;
  BehaviorSubject<String> _lastNameController;
  BehaviorSubject<String> _phoneController;
  BehaviorSubject<String> _emailController;
  BehaviorSubject<File> _profilePicController;
  BehaviorSubject<String> _collegesController;
  BehaviorSubject<String> _hospitalController;
  BehaviorSubject<List<Hospital>> _selectedListController;
  BehaviorSubject<List<Speciality>> _selectedSpecialityListController;
  BehaviorSubject<AllServicesResponse> _allServiceResponseController;
  BehaviorSubject<RegisterUserResponse> _registerUserResponseController;

  CompleteProfileBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 600),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0)).animate(_controller);
    slidingLeftToRightAnimation = Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0)).animate(_controller);
    slidingTopRightToCenterAnimation = Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0)).animate(_controller);
    slidingBottomRightToCenterAnimation = Tween<Offset>(begin: Offset(0.2, 3.0), end: Offset(0.0, 0.0)).animate(_controller);
    //value controller initialization
    _countryPickerController = BehaviorSubject.seeded(Country(countryCode: "CA", phoneCode: "1"));
    _loadingController = BehaviorSubject.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _userNameController = BehaviorSubject.seeded("");
    _firstNameController = BehaviorSubject.seeded("");
    _lastNameController = BehaviorSubject.seeded("");
    _phoneController = BehaviorSubject.seeded("");
    _emailController = BehaviorSubject.seeded("");
    _profilePicController = BehaviorSubject<File>();
    _collegesController = BehaviorSubject();
    _hospitalController = BehaviorSubject();
    _selectedListController = BehaviorSubject.seeded(List<Hospital>());
    _selectedSpecialityListController = BehaviorSubject.seeded(List<Speciality>());
    _allServiceResponseController = BehaviorSubject();
    _registerUserResponseController = BehaviorSubject();
  }

  StreamSink<Country> get countryPickerSink => _countryPickerController.sink;

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  StreamSink<String> get userNameSink => _userNameController.sink;

  StreamSink<String> get firstNameSink => _firstNameController.sink;

  StreamSink<String> get lastNameSink => _lastNameController.sink;

  StreamSink<String> get phoneSink => _phoneController.sink;

  StreamSink<String> get emailSink => _emailController.sink;

  StreamSink<File> get profilePicSink => _profilePicController.sink;

  StreamSink<String> get collegesSink => _collegesController.sink;

  StreamSink<String> get hospitalSink => _hospitalController.sink;

  StreamSink<List<Hospital>> get selectedListSink => _selectedListController.sink;

  StreamSink<List<Speciality>> get selectedSpecialityListSink => _selectedSpecialityListController.sink;

  StreamSink<RegisterUserResponse> get registerUserResponseSink => _registerUserResponseController.sink;

  StreamSink<AllServicesResponse> get allServiceResponseResponseSink => _allServiceResponseController.sink;

  Stream<Country> get countryPickerStream => _countryPickerController.stream;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<String> get userNameStream => _userNameController.stream;

  Stream<String> get firstNameStream => _firstNameController.stream;

  Stream<String> get lastNameStream => _lastNameController.stream;

  Stream<File> get profilePicStream => _profilePicController.stream;

  Stream<String> get collegesStream => _collegesController.stream;

  Stream<String> get hospitalStream => _hospitalController.stream;

  Stream<List<Hospital>> get selectedListStream => _selectedListController.stream;

  Stream<List<Speciality>> get selectedSpecialityListStream => _selectedSpecialityListController.stream;

  Stream<RegisterUserResponse> get registerUserResponseStream => _registerUserResponseController.stream;

  Stream<AllServicesResponse> get allServicesResponseStream => _allServiceResponseController.stream;

  TickerFuture startAnimationForward() {
    return _controller.forward();
  }

  TickerFuture startAnimationReverse() {
    return _controller.reverse();
  }

  onClickUploadPhoto(BuildContext context) {
    ImagePickerPopup.showImagePickerDialog(context, (image) {
      if (image != null) _profilePicController.sink.add(image);
    });
  }

  getAllServiceList(BuildContext context) async {
    _loadingController.sink.add(true);
    final response = await WebService.getInstance().getAllServiceList();
    _loadingController.sink.add(false);
    if (response.success) {
      allServiceResponseResponseSink.add(response);
    } else {
      errorMessageSink.add(response.message);
    }
  }

  onClickRegister(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if(_profilePicController.value==null){
      errorMessageSink.add("Please select a profile picture.");
    }else if(Validations.getInstance().isFieldEmpty(_userNameController.value)){
      errorMessageSink.add("Please enter an user id.");
    }else if(Validations.getInstance().isFieldEmpty(_firstNameController.value)){
      errorMessageSink.add("Please enter your first name.");
    }else if(Validations.getInstance().isFieldEmpty(_lastNameController.value)){
      errorMessageSink.add("Please enter your last name.");
    }else if(Validations.getInstance().isFieldEmpty(_phoneController.value)){
      errorMessageSink.add("Please enter your phone number.");
    }else if(Validations.getInstance().isInvalidPhone(_phoneController.value)){
      errorMessageSink.add("Please enter valid phone number.");
    }else if(Validations.getInstance().isFieldEmpty(_emailController.value)){
      errorMessageSink.add("Please enter your email address.");
    }else if(Validations.getInstance().isInvalidEmail(_emailController.value)){
      errorMessageSink.add("Please enter valid email address.");
    }else if(Validations.getInstance().isFieldEmpty(_collegesController.value)){
      errorMessageSink.add("Please select a collage.");
    }else if(_selectedSpecialityListController.value.isEmpty){
      errorMessageSink.add("Please select speciality.");
    }else if(_selectedListController.value.isEmpty){
      errorMessageSink.add("Please select hospital.");
    }else{
      _loadingController.sink.add(true);
      final phone = FrequentUtils.getInstance().getPlanPhoneNumber(
          _countryPickerController.value.phoneCode, _phoneController.value);
      final registerRequest = RegisterUserRequest(
        userName: _userNameController.value,
        firstName: _firstNameController.value,
        lastName: _lastNameController.value,
        phone: phone,
        countryCode: _countryPickerController.value.countryCode,
        email: _emailController.value,
        college: _collegesController.value,
        deviceId: "",
        hospital: _selectedListController.value.map((e) => e.id1).toList(),
        speciality: _selectedSpecialityListController.value.map((e) => e.id1).toList(),
      );
      final response = await WebService.getInstance().registerUserRequest(registerRequest,_profilePicController.value);
      _loadingController.sink.add(false);
      if (response.success) {
        registerUserResponseSink.add(response);
      } else {
        errorMessageSink.add(response.message);
      }
    }
  }


  @override
  void dispose() {
    _controller.dispose();
    _countryPickerController.close();
    _loadingController.close();
    _errorMessageController.close();
    _userNameController.close();
    _firstNameController.close();
    _lastNameController.close();
    _phoneController.close();
    _emailController.close();
    _profilePicController.close();
    _collegesController.close();
    _hospitalController.close();
    _selectedListController.close();
    _selectedSpecialityListController.close();
    _allServiceResponseController.close();
    _registerUserResponseController.close();
  }

}
