import 'dart:async';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/sendOtp/SendOTPResponse.dart';
import 'package:fracture_buddy/models/surgicalForm/AllSurgicalFormResponse.dart';
import 'package:fracture_buddy/models/surgicalForm/SurgicalFormData.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/base_bloc.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/web/web_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class HomeBloc extends BaseBloc {
  AnimationController _controller;
  Animation<Offset> slidingRightToLeftAnimation,
      slidingLeftToRightAnimation,
      slidingTopRightToCenterAnimation;

  BehaviorSubject<bool> _loadingController;
  BehaviorSubject<String> _errorMessageController;
  BehaviorSubject<AllSurgicalFormResponse> _responseController;
  BehaviorSubject<List<SurgicalFormData>> _surgicalFormListController;

  HomeBloc(TickerProvider provider) {
    _controller = AnimationController(
      vsync: provider,
      duration: Duration(milliseconds: 700),
      reverseDuration: Duration(milliseconds: 300),
    );
    slidingRightToLeftAnimation =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingLeftToRightAnimation =
        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0))
            .animate(_controller);
    slidingTopRightToCenterAnimation =
        Tween<Offset>(begin: Offset(1.0, -1.0), end: Offset(0.0, 0.0))
            .animate(_controller);

    _loadingController = BehaviorSubject.seeded(false);
    _errorMessageController = BehaviorSubject.seeded("");
    _responseController = BehaviorSubject();
    _surgicalFormListController = BehaviorSubject<List<SurgicalFormData>>.seeded(List());
  }

  StreamSink<bool> get loadingSink => _loadingController.sink;

  StreamSink<String> get errorMessageSink => _errorMessageController.sink;

  StreamSink<List<SurgicalFormData>> get surgicalFormListSink => _surgicalFormListController.sink;

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<String> get errorMessageStream => _errorMessageController.stream;

  Stream<AllSurgicalFormResponse> get responseStream => _responseController.stream;

  Stream<List<SurgicalFormData>> get surgicalFormListStream => _surgicalFormListController.stream;

  TickerFuture startAnimationForward() {
    return _controller.forward();
  }

  TickerFuture startAnimationReverse() {
    return _controller.reverse();
  }

  Future<void> getAllSurgicalForm(BuildContext context) async {
    _loadingController.sink.add(true);
    final authToken = SharedPref.mInstance.getString(SharedPref.STORAGE_TOKEN);
    final response = await WebService.getInstance().allSurgicalForm(authToken,0);
    _loadingController.sink.add(false);
    if (response.success) {
      _responseController.add(response);
    } else {
      _errorMessageController.sink.add(response.message);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _loadingController.close();
    _errorMessageController.close();
    _responseController.close();
    _surgicalFormListController.close();
  }
}
