import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fracture_buddy/models/surgicalForm/SurgicalFormData.dart';
import 'package:fracture_buddy/utils/SharedPref.dart';
import 'package:fracture_buddy/utils/app_router.dart';
import 'package:fracture_buddy/utils/constants.dart';
import 'package:fracture_buddy/utils/frequent_utils.dart';
import 'package:fracture_buddy/utils/style.dart';
import 'package:fracture_buddy/views/home/home_bloc.dart';
import 'package:fracture_buddy/utils/custom_extensions.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  HomeBloc _bloc;

  @override
  void initState() {
    _bloc = HomeBloc(this);
    _initializeCommonListener(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: APP_PRIMARY_COLOR,
      drawer: _drawerDataController(context, screenSize),
      appBar: AppBar(
        leading: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 9.0),
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            splashColor: Colors.grey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset("assets/png/menu.png", width: 24, height: 24),
            ),
            onTap: () {
              _scaffoldKey.currentState.openDrawer();
            },
          ),
        ),
        elevation: 0,
        title: Stack(
          alignment: Alignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/png/stethoscope_transparent_white.png",
                  height: 100,
                  width: 90,
                ),
                Image.asset("assets/png/stethoscope_transparent_blue.png",
                    height: 80, width: 90),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: screenSize.width / 7.0),
              child: Text("Home", style: textStyleWhiteBold18),
            ),
          ],
        ),
        toolbarHeight: 80,
      ),
      body: Container(
        padding: EdgeInsets.all(24.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: APP_NAME,
              child: Material(
                borderRadius: BorderRadius.all(
                  Radius.circular(15.0),
                ),
                color: APP_PRIMARY_COLOR_LIGHT2,
                child: InkWell(
                  onTap: () {
                    onClickAddSurgicalForm(context);
                  },
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                  splashColor: APP_PRIMARY_COLOR,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15.0),
                      ),
                      border: Border.all(color: APP_PRIMARY_COLOR, width: 1.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/png/plus_circle_blue.png",
                            width: 25,
                            height: 25,
                          ),
                          SizedBox(width: 8.0),
                          Text(
                            "Surgical Care Form",
                            style: textStyleQuicksandBlueBold16,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(width: 24.0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Text(
              "Logs",
              style: textStyleQuicksandLightGrey14,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () => _bloc.getAllSurgicalForm(context),
                child: Container(
                  height: double.maxFinite,
                  width: double.maxFinite,
                  child: StreamBuilder<List<SurgicalFormData>>(
                    initialData: [],
                    stream: _bloc.surgicalFormListStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data.isNotEmpty) {
                        return ListView.separated(
                          itemCount: snapshot.data.length,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            final formData = snapshot.data[index];
                            return Container(
                              height: 85,
                              child: Material(
                                elevation: 2.0,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0)),
                                child: Row(
                                  children: [
                                    Container(
                                      width: 3.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.horizontal(
                                            left: Radius.circular(6.0),
                                          ),
                                          color: COLOR_YELLOW_LIGHT),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 50,
                                              width: 50,
                                              decoration: BoxDecoration(
                                                color: Colors.grey,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(8.0),
                                                ),
                                              ),
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8.0)),
                                                  child: CachedNetworkImage(
                                                    imageUrl: APP_IMAGE_URL +
                                                        formData.diagnosis,
                                                    fit: BoxFit.cover,
                                                    placeholder:
                                                        (context, url) =>
                                                            Center(
                                                      child: Padding(
                                                        padding: EdgeInsets.all(
                                                            30.0),
                                                        child:
                                                            CircularProgressIndicator(
                                                          strokeWidth: 2,
                                                        ),
                                                      ),
                                                    ),
                                                    errorWidget:
                                                        (context, url, value) {
                                                      return Image.asset(
                                                        "assets/png/xray_image.png",
                                                        fit: BoxFit.fill,
                                                      );
                                                    },
                                                  )),
                                            ),
                                            Expanded(
                                              child: Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 4.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "#44556${(index + 1) * 3}",
                                                            style:
                                                                textStyleGreyQuicksandBold12),
                                                        SizedBox(width: 4.0),
                                                        Text("Pending",
                                                            style:
                                                                textStyleQuicksandBoldYellow12),
                                                      ],
                                                    ),
                                                    Text(
                                                      "${formData.firstName} ${formData.lastName}",
                                                      style:
                                                          textStyleQuicksandBlack16,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    SizedBox(height: 4.0),
                                                    Row(
                                                      children: [
                                                        Image.asset(
                                                            "assets/png/location_pin.png",
                                                            width: 16,
                                                            height: 16),
                                                        SizedBox(width: 4.0),
                                                        Expanded(
                                                          child: Text(
                                                            formData.patientLocation,
                                                            style: textStyleGreyQuicksand12,
                                                            overflow: TextOverflow.ellipsis,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            // Container(
                                            //   padding: EdgeInsets.all(2.0),
                                            //   child: Image.asset(
                                            //     "assets/png/3_arrows_grey.png",
                                            //     width: 16,
                                            //     height: 16,
                                            //   ),
                                            // )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox(height: 10.0);
                          },
                        );
                      } else {
                        return Center(
                          child: Padding(
                            padding: EdgeInsets.all(20),
                            child: Text("No surgical form found."),
                          ),
                        );
                      }
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _drawerDataController(BuildContext context, Size screenSize) {
    return SafeArea(
      child: Container(
        width: screenSize.width / 1.3,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.horizontal(
            right: Radius.circular(30.0),
          ),
          color: Colors.white,
        ),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            InkWell(
              onTap: () => onClickDrawer(context),
              child: DrawerHeader(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30.0),
                  ),
                  color: APP_PRIMARY_COLOR,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        borderRadius: BorderRadius.all(
                          Radius.circular(35.0),
                        ),
                      ),
                      width: 70,
                      height: 70,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(35.0)),
                        child: CachedNetworkImage(
                          imageUrl: APP_IMAGE_URL +
                              SharedPref.mInstance
                                  .getString(SharedPref.STORAGE_PROFILE_PIC),
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Center(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              ),
                            ),
                          ),
                          errorWidget: (context, url, value) {
                            return Image.asset("assets/png/doctor.png");
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      "${SharedPref.mInstance.getString(SharedPref.STORAGE_FIRST_NAME)} ${SharedPref.mInstance.getString(SharedPref.STORAGE_LAST_NAME)}",
                      style: textStyleWhite16,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      "${SharedPref.mInstance.getString(SharedPref.STORAGE_EMAIL)}",
                      style: textStyleWhite12,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () => {},
              child: ListTile(
                leading: Icon(Icons.home, color: APP_PRIMARY_COLOR),
                title: Text("Home", style: textStyleQuicksandBlack16),
              ),
            ),
            InkWell(
              onTap: () => {},
              child: ListTile(
                leading: Icon(Icons.history, color: APP_PRIMARY_COLOR),
                title: Text("History", style: textStyleQuicksandBlack16),
              ),
            ),
            InkWell(
              onTap: () => onClickLogout(context),
              child: ListTile(
                leading: Icon(Icons.exit_to_app, color: APP_PRIMARY_COLOR),
                title: Text("Logout", style: textStyleQuicksandBlack16),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _initializeCommonListener(BuildContext context) {
    //get all class list
    _bloc.getAllSurgicalForm(context);
    //loading indicator
    _bloc.loadingStream.listen((bool value) {
      if (value) {
        FrequentUtils.getInstance().showProgressDialog(context);
      } else {
        FrequentUtils.getInstance().hideProgressDialog(context);
      }
    });
    //message
    _bloc.errorMessageStream.listen((String message) {
      FrequentUtils.getInstance().showSnackBarMessage(_scaffoldKey, message);
    });
    //
    _bloc.responseStream.listen((response) async {
      if (response.isNotNull() && response.success) {
        _bloc.surgicalFormListSink.add(response.data);
      }
    });
  }

  void onClickDrawer(BuildContext context) {
    Navigator.of(context).pushNamed(AppRoute.editProfile);
  }

  onClickAddSurgicalForm(BuildContext context) async {
    await Navigator.of(context).pushNamed(AppRoute.surgicalCareForm);
    _bloc.getAllSurgicalForm(context);
  }

  onClickLogout(BuildContext context) {
    SharedPref.mInstance.clearData();
    Navigator.of(context)
        .pushNamedAndRemoveUntil(AppRoute.createAccount, (_) => false);
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
